package com.sbc.config;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jndi.JndiTemplate;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.sbc.util.Parametros;

@EnableWebMvc
@Configuration

@ComponentScan(basePackages = { "com.sbc" })
public class AppConfig extends WebMvcConfigurerAdapter {

	@Bean
	public DataSource getDataSource() throws NamingException {
		return (DataSource) new JndiTemplate().lookup(Parametros.datasource);
	}

	@Bean
	public JdbcTemplate getJdbcTemplate() throws NamingException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(getDataSource());
		return jdbcTemplate;
	}

	@Bean
	public DataSourceTransactionManager getDataSourceTransactionManager() throws NamingException {
		DataSourceTransactionManager dataSourceTransactionManager = new DataSourceTransactionManager();
		dataSourceTransactionManager.setDataSource(getDataSource());
		return dataSourceTransactionManager;
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/**").addResourceLocations("/");
	}

	@Bean
	public InternalResourceViewResolver jspViewResolver() {
		InternalResourceViewResolver bean = new InternalResourceViewResolver();
		bean.setPrefix("/");
		bean.setSuffix(".jsp");
		return bean;
	}

	@Bean
	public MultipartResolver multipartResolver() {
		return new StandardServletMultipartResolver();
	}

	/*
	 * @Bean public ReloadableResourceBundleMessageSource messageSource() {
	 * ReloadableResourceBundleMessageSource messageSource = new
	 * ReloadableResourceBundleMessageSource();
	 * messageSource.setBasename("classpath:messages");
	 * messageSource.setDefaultEncoding("UTF-8"); return messageSource; }
	 * 
	 * @Bean public CookieLocaleResolver localeResolver() { CookieLocaleResolver
	 * localeResolver = new CookieLocaleResolver();
	 * localeResolver.setDefaultLocale(new Locale("es","ES"));
	 * localeResolver.setCookieName("locale-cookie");
	 * localeResolver.setCookieMaxAge(-1); //localeResolver.setCookieMaxAge(1800);
	 * return localeResolver; }
	 * 
	 * @Bean public LocaleChangeInterceptor localeInterceptor() {
	 * LocaleChangeInterceptor interceptor = new LocaleChangeInterceptor();
	 * interceptor.setParamName("locale"); return interceptor; }
	 * 
	 * @Override public void addInterceptors(InterceptorRegistry registry) {
	 * 
	 * registry.addInterceptor(localeInterceptor()); } /*
	 * 
	 * @Override public void addInterceptors(InterceptorRegistry registry) {
	 * LocaleChangeInterceptor interceptor = new LocaleChangeInterceptor();
	 * interceptor.setParamName("locale"); registry.addInterceptor(interceptor); }
	 */
	/*
	 * @Bean public LocaleResolver localeResolver() { SessionLocaleResolver slr =
	 * new SessionLocaleResolver(); slr.setDefaultLocale(new Locale("es")); return
	 * slr; }
	 */

	// @Bean(name = "localeResolver")
	// public LocaleResolver getLocaleResolver() {
	// return new CookieLocaleResolver();
	// }

	// @Bean(name = "localeResolver")
	// public LocaleResolver getLocaleResolver() {
	// CookieLocaleResolver localeResolver = new CookieLocaleResolver();
	// localeResolver.setDefaultLocale(new Locale("es"));
	// localeResolver.setCookieName("lang");
	// localeResolver.setCookieMaxAge(-1);
	// return localeResolver;
	// }

}
