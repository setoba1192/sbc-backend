package com.sbc.dto;

import com.sbc.model.Actividad;
import com.sbc.model.Area;

public class AreaActividad {

	private Area area;

	private Actividad actividad;

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public Actividad getActividad() {
		return actividad;
	}

	public void setActividad(Actividad actividad) {
		this.actividad = actividad;
	}
	
	

}
