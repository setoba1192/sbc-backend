package com.sbc.model;

import java.util.List;

public class PersonaArea {

	private int pea_codigo;

	private Persona persona;

	private Area area;

	private List<Recordatorio> recordatorios;

	private List<Actividad> actividades;

	public List<Actividad> getActividades() {
		return actividades;
	}

	public void setActividades(List<Actividad> actividades) {
		this.actividades = actividades;
	}

	public int getPea_codigo() {
		return pea_codigo;
	}

	public void setPea_codigo(int pea_codigo) {
		this.pea_codigo = pea_codigo;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public List<Recordatorio> getRecordatorios() {
		return recordatorios;
	}

	public void setRecordatorios(List<Recordatorio> recordatorios) {
		this.recordatorios = recordatorios;
	}

}
