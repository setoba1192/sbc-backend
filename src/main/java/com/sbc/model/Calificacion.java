package com.sbc.model;

public class Calificacion {
	
	private int cal_codigo;
	
	private String cal_nombre;

	public int getCal_codigo() {
		return cal_codigo;
	}

	public void setCal_codigo(int cal_codigo) {
		this.cal_codigo = cal_codigo;
	}

	public String getCal_nombre() {
		return cal_nombre;
	}

	public void setCal_nombre(String cal_nombre) {
		this.cal_nombre = cal_nombre;
	}
	
	

}
