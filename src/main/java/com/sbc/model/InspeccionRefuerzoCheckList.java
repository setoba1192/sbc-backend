package com.sbc.model;

public class InspeccionRefuerzoCheckList {

	private int id;
	private boolean aplica;
	private RefuerzoCheckList refuerzoCheckList;

	public RefuerzoCheckList getRefuerzoCheckList() {
		return refuerzoCheckList;
	}

	public void setRefuerzoCheckList(RefuerzoCheckList refuerzoCheckList) {
		this.refuerzoCheckList = refuerzoCheckList;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isAplica() {
		return aplica;
	}

	public void setAplica(boolean aplica) {
		this.aplica = aplica;
	}

}
