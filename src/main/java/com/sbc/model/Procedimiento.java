package com.sbc.model;

public class Procedimiento {

	private int pro_codigo;

	private String pro_nombre;

	private int pro_orden;

	private boolean pro_estado;

	public boolean isPro_estado() {
		return pro_estado;
	}

	public void setPro_estado(boolean pro_estado) {
		this.pro_estado = pro_estado;
	}

	public int getPro_codigo() {
		return pro_codigo;
	}

	public void setPro_codigo(int pro_codigo) {
		this.pro_codigo = pro_codigo;
	}

	public String getPro_nombre() {
		return pro_nombre;
	}

	public void setPro_nombre(String pro_nombre) {
		this.pro_nombre = pro_nombre;
	}

	public int getPro_orden() {
		return pro_orden;
	}

	public void setPro_orden(int pro_orden) {
		this.pro_orden = pro_orden;
	}

}
