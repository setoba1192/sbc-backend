package com.sbc.model;

public class Dia {

	private int dia_codigo;

	private String dia_nombre;

	private int dia_dia_semana;

	private boolean laboral;

	private int ard_codigo;

	
	public int getDia_dia_semana() {
		return dia_dia_semana;
	}

	public void setDia_dia_semana(int dia_dia_semana) {
		this.dia_dia_semana = dia_dia_semana;
	}

	public int getArd_codigo() {
		return ard_codigo;
	}

	public void setArd_codigo(int ard_codigo) {
		this.ard_codigo = ard_codigo;
	}

	public boolean isLaboral() {
		return laboral;
	}

	public void setLaboral(boolean laboral) {
		this.laboral = laboral;
	}

	public int getDia_codigo() {
		return dia_codigo;
	}

	public void setDia_codigo(int dia_codigo) {
		this.dia_codigo = dia_codigo;
	}

	public String getDia_nombre() {
		return dia_nombre;
	}

	public void setDia_nombre(String dia_nombre) {
		this.dia_nombre = dia_nombre;
	}

}
