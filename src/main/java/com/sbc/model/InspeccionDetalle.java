package com.sbc.model;

public class InspeccionDetalle {

	private long ind_codigo;

	private Procedimiento procedimiento;

	private String ind_observacion;

	private Calificacion calificacion;

	public long getInd_codigo() {
		return ind_codigo;
	}

	public void setInd_codigo(long ind_codigo) {
		this.ind_codigo = ind_codigo;
	}

	public Procedimiento getProcedimiento() {
		return procedimiento;
	}

	public void setProcedimiento(Procedimiento procedimiento) {
		this.procedimiento = procedimiento;
	}

	public String getInd_observacion() {
		return ind_observacion;
	}

	public void setInd_observacion(String ind_observacion) {
		this.ind_observacion = ind_observacion;
	}

	public Calificacion getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(Calificacion calificacion) {
		this.calificacion = calificacion;
	}

}
