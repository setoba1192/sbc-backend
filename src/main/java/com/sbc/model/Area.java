package com.sbc.model;

import java.util.List;

public class Area {

	private int are_codigo;

	private String are_nombre;

	private String are_descripcion;

	private NivelCritico nivelCritico;

	private boolean asignacion;

	private List<Actividad> actividades;

	private List<Dia> dias;

	public List<Dia> getDias() {
		return dias;
	}

	public void setDias(List<Dia> dias) {
		this.dias = dias;
	}

	public List<Actividad> getActividades() {
		return actividades;
	}

	public void setActividades(List<Actividad> actividades) {
		this.actividades = actividades;
	}

	public boolean isAsignacion() {
		return asignacion;
	}

	public void setAsignacion(boolean asignacion) {
		this.asignacion = asignacion;
	}

	public String getAre_descripcion() {
		return are_descripcion;
	}

	public void setAre_descripcion(String are_descripcion) {
		this.are_descripcion = are_descripcion;
	}

	public int getAre_codigo() {
		return are_codigo;
	}

	public void setAre_codigo(int are_codigo) {
		this.are_codigo = are_codigo;
	}

	public String getAre_nombre() {
		return are_nombre;
	}

	public void setAre_nombre(String are_nombre) {
		this.are_nombre = are_nombre;
	}

	public NivelCritico getNivelCritico() {
		return nivelCritico;
	}

	public void setNivelCritico(NivelCritico nivelCritico) {
		this.nivelCritico = nivelCritico;
	}

}
