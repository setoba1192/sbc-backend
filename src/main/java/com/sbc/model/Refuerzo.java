package com.sbc.model;

import java.util.List;

public class Refuerzo {

	private int id;

	private int puntajeMaximo;

	private int puntajeObtenido;

	private int puntajeMejora;

	private String observacion;

	private String audio;

	private List<InspeccionRefuerzoCheckList> checkList;

	public List<InspeccionRefuerzoCheckList> getCheckList() {
		return checkList;
	}

	public void setCheckList(List<InspeccionRefuerzoCheckList> checkList) {
		this.checkList = checkList;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPuntajeMaximo() {
		return puntajeMaximo;
	}

	public void setPuntajeMaximo(int puntajeMaximo) {
		this.puntajeMaximo = puntajeMaximo;
	}

	public int getPuntajeObtenido() {
		return puntajeObtenido;
	}

	public void setPuntajeObtenido(int puntajeObtenido) {
		this.puntajeObtenido = puntajeObtenido;
	}

	public int getPuntajeMejora() {
		return puntajeMejora;
	}

	public void setPuntajeMejora(int puntajeMejora) {
		this.puntajeMejora = puntajeMejora;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getAudio() {
		return audio;
	}

	public void setAudio(String audio) {
		this.audio = audio;
	}

}
