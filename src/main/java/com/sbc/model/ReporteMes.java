package com.sbc.model;

public class ReporteMes {

	private String mes;

	private int cumple;

	private int noCumple;

	private int noAplica;

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public int getCumple() {
		return cumple;
	}

	public void setCumple(int cumple) {
		this.cumple = cumple;
	}

	public int getNoCumple() {
		return noCumple;
	}

	public void setNoCumple(int noCumple) {
		this.noCumple = noCumple;
	}

	public int getNoAplica() {
		return noAplica;
	}

	public void setNoAplica(int noAplica) {
		this.noAplica = noAplica;
	}

}
