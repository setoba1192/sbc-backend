package com.sbc.util;

public class Parametros {

	/**
	 * AMBIENTE DE PRUEBA
	 */
	public static final String datasource = "java:/sbcDS";
	public static final String logo = "uao_logo_.png";

	/**
	 * Clipsalud
	 */
	// public static final String datasource = "java:/sbcDS1";
	// public static final String logo = "uao_logo_clips.png";

	/**
	 * Mayra
	 */
	// public static final String datasource = "java:/sbcDS2";
	// public static final String logo = "uao_logo_mayra.png";
	/**
	 * Freez Ingenierias
	 */

	// public static final String datasource = "java:/sbcDS3";
	// public static final String logo = "uao_logo_freez.png";
	/**
	 * Green Solutions
	 */

	// public static final String datasource = "java:/sbcDS4";
	// public static final String logo = "uao_logo_green.png";
	/**
	 * Cafe especial tierradentro
	 */

	// public static final String datasource = "java:/sbcDS5";
	// public static final String logo = "uao_logo_cafe.png";

}
