package com.sbc.exception;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


import com.sbc.dto.Respuesta;

@RestControllerAdvice
public class HandlerException {

	@ExceptionHandler({ ObjectAlreadyExistException.class })
	public Respuesta objectAlreadyExistException(Exception e, HttpServletResponse response) {
		e.printStackTrace();
		response.setStatus(505);
		return new Respuesta(e.getMessage(), Respuesta.Type.ERROR);
	}

	@ExceptionHandler({ RuntimeException.class })
	public Respuesta handleExceptions(Exception e, HttpServletResponse response) {
		e.printStackTrace();
		response.setStatus(400);
		return new Respuesta(e.getMessage(), Respuesta.Type.ERROR);
	}

	@ExceptionHandler({ UnexpectedException.class })
	public Respuesta unexpectedExceptions(Exception e, HttpServletResponse response) {
		e.printStackTrace();
		response.setStatus(500);
		return new Respuesta(e.getMessage(), Respuesta.Type.ERROR);
	}

}