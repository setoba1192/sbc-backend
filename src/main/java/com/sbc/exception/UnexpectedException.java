package com.sbc.exception;

public class UnexpectedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1842741735490931325L;

	public UnexpectedException() {

	}

	public UnexpectedException(String message) {
		super(message);
	}

	public UnexpectedException(Throwable cause) {
		super(cause);
	}

	public UnexpectedException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnexpectedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}