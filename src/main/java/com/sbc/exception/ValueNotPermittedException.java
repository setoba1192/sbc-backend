package com.sbc.exception;

public class ValueNotPermittedException extends RuntimeException {

	private static final long serialVersionUID = -4325423036600329705L;

	public ValueNotPermittedException() {

	}

	public ValueNotPermittedException(String message) {
		super(message);
	}

	public ValueNotPermittedException(Throwable cause) {
		super(cause);
	}

	public ValueNotPermittedException(String message, Throwable cause) {
		super(message, cause);
	}

	public ValueNotPermittedException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}