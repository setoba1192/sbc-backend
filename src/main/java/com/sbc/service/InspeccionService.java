package com.sbc.service;

import java.util.List;

import com.sbc.dto.Respuesta;
import com.sbc.model.Inspeccion;
import com.sbc.model.InspeccionTipo;
import com.sbc.model.Persona;

public interface InspeccionService {

	public Respuesta registrarInspeccion(Inspeccion inspeccion);
	
	public List<Inspeccion> listInspecciones(Persona persona, InspeccionTipo inspeccionTipo);

}
