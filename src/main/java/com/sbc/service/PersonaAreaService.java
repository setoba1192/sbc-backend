package com.sbc.service;

import com.sbc.dto.Respuesta;
import com.sbc.model.Persona;
import com.sbc.model.PersonaArea;

public interface PersonaAreaService {

	public Respuesta registrarAsignacion(PersonaArea personaArea);

	public Respuesta cambiarAsignacion(PersonaArea personaArea);
	
	public PersonaArea getPersonaArea(Persona persona);

}
