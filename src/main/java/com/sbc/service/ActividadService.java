package com.sbc.service;

import com.sbc.dto.Respuesta;
import com.sbc.model.Actividad;
import com.sbc.model.Area;

public interface ActividadService {

	public Respuesta registrarActividad(Area area, Actividad actividad);

	public Respuesta actualizarActividad(Area area, Actividad actividad);

}
