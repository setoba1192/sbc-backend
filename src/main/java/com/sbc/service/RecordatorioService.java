package com.sbc.service;

import com.sbc.dto.Respuesta;
import com.sbc.model.Recordatorio;

public interface RecordatorioService {

	public Respuesta crear(Recordatorio recordatorio);

	public Respuesta actualizar(Recordatorio recordatorio);

	public Recordatorio buscarPorDiaPersonaArea(Recordatorio recordatorio);

}
