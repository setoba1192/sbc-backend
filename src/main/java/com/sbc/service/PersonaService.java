package com.sbc.service;

import com.sbc.dto.Respuesta;
import com.sbc.model.Persona;

public interface PersonaService {

	public Respuesta crearPersona(Persona persona);

	public Respuesta actualizarPersona(Persona persona);

	public Persona cambiarEstado(Persona persona);
}
