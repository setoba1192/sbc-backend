package com.sbc.service;

import com.sbc.dto.Respuesta;
import com.sbc.model.ReporteOcasional;

public interface ReporteOcasionalService {
	
	public Respuesta crear(ReporteOcasional reporteOcasional);

}
