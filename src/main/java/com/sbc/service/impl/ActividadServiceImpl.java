package com.sbc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbc.dao.ActividadDao;
import com.sbc.dto.Respuesta;
import com.sbc.model.Actividad;
import com.sbc.model.Area;
import com.sbc.service.ActividadService;

@Service("ActividadServiceImpl")
public class ActividadServiceImpl implements ActividadService {

	@Autowired
	private ActividadDao actividadDao;

	public Respuesta registrarActividad(Area area, Actividad actividad) {
		// TODO Auto-generated method stub

		Respuesta respuesta = new Respuesta();

		if (area == null || area.getAre_codigo() == 0) {

			respuesta.setMensaje("Debe seleccionar un área.");
			respuesta.setType(Respuesta.Type.ERROR);

			return respuesta;

		}

		if (actividad.getProcedimientos() == null || actividad.getProcedimientos().size() == 0) {

			respuesta.setMensaje("Debe agregar uno o más procedimientos.");
			respuesta.setType(Respuesta.Type.ERROR);

			return respuesta;
		}

		if (actividadDao.registrarActividad(area, actividad)) {

			respuesta.setMensaje("Actividad registrada exitosamente.");
			respuesta.setData(actividad);
			respuesta.setType(Respuesta.Type.SUCCESS);

		}

		return respuesta;
	}

	public Respuesta actualizarActividad(Area area, Actividad actividad) {
		// TODO Auto-generated method stub
		Respuesta respuesta = new Respuesta();

		if (area == null || area.getAre_codigo() == 0) {

			respuesta.setMensaje("Debe seleccionar un área.");
			respuesta.setType(Respuesta.Type.ERROR);

			return respuesta;

		}

		if (actividad.getProcedimientos() == null || actividad.getProcedimientos().size() == 0) {

			respuesta.setMensaje("Debe agregar uno o más procedimientos.");
			respuesta.setType(Respuesta.Type.ERROR);

			return respuesta;
		}

		if (actividadDao.actualizarActividad(area, actividad)) {

			respuesta.setMensaje("Actividad actualizada exitosamente.");
			respuesta.setData(actividad);
			respuesta.setType(Respuesta.Type.SUCCESS);

		}

		return respuesta;
	}

}
