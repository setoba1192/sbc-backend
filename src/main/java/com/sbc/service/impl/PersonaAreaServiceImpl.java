package com.sbc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbc.dao.ActividadDao;
import com.sbc.dao.PersonaAreaDao;
import com.sbc.dao.RecordatorioDao;
import com.sbc.dto.Respuesta;
import com.sbc.exception.EmptyCollectionException;
import com.sbc.model.Actividad;
import com.sbc.model.Persona;
import com.sbc.model.PersonaArea;
import com.sbc.service.PersonaAreaService;

@Service("PersonaAreaServiceImpl")
public class PersonaAreaServiceImpl implements PersonaAreaService {

	@Autowired
	private PersonaAreaDao personaAreaDao;

	@Autowired
	private RecordatorioDao recordatorioDao;

	@Autowired
	private ActividadDao actividadDao;

	public Respuesta registrarAsignacion(PersonaArea personaArea) {
		// TODO Auto-generated method stub
		Respuesta respuesta = new Respuesta();

		if (personaAreaDao.areaAsignada(personaArea.getArea().getAre_codigo())) {

			respuesta.setMensaje("El área ya se encuentra asignada.");
			respuesta.setType(Respuesta.Type.WARNING);

			return respuesta;
		}

		if (personaAreaDao.personaAsignada(personaArea.getPersona().getPer_codigo())) {

			respuesta.setMensaje("El personal ya se encuentra con área asignada.");
			respuesta.setType(Respuesta.Type.WARNING);

			return respuesta;
		}

		if (personaArea.getActividades().size() == 0) {

			respuesta.setMensaje("Debe seleccionar al menos una Tarea Crítica.");
			respuesta.setType(Respuesta.Type.WARNING);

			return respuesta;
		}

		if (personaAreaDao.registrarAsignacion(personaArea.getPersona(), personaArea.getArea())) {
			actividadDao.inhabilitarActividadesEnPersona(personaArea.getPersona());
			registrarActividadesEnPersona(personaArea);

			respuesta.setMensaje("área asignada correctamente.");
			respuesta.setType(Respuesta.Type.SUCCESS);

			return respuesta;
		} else {

			respuesta.setMensaje("Ocurrió un error, intentelo de nuevo...");
			respuesta.setType(Respuesta.Type.SUCCESS);
			return respuesta;

		}
	}

	public Respuesta cambiarAsignacion(PersonaArea personaArea) {
		// TODO Auto-generated method stub
		Respuesta respuesta = new Respuesta();

		/*
		 * if (personaAreaDao.areaAsignada(personaArea.getArea().getAre_codigo())) {
		 * 
		 * respuesta.setMensaje("El área ya se encuentra asignada.");
		 * respuesta.setType(Respuesta.Type.WARNING);
		 * 
		 * return respuesta; }
		 */

		if (personaArea.getActividades().size() == 0) {

			respuesta.setMensaje("Debe seleccionar al menos una Tarea Crítica.");
			respuesta.setType(Respuesta.Type.WARNING);

			return respuesta;
		}

		personaAreaDao.cambiarAsignacion(personaArea);
		actividadDao.inhabilitarActividadesEnPersona(personaArea.getPersona());
		registrarActividadesEnPersona(personaArea);

		respuesta.setMensaje("Area re asignada exitosamente.");
		respuesta.setType(Respuesta.Type.SUCCESS);
		return respuesta;
	}

	@Override
	public PersonaArea getPersonaArea(Persona persona) {
		// TODO Auto-generated method stub

		PersonaArea personaArea = personaAreaDao.getPersonaArea(persona);

		if (personaArea == null) {

			throw new EmptyCollectionException("El usuario no tiene un área asignada.");
		}

		personaArea.setRecordatorios(recordatorioDao.listarPorPersonaArea(personaArea));

		return personaArea;
	}

	public void registrarActividadesEnPersona(PersonaArea personaArea) {

		for (Actividad actividad : personaArea.getActividades()) {

			if (actividadDao.validarActividadEnPersonaExistente(personaArea.getPersona(), actividad) > 0) {

				actividadDao.actualizarActividadEnPersona(personaArea.getPersona(), actividad);
			} else {

				actividadDao.registrarActividadEnPersona(personaArea.getPersona(), actividad);
			}

		}

	}

}
