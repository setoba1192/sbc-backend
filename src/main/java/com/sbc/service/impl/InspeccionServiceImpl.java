package com.sbc.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbc.dao.InspeccionDao;
import com.sbc.dao.InspeccionDetalleDao;
import com.sbc.dao.InspeccionRefuerzoCheckListDao;
import com.sbc.dao.RefuerzoDao;
import com.sbc.dto.Respuesta;
import com.sbc.model.Inspeccion;
import com.sbc.model.InspeccionTipo;
import com.sbc.model.Persona;
import com.sbc.service.InspeccionService;

@Service("InspeccionServiceImpl")
public class InspeccionServiceImpl implements InspeccionService {

	@Autowired
	private InspeccionDao inspeccionDao;

	@Autowired
	private InspeccionDetalleDao inspeccionDetalleDao;

	@Autowired
	private RefuerzoDao refuerzoDao;

	@Autowired
	private InspeccionRefuerzoCheckListDao inspeccionRefuerzoCheckListDao;

	@Override
	@Transactional
	public Respuesta registrarInspeccion(Inspeccion inspeccion) {
		// TODO Auto-generated method stub

		Respuesta respuesta = new Respuesta();

		if (inspeccion.getActividad() == null) {

			respuesta.setMensaje("Debe tener una tarea crítica.");
			respuesta.setType(Respuesta.Type.WARNING);
			return respuesta;

		}

		if (inspeccion.getArea() == null) {

			respuesta.setMensaje("Debe tener un area.");
			respuesta.setType(Respuesta.Type.WARNING);
			return respuesta;

		}
		if (inspeccion.getPersona() == null) {

			respuesta.setMensaje("Debe tener un responsable.");
			respuesta.setType(Respuesta.Type.WARNING);
			return respuesta;

		}

		if (inspeccion.getInspeccionTipo() == null) {

			respuesta.setMensaje("Debe tener un tipo de observación.");
			respuesta.setType(Respuesta.Type.WARNING);
			return respuesta;

		}

		inspeccionDao.registrarInspeccion(inspeccion);
		inspeccionDetalleDao.registrarDetalleInspeccion(inspeccion);
		refuerzoDao.registrarRefuerzo(inspeccion.getRefuerzo(), inspeccion);
		inspeccionRefuerzoCheckListDao.registrarCheckList(inspeccion.getRefuerzo().getCheckList(),
				inspeccion.getRefuerzo());

		respuesta.setMensaje("Se ha registrado la observación exitosamente.");
		respuesta.setType(Respuesta.Type.SUCCESS);
		return respuesta;

	}

	@Override
	public List<Inspeccion> listInspecciones(Persona persona, InspeccionTipo inspeccionTipo) {
		// TODO Auto-generated method stub
		return inspeccionDao.listInspeccion(persona, inspeccionTipo);
	}

}
