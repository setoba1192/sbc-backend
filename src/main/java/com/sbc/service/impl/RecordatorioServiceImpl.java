package com.sbc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbc.dao.RecordatorioDao;
import com.sbc.dto.Respuesta;
import com.sbc.dto.Respuesta.Type;
import com.sbc.model.Recordatorio;
import com.sbc.service.RecordatorioService;

@Service("RecordatorioServiceImpl")
public class RecordatorioServiceImpl implements RecordatorioService {

	@Autowired
	private RecordatorioDao recordatorioDao;

	@Override
	public Respuesta crear(Recordatorio recordatorio) {
		// TODO Auto-generated method stub
		Respuesta respuesta = new Respuesta();

		if (recordatorioDao.buscarPorDiaPersonaArea(recordatorio) != null) {

			respuesta.setMensaje("Error, recordatorio ya se encuentra creado.");
			respuesta.setType(Type.ERROR);
			return respuesta;

		}

		if (recordatorio.getRec_codigo() != 0) {

			respuesta.setMensaje("Error, el recordatorio no debe tener un id");
			respuesta.setType(Type.ERROR);
			return respuesta;
		}

		if (recordatorio.getArea() == null) {

			respuesta.setMensaje("Error, el recordatorio debe tener un área");
			respuesta.setType(Type.ERROR);
			return respuesta;
		}

		if (recordatorio.getPersona() == null) {

			respuesta.setMensaje("Error, el recordatorio debe tener una persona");
			respuesta.setType(Type.ERROR);
			return respuesta;
		}
		if (recordatorio.getDia() == null) {

			respuesta.setMensaje("Error, el recordatorio debe tener un día");
			respuesta.setType(Type.ERROR);
			return respuesta;
		}

		recordatorioDao.crear(recordatorio);
		respuesta.setData(buscarPorDiaPersonaArea(recordatorio));
		respuesta.setMensaje("Recordatorio establecido exitosamente.");
		respuesta.setType(Type.SUCCESS);
		return respuesta;
	}

	@Override
	public Respuesta actualizar(Recordatorio recordatorio) {
		// TODO Auto-generated method stub
		Respuesta respuesta = new Respuesta();

		if (recordatorio.getRec_codigo() == 0) {

			respuesta.setMensaje("Error, el recordatorio debe tener un id");
			respuesta.setType(Type.ERROR);
			return respuesta;
		}
		if (recordatorio.getArea() == null) {

			respuesta.setMensaje("Error, el recordatorio debe tener un área");
			respuesta.setType(Type.ERROR);
			return respuesta;
		}

		if (recordatorio.getPersona() == null) {

			respuesta.setMensaje("Error, el recordatorio debe tener una persona");
			respuesta.setType(Type.ERROR);
			return respuesta;
		}

		if (recordatorio.getDia() == null) {

			respuesta.setMensaje("Error, el recordatorio debe tener un día");
			respuesta.setType(Type.ERROR);
			return respuesta;
		}

		
		recordatorioDao.actualizar(recordatorio);
		respuesta.setData(buscarPorDiaPersonaArea(recordatorio));
		respuesta.setMensaje("Recordatorio establecido exitosamente.");
		respuesta.setType(Type.SUCCESS);
		return respuesta;
	}

	@Override
	public Recordatorio buscarPorDiaPersonaArea(Recordatorio recordatorio) {
		// TODO Auto-generated method stub
		return recordatorioDao.buscarPorDiaPersonaArea(recordatorio);
	}

}
