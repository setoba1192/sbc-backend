package com.sbc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbc.dao.PersonaDao;
import com.sbc.dao.UsuarioDao;
import com.sbc.dto.Respuesta;
import com.sbc.model.Persona;
import com.sbc.model.Usuario;
import com.sbc.service.PersonaService;

@Service("PersonaServiceImpl")
public class PersonaServiceImpl implements PersonaService {

	@Autowired
	private PersonaDao personaDao;

	@Autowired
	private UsuarioDao usuarioDao;

	public Respuesta crearPersona(Persona persona) {
		// TODO Auto-generated method stub

		Respuesta respuesta = new Respuesta();

		if (personaDao.buscarPorIdentificacion(persona.getPer_identificacion().trim()) != null) {

			respuesta.setMensaje("Personal ya se encuentra registrado con la misma identificación.");
			respuesta.setType(Respuesta.Type.WARNING);

			return respuesta;

		}

		if (personaDao.create(persona)) {

			usuarioDao.crearUsuario(persona);

			respuesta.setMensaje("Personal registrado exitosamente.");
			respuesta.setType(Respuesta.Type.SUCCESS);

			return respuesta;

		}

		respuesta.setMensaje("Ocurrió un error, intentelo de nuevo.");
		respuesta.setType(Respuesta.Type.ERROR);

		return respuesta;
	}

	public Respuesta actualizarPersona(Persona persona) {
		// TODO Auto-generated method stub
		Respuesta respuesta = new Respuesta();

		if (personaDao.buscarPorIdentificacion(persona.getPer_identificacion().trim()) != null) {

			respuesta.setMensaje("Personal ya se encuentra registrado con la misma identificación.");
			respuesta.setType(Respuesta.Type.WARNING);

			return respuesta;

		}

		if (personaDao.actualizarPersona(persona)) {

			respuesta.setMensaje("Personal actualizado exitosamente.");
			respuesta.setType(Respuesta.Type.SUCCESS);

			Usuario usuario = new Usuario();
			usuario.setPersona(persona);
			usuarioDao.actualizarUsuario(usuario);

			return respuesta;

		}

		respuesta.setMensaje("Ocurrió un error, intentelo de nuevo.");
		respuesta.setType(Respuesta.Type.ERROR);

		return respuesta;
	}

	public Persona cambiarEstado(Persona persona) {
		// TODO Auto-generated method stub
		return personaDao.cambiarEstado(persona);
	}

}
