package com.sbc.service.impl;

import java.io.UnsupportedEncodingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbc.dao.UsuarioDao;
import com.sbc.dto.Respuesta;
import com.sbc.model.Persona;
import com.sbc.model.Usuario;
import com.sbc.service.PersonaAreaService;
import com.sbc.service.UsuarioService;
import org.apache.commons.codec.binary.Base64;

@Service("UsuarioServiceImpl")
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioDao usuarioDao;

	@Autowired
	private PersonaAreaService personaAreaService;

	public Respuesta login(String auth) {
		// TODO Auto-generated method stub
		Respuesta respuesta = new Respuesta();

		try {
			String user = new String(Base64.decodeBase64(auth.split(":")[0]), "UTF-8");

			String password = new String(Base64.decodeBase64(auth.split(":")[1]), "UTF-8");

			if (user.equals("")) {
				respuesta.setMensaje("Debe ingresar el usuario.");
				respuesta.setType(Respuesta.Type.WARNING);
				return respuesta;
			}
			if (password.equals("")) {
				respuesta.setMensaje("Debe ingresar la contraseña.");
				respuesta.setType(Respuesta.Type.WARNING);
				return respuesta;
			}

			Usuario usuario = usuarioDao.usuarioPorUsuarioYClave(user, password);

			if (usuario == null) {
				respuesta.setMensaje("Usuario o contraseña incorrecta.");
				respuesta.setType(Respuesta.Type.ERROR);
				return respuesta;
			}

			respuesta.setMensaje("Ingreso exitoso.");
			respuesta.setLogin(true);
			respuesta.setType(Respuesta.Type.SUCCESS);
			respuesta.setData(personaAreaService.getPersonaArea(usuario.getPersona()));

			return respuesta;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return respuesta;
	}

	@Override
	public Respuesta getUserByAuth(Persona persona) {

		Respuesta respuesta = new Respuesta();

		respuesta.setMensaje("Ingreso exitoso.");
		respuesta.setLogin(true);
		respuesta.setType(Respuesta.Type.SUCCESS);
		respuesta.setData(personaAreaService.getPersonaArea(persona));
		return respuesta;
	}

	@Override
	public void crearUsuario(Persona persona) {

	}

}
