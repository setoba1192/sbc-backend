package com.sbc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbc.dao.AreaDao;
import com.sbc.dto.Respuesta;
import com.sbc.model.Area;
import com.sbc.service.AreaService;

@Service("AreaServiceImpl")
public class AreaServiceImpl implements AreaService {

	@Autowired
	private AreaDao areaDao;

	public Respuesta crearArea(Area area) {
		// TODO Auto-generated method stub

		Respuesta respuesta = new Respuesta();

		/*
		 * if (area.getNivelCritico() == null) {
		 * 
		 * respuesta.setMensaje("Debe seleccionar un Nivel Cr�tico.");
		 * respuesta.setType(Respuesta.Type.WARNING); return respuesta;
		 * 
		 * }
		 */
		if (areaDao.create(area)) {

			respuesta.setMensaje("Se ha creado el área exitosamente.");
			respuesta.setType(Respuesta.Type.SUCCESS);
			return respuesta;

		} else {

			respuesta.setMensaje("Ocurrió un error intentelo de nuevo...");
			respuesta.setType(Respuesta.Type.ERROR);

		}

		return null;
	}

	public Respuesta actualizarArea(Area area) {
		// TODO Auto-generated method stub

		Respuesta respuesta = new Respuesta();

		/*
		 * 
		 * if (area.getNivelCritico() == null) {
		 * 
		 * respuesta.setMensaje("Debe seleccionar un Nivel Cr�tico.");
		 * respuesta.setType(Respuesta.Type.WARNING); return respuesta;
		 * 
		 * }
		 */

		if (areaDao.actualizar(area))

		{

			respuesta.setMensaje("Se ha actualizado el área exitosamente.");
			respuesta.setType(Respuesta.Type.SUCCESS);
			return respuesta;

		} else {

			respuesta.setMensaje("Ocurrió un error intentelo de nuevo...");
			respuesta.setType(Respuesta.Type.ERROR);

		}
		return null;
	}

}
