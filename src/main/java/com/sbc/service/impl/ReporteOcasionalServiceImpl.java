package com.sbc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbc.dao.ReporteOcasionalDao;
import com.sbc.dto.Respuesta;
import com.sbc.model.ReporteOcasional;
import com.sbc.service.ReporteOcasionalService;

@Service("ReporteOcasionalServiceImpl")
public class ReporteOcasionalServiceImpl implements ReporteOcasionalService {

	@Autowired
	private ReporteOcasionalDao reporteOcasionalDao;

	@Override
	public Respuesta crear(ReporteOcasional reporteOcasional) {
		// TODO Auto-generated method stub
		Respuesta respuesta = new Respuesta();

		if (reporteOcasional.getReo_area().trim().isEmpty()) {

			respuesta.setMensaje("Debe ingresar un área.");
			respuesta.setType(Respuesta.Type.ERROR);
			return respuesta;
		}
		if (reporteOcasional.getReo_observacion().trim().isEmpty()) {

			respuesta.setMensaje("Debe ingresar una observacion.");
			respuesta.setType(Respuesta.Type.ERROR);
			return respuesta;
		}

		reporteOcasionalDao.crear(reporteOcasional);

		respuesta.setMensaje("Reporte enviado exitosamente.");
		respuesta.setData(reporteOcasional);
		respuesta.setType(Respuesta.Type.SUCCESS);
		return respuesta;
	}

}
