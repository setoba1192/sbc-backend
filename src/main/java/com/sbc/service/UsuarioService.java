package com.sbc.service;

import com.sbc.dto.Respuesta;
import com.sbc.model.Persona;

public interface UsuarioService {

	public Respuesta login(String auth);
	
	public void crearUsuario(Persona persona);
	
	public Respuesta getUserByAuth(Persona persona);

}
