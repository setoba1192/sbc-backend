package com.sbc.service;

import com.sbc.dto.Respuesta;
import com.sbc.model.Area;

public interface AreaService {

	public Respuesta crearArea(Area area);
	
	public Respuesta actualizarArea(Area area);

}
