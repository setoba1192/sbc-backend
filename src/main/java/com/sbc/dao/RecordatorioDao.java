package com.sbc.dao;

import java.util.List;

import com.sbc.model.PersonaArea;
import com.sbc.model.Recordatorio;

public interface RecordatorioDao {

	public void crear(Recordatorio recordatorio);
	
	public void actualizar(Recordatorio recordatorio);
	
	public Recordatorio buscarPorDiaPersonaArea(Recordatorio recordatorio);

	public List<Recordatorio> listarPorPersonaArea(PersonaArea personaArea);

	
}
