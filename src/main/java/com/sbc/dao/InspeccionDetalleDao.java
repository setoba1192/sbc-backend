package com.sbc.dao;

import java.util.List;

import com.sbc.model.Inspeccion;
import com.sbc.model.InspeccionDetalle;

public interface InspeccionDetalleDao {

	public void registrarDetalleInspeccion(Inspeccion inspeccion);

	public List<InspeccionDetalle> listInspeccionDetalle(Inspeccion inspeccion);

}
