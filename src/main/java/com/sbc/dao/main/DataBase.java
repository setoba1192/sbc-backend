package com.sbc.dao.main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import com.sbc.util.Parametros;

public class DataBase {

	/**
	 * Metodo que retorna la conexi�n a la base de datos
	 * 
	 * @return
	 */
	public Connection getConexion() {
		
		/*
		Connection conexion = null;
		String fuente_datos = "";
		try {

			fuente_datos = Parametros.datasource;
			Context ctx = new InitialContext();
			DataSource dataSource = null;

			dataSource = (DataSource) ctx.lookup(fuente_datos);
			conexion = dataSource.getConnection();

		} catch (Exception e) {
			e.printStackTrace();
		}*/
		return getConexionTemporal();
	}

	public Connection getConexionTemporal() {
		Connection conexion = null;

		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			// Setup the connection with the DB
			conexion = DriverManager
					.getConnection("jdbc:mysql://localhost:3306/sbc?" + "user=root&password=1234&serverTimezone=UTC");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return conexion;
	}

	/**
	 * Metodo para obtener el nombre del usuario logueado
	 */

	public static String usuarioLogueado(HttpServletRequest request) {

		String usuario = "";

		DataBase baseDato = new DataBase();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		try {
			connection = baseDato.getConexion();

			String sql = "select * from usuario, usuario_grupo where usuario.usu_codigo = usuario_grupo.usg_usuario and usuario.usu_login = ?";

			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, request.getUserPrincipal().toString());

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				usuario = resultSet.getString("usu_nombre") + " " + resultSet.getString("usu_apellido");

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			DataBase.cerrarObjeto(preparedStatement);
			DataBase.cerrarObjeto(resultSet);
			DataBase.cerrarObjeto(connection);
		}

		return usuario;
	}

	/**
	 * Metodo para cerrar un objeto de tipo ResultSet
	 * 
	 * @param resultSet
	 */
	public static void cerrarObjeto(ResultSet resultSet) {
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			// resultSet = null;
		}
	}

	/**
	 * Metodo para cerrar un objeto de tipo PreparedStatement
	 * 
	 * @param preparedStatement
	 */
	public static void cerrarObjeto(PreparedStatement preparedStatement) {
		if (preparedStatement != null) {
			try {
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			// preparedStatement = null;
		}
	}

	/**
	 * Metodo para cerrar un objeto de tipo Connection
	 * 
	 * @param connection
	 */

	public static void cerrarObjeto(Connection connection) {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException localSQLException5) {
			}
			// connection = null;
		}
	}

}