package com.sbc.dao;

import java.util.List;

import com.sbc.model.Area;
import com.sbc.model.Dia;

public interface AreaDao extends BaseDao {
	
	public boolean actualizar(Area area);
	
	public List<Dia> getDiasPorArea(Area area);

}
