package com.sbc.dao;

import com.sbc.model.RefuerzoCheckList;

public interface RefuerzoCheckListDao {

	public RefuerzoCheckList buscarPorId(int id);

}
