package com.sbc.dao;

import com.sbc.model.Persona;

public interface PersonaDao extends BaseDao {

	public boolean actualizarPersona(Persona persona);

	public Persona cambiarEstado(Persona persona);
	
	public Persona buscarPorIdentificacion(String identificacion);

}
