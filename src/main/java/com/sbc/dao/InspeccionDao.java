package com.sbc.dao;

import java.util.Date;
import java.util.List;

import com.sbc.model.Inspeccion;
import com.sbc.model.InspeccionTipo;
import com.sbc.model.Persona;

public interface InspeccionDao {

	public void registrarInspeccion(Inspeccion inspeccion);

	public List<Inspeccion> listInspeccion(Persona persona, InspeccionTipo inspeccionTipo);

	public List<Inspeccion> listPorFechas(Date fechaInicio, Date fechaFin, int tipo);

}
