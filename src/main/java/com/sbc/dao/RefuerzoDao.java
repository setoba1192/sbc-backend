package com.sbc.dao;

import com.sbc.model.Inspeccion;
import com.sbc.model.Refuerzo;

public interface RefuerzoDao {

	public Refuerzo buscarPorInspeccion(long idInspeccion);

	public void registrarRefuerzo(Refuerzo refuerzo, Inspeccion inspeccion);

}
