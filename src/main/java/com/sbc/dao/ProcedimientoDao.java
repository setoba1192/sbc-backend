package com.sbc.dao;

import java.util.List;

import com.sbc.model.Actividad;
import com.sbc.model.Procedimiento;

public interface ProcedimientoDao {

	public List<Procedimiento> procedimientos(Actividad actividad);
	
	public Procedimiento getProcedimiento(int pro_codigo);

}
