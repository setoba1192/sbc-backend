package com.sbc.dao;

import com.sbc.model.Persona;
import com.sbc.model.Usuario;

public interface UsuarioDao {

	public Usuario usuarioPorUsuarioYClave(String user, String password);


	public void crearUsuario(Persona persona);
	
	public void actualizarPassword(Usuario usuario);
	
	public void actualizarUsuario(Usuario usuario);
}
