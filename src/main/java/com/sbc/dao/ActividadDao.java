package com.sbc.dao;

import java.util.List;

import com.sbc.model.Actividad;
import com.sbc.model.Area;
import com.sbc.model.Persona;

public interface ActividadDao {

	public List<Actividad> getActividades(Area area);

	public List<Actividad> getActividadesPorPersona(int personaId);

	public Actividad getActividad(int act_codigo);

	public boolean registrarActividad(Area area, Actividad actividad);

	public boolean actualizarActividad(Area area, Actividad actividad);

	public int validarActividadEnPersonaExistente(Persona persona, Actividad actividad);

	public void actualizarActividadEnPersona(Persona persona, Actividad actividad);

	public void inhabilitarActividadesEnPersona(Persona persona);

	public void registrarActividadEnPersona(Persona persona, Actividad actividad);

}
