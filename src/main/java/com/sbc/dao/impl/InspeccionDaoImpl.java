package com.sbc.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.sbc.dao.ActividadDao;
import com.sbc.dao.AreaDao;
import com.sbc.dao.InspeccionDao;
import com.sbc.dao.InspeccionDetalleDao;
import com.sbc.dao.InspeccionTipoDao;
import com.sbc.dao.PersonaDao;
import com.sbc.dao.RefuerzoDao;
import com.sbc.dao.mapper.InspeccionRowMapper;
import com.sbc.model.Inspeccion;
import com.sbc.model.InspeccionTipo;
import com.sbc.model.Persona;

@Repository("InspeccionDaoImpl")
public class InspeccionDaoImpl implements InspeccionDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private String sql;

	@Autowired
	private ActividadDao actividadDao;
	@Autowired
	private AreaDao areaDao;
	@Autowired
	private PersonaDao personaDao;
	@Autowired
	private InspeccionTipoDao inspeccionTipoDao;
	@Autowired
	private InspeccionDetalleDao inspeccionDetalleDao;
	@Autowired
	private RefuerzoDao refuerzoDao;

	@Override
	public void registrarInspeccion(final Inspeccion inspeccion) {
		// TODO Auto-generated method stub

		sql = "INSERT INTO inspeccion (ins_fecha,ins_actividad,ins_area,ins_persona,ins_observacion,ins_tipo,ins_estado)";
		sql = sql + "values (now(),?,?,?,?,?,?);";

		KeyHolder key = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				final PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, inspeccion.getActividad().getAct_codigo());
				ps.setInt(2, inspeccion.getArea().getAre_codigo());
				ps.setInt(3, inspeccion.getPersona().getPer_codigo());
				ps.setString(4, inspeccion.getIns_observacion());
				ps.setInt(5, inspeccion.getInspeccionTipo().getInt_codigo());
				ps.setBoolean(6, true);
				return ps;
			}
		}, key);

		inspeccion.setIns_codigo(key.getKey().longValue());

	}

	@Override
	public List<Inspeccion> listInspeccion(Persona persona, InspeccionTipo inspeccionTipo) {
		// TODO Auto-generated method stub
		sql = "select * from inspeccion where ins_tipo = ? and ins_persona = ?;";

		InspeccionRowMapper mapper = new InspeccionRowMapper();
		mapper.setActividadDao(actividadDao);
		mapper.setAreaDao(areaDao);
		mapper.setInspeccionTipoDao(inspeccionTipoDao);
		mapper.setPersonaDao(personaDao);
		mapper.setInspeccionDetalleDao(inspeccionDetalleDao);
		mapper.setRefuerzoDao(refuerzoDao);
		
		return jdbcTemplate.query(sql, new Object[] { inspeccionTipo.getInt_codigo(), persona.getPer_codigo() },
				mapper);
	}

	@Override
	public List<Inspeccion> listPorFechas(Date fechaInicio, Date fechaFin, int tipo) {
		// TODO Auto-generated method stub
		sql = "select * from inspeccion where ins_fecha between ? and ? and ins_tipo = ?;";

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

		InspeccionRowMapper mapper = new InspeccionRowMapper();
		mapper.setActividadDao(actividadDao);
		mapper.setAreaDao(areaDao);
		mapper.setInspeccionTipoDao(inspeccionTipoDao);
		mapper.setPersonaDao(personaDao);
		mapper.setInspeccionDetalleDao(inspeccionDetalleDao);
		mapper.setRefuerzoDao(refuerzoDao);

		return jdbcTemplate.query(sql,
				new Object[] { format.format(fechaInicio) + " 00:00:00", format.format(fechaFin) + " 23:59:59", tipo },
				mapper);

	}

}
