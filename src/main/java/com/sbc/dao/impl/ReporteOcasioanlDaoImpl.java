package com.sbc.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.sbc.dao.ReporteOcasionalDao;
import com.sbc.dao.mapper.ReporteOcasionalExtractor;
import com.sbc.model.ReporteOcasional;

@Repository("ReporteOcasioanlDaoImpl")
public class ReporteOcasioanlDaoImpl implements ReporteOcasionalDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private String sql;

	@Override
	public void crear(final ReporteOcasional reporteOcasional) {
		// TODO Auto-generated method stub
		sql = "INSERT INTO reporte_ocasional (reo_area, reo_observacion) ";
		sql = sql + "values (?,?);";

		KeyHolder key = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				final PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, reporteOcasional.getReo_area());
				ps.setString(2, reporteOcasional.getReo_observacion());
				return ps;
			}
		}, key);

		reporteOcasional.setReo_codigo(key.getKey().intValue());

	}

	@Override
	public List<ReporteOcasional> listPorFechas(Date fechaInicio, Date fechaFin) {
		// TODO Auto-generated method stub
		sql = "SELECT * FROM reporte_ocasional where reo_fecha between ? and ?;";

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

		return jdbcTemplate.query(sql,
				new Object[] { format.format(fechaInicio) + " 00:00:00", format.format(fechaFin) + " 23:59:59" },
				new ReporteOcasionalExtractor());

	}

}
