package com.sbc.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.sbc.dao.DiaDao;
import com.sbc.dao.RecordatorioDao;
import com.sbc.dao.mapper.RecordatorioExtractor;
import com.sbc.model.PersonaArea;
import com.sbc.model.Recordatorio;

@Repository("RecordatorioDaoImpl")
public class RecordatorioDaoImpl implements RecordatorioDao {

	@Autowired
	private DiaDao diaDao;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private String sql;

	@Override
	public void crear(final Recordatorio recordatorio) {
		// TODO Auto-generated method stub

		sql = " INSERT INTO recordatorio (rec_dia,rec_hora,rec_area,rec_persona,rec_fecha) ";
		sql = sql + "values (?,?,?,?, now());";

		KeyHolder key = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				final PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, recordatorio.getDia().getDia_codigo());
				ps.setTimestamp(2, recordatorio.getRec_hora());
				ps.setInt(3, recordatorio.getArea().getAre_codigo());
				ps.setInt(4, recordatorio.getPersona().getPer_codigo());
				return ps;
			}
		}, key);

		recordatorio.setRec_codigo(key.getKey().intValue());

	}

	@Override
	public void actualizar(Recordatorio recordatorio) {
		// TODO Auto-generated method stub
		sql = " UPDATE recordatorio set rec_hora = ?, rec_activo = ? where rec_codigo = ? and rec_persona = ? and rec_area = ?;";

		jdbcTemplate.update(sql,
				new Object[] { recordatorio.getRec_hora(), recordatorio.isActivo(), recordatorio.getRec_codigo(),
						recordatorio.getPersona().getPer_codigo(), recordatorio.getArea().getAre_codigo() });

	}

	@Override
	public List<Recordatorio> listarPorPersonaArea(PersonaArea personaArea) {
		// TODO Auto-generated method stub

		String sql = "SELECT * FROM recordatorio where rec_persona =  ? and rec_area = ?;";

		RecordatorioExtractor extractor = new RecordatorioExtractor();
		extractor.setDiaDao(diaDao);

		return jdbcTemplate.query(sql,
				new Object[] { personaArea.getPersona().getPer_codigo(), personaArea.getArea().getAre_codigo() },
				extractor);
	}

	@Override
	public Recordatorio buscarPorDiaPersonaArea(Recordatorio recordatorio) {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM recordatorio where rec_persona =  ? and rec_area = ? and rec_dia = ?;";

		RecordatorioExtractor extractor = new RecordatorioExtractor();
		extractor.setDiaDao(diaDao);

		List<Recordatorio> recordatorios = jdbcTemplate
				.query(sql,
						new Object[] { recordatorio.getPersona().getPer_codigo(),
								recordatorio.getArea().getAre_codigo(), recordatorio.getDia().getDia_codigo() },
						extractor);

		return recordatorios.size() == 0 ? null : recordatorios.get(0);
	}

}
