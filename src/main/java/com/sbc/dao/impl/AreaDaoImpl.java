package com.sbc.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.sbc.dao.ActividadDao;
import com.sbc.dao.AreaDao;
import com.sbc.dao.NivelCriticoDao;
import com.sbc.dao.PersonaAreaDao;
import com.sbc.dao.mapper.AreaRowMapper;
import com.sbc.model.Area;
import com.sbc.model.Dia;

@Repository("AreaDaoImpl")
public class AreaDaoImpl implements AreaDao {

	@Autowired
	private PersonaAreaDao personaAreaDao;

	@Autowired
	private NivelCriticoDao nivelCriticoDao;

	@Autowired
	private ActividadDao actividadDao;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private DataSource dataSource;

	public Object get(int id) {
		// TODO Auto-generated method stub
		String sql = "select * from area where are_codigo = ?;";

		AreaRowMapper mapper = new AreaRowMapper();

		mapper.setActividadDao(actividadDao);
		mapper.setAreaDao(this);
		mapper.setNivelCriticoDao(nivelCriticoDao);
		mapper.setPersonaAreaDao(personaAreaDao);

		return jdbcTemplate.queryForObject(sql, new Object[] { id }, mapper);
	}

	public boolean create(Object object) {

		Area area = (Area) object;

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try (Connection connection = dataSource.getConnection()) {
			connection.setAutoCommit(false);

			String sql = "INSERT INTO area (are_nombre,are_nivelcritico, are_descripcion) ";
			sql += "VALUES (?,?,?);";

			preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			preparedStatement.setString(1, area.getAre_nombre());
			// preparedStatement.setInt(2, area.getNivelCritico().getNic_codigo());
			preparedStatement.setNull(2, Types.INTEGER);
			preparedStatement.setString(3, area.getAre_descripcion());

			preparedStatement.executeUpdate();

			resultSet = preparedStatement.getGeneratedKeys();

			if (resultSet.next()) {
				area.setAre_codigo(resultSet.getInt(1));
			}

			if (validarDiasRegistrados(area.getAre_codigo())) {

				System.out.println("actualizar");
				if (!actualizarDias(area, connection)) {
					connection.rollback();
					return false;
				}

			} else {
				System.out.println("registrar");

				if (!registrarDias(area, connection)) {
					connection.rollback();
					return false;
				}
			}

			connection.commit();

		} catch (SQLException e) {

			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean registrarDias(Area area, Connection connection_) {

		PreparedStatement preparedStatement = null;

		try (Connection connection = dataSource.getConnection()) {
			String sql = "INSERT INTO area_dia (ard_area, ard_dia, ard_laboral) ";
			sql += "VALUES (?,?,?);";

			preparedStatement = connection.prepareStatement(sql);

			for (Dia dia : area.getDias()) {

				preparedStatement.setInt(1, area.getAre_codigo());
				preparedStatement.setInt(2, dia.getDia_codigo());
				preparedStatement.setBoolean(3, dia.isLaboral());

				preparedStatement.addBatch();

			}
			preparedStatement.executeBatch();

			return true;
		} catch (

		SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean actualizarDias(Area area, Connection connection_) {

		PreparedStatement preparedStatement = null;

		try (Connection connection = dataSource.getConnection()) {

			String sql = "UPDATE area_dia set ard_laboral = ? where ard_codigo = ? and ard_area = ?;";

			preparedStatement = connection.prepareStatement(sql);

			for (Dia dia : area.getDias()) {

				preparedStatement.setBoolean(1, dia.isLaboral());
				preparedStatement.setInt(2, dia.getArd_codigo());
				preparedStatement.setInt(3, area.getAre_codigo());

				preparedStatement.addBatch();

			}
			preparedStatement.executeBatch();

			return true;
		} catch (

		SQLException e) {
			e.printStackTrace();
			return false;
		}

	}

	public List<Object> getAll() {
		// TODO Auto-generated method stub

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		List<Object> areaList = new ArrayList<Object>();

		try (Connection connection = dataSource.getConnection()) {

			String sql = "Select * from area order by are_codigo desc;";

			preparedStatement = connection.prepareStatement(sql);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				Area area = new Area();

				area.setAre_codigo(resultSet.getInt("are_codigo"));
				area.setAre_nombre(resultSet.getString("are_nombre"));
				area.setAre_descripcion(resultSet.getString("are_descripcion"));
				area.setAsignacion(personaAreaDao.areaAsignada(area.getAre_codigo()));

				// area.setNivelCritico((NivelCritico)
				// nivelCriticoDao.get(resultSet.getInt("are_nivelcritico")));

				area.setActividades(actividadDao.getActividades(area));

				area.setDias(getDiasPorArea(area));

				areaList.add(area);

			}

		} catch (SQLException e) {
			e.printStackTrace();

		}

		return areaList;
	}

	public boolean actualizar(Area area) {

		PreparedStatement preparedStatement = null;

		try (Connection connection = dataSource.getConnection()) {

			String sql = "UPDATE area  SET are_nombre = ?, are_nivelcritico = ?, are_descripcion = ? WHERE are_codigo = ?;";

			preparedStatement = connection.prepareStatement(sql);

			preparedStatement.setString(1, area.getAre_nombre());
			preparedStatement.setNull(2, Types.INTEGER);
			preparedStatement.setString(3, area.getAre_descripcion());
			preparedStatement.setInt(4, area.getAre_codigo());

			preparedStatement.executeUpdate();

			if (validarDiasRegistrados(area.getAre_codigo())) {

				System.out.println("actualizar");
				if (!actualizarDias(area, connection)) {
					connection.rollback();
					return false;
				}

			} else {
				System.out.println("registrar");

				if (!registrarDias(area, connection)) {
					connection.rollback();
					return false;
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean validarDiasRegistrados(int idArea) {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try (Connection connection = dataSource.getConnection()) {
			String sql = "SELECT COUNT(*) as dias FROM area_dia WHERE ard_area = ?;";

			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, idArea);

			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {

				int dias = resultSet.getInt("dias");

				if (dias > 0) {

					return true;
				}

			}

		} catch (SQLException e) {
			e.printStackTrace();

		}
		return false;

	}

	public List<Dia> getDiasPorArea(Area area) {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		List<Dia> dias = new ArrayList<Dia>();

		try (Connection connection = dataSource.getConnection()) {

			String sql = "SELECT * FROM dia LEFT JOIN area_dia ON dia.dia_codigo = area_dia.ard_dia AND area_dia.ard_area = ?;";

			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, area.getAre_codigo());

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				Dia dia = new Dia();

				dia.setDia_codigo(resultSet.getInt("dia_codigo"));
				dia.setDia_nombre(resultSet.getString("dia_nombre"));
				dia.setArd_codigo(resultSet.getInt("ard_codigo"));
				dia.setLaboral(resultSet.getBoolean("ard_laboral"));

				dias.add(dia);

			}

		} catch (SQLException e) {
			e.printStackTrace();

		}
		return dias;

	}

}
