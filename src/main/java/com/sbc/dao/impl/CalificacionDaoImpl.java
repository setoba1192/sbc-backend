package com.sbc.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.sbc.dao.CalificacionDao;
import com.sbc.dao.mapper.CalificacionRowMapper;
import com.sbc.model.Calificacion;

@Repository("CalificacionDaoImpl")
public class CalificacionDaoImpl implements CalificacionDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public Calificacion getCalificacion(int cal_codigo) {
		// TODO Auto-generated method stub
		String sql = "select * from calificacion where cal_codigo = ?;";
		return jdbcTemplate.queryForObject(sql, new Object[] { cal_codigo }, new CalificacionRowMapper());
	}

}
