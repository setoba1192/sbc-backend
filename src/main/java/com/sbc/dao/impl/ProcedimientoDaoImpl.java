package com.sbc.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.sbc.dao.ProcedimientoDao;
import com.sbc.dao.mapper.ProcedimientoExtractor;
import com.sbc.dao.mapper.ProcedimientoRowMapper;
import com.sbc.model.Actividad;
import com.sbc.model.Procedimiento;

@Repository
public class ProcedimientoDaoImpl implements ProcedimientoDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public List<Procedimiento> procedimientos(Actividad actividad) {
		// TODO Auto-generated method stub

		String sql = "Select * from procedimiento where pro_actividad = ? and pro_estado is true order by pro_orden;";

		return jdbcTemplate.query(sql, new Object[] { actividad.getAct_codigo() }, new ProcedimientoExtractor());
	}

	@Override
	public Procedimiento getProcedimiento(int pro_codigo) {
		// TODO Auto-generated method stub
		String sql = "select * from procedimiento where pro_codigo= ?";

		return jdbcTemplate.queryForObject(sql, new Object[] { pro_codigo }, new ProcedimientoRowMapper());
	}

}
