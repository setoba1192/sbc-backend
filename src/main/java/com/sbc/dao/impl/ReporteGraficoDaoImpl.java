package com.sbc.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.sbc.dao.ReporteGraficoDao;
import com.sbc.dao.mapper.ReporteGraficoExtractor;
import com.sbc.model.ReporteGrafico;

@Repository
public class ReporteGraficoDaoImpl implements ReporteGraficoDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<ReporteGrafico> getResumenAnualObservacion() {
		// TODO Auto-generated method stub

		String sql = "select sum(ind_calificacion) as valor, ind_calificacion, cal_nombre as label from inspeccion_detalle, calificacion ";
		sql += "where ind_calificacion in(1,2,3) ";
		sql += "and ind_calificacion = calificacion.cal_codigo group by ind_calificacion, cal_nombre;";
		return jdbcTemplate.query(sql, new ReporteGraficoExtractor());
	}

}
