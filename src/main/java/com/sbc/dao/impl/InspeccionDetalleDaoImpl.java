package com.sbc.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.sbc.dao.CalificacionDao;
import com.sbc.dao.InspeccionDetalleDao;
import com.sbc.dao.ProcedimientoDao;
import com.sbc.dao.mapper.InspeccionDetalleRowMapper;
import com.sbc.model.Inspeccion;
import com.sbc.model.InspeccionDetalle;

@Repository("InspeccionDetalleDaoImpl")
public class InspeccionDetalleDaoImpl implements InspeccionDetalleDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private CalificacionDao calificacionDao;
	@Autowired
	private ProcedimientoDao procedimientoDao;

	@Override
	public void registrarDetalleInspeccion(final Inspeccion inspeccion) {
		// TODO Auto-generated method stub

		String sql = "INSERT INTO inspeccion_detalle (ind_procedimiento,ind_observacion,ind_calificacion,ind_inspeccion) ";
		sql += "VALUES (?,?,?,?) ";

		jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				InspeccionDetalle inDetalle = inspeccion.getInspeccionDetalles().get(i);
				ps.setLong(1, inDetalle.getProcedimiento().getPro_codigo());
				ps.setString(2, inDetalle.getInd_observacion());
				ps.setInt(3, inDetalle.getCalificacion().getCal_codigo());
				ps.setLong(4, inspeccion.getIns_codigo());
			}

			@Override
			public int getBatchSize() {
				return inspeccion.getInspeccionDetalles().size();
			}
		});

	}

	@Override
	public List<InspeccionDetalle> listInspeccionDetalle(Inspeccion inspeccion) {
		// TODO Auto-generated method stub
		String sql = "select * from inspeccion_detalle where ind_inspeccion = ?;";

		InspeccionDetalleRowMapper mapper = new InspeccionDetalleRowMapper();
		mapper.setCalificacionDao(calificacionDao);
		mapper.setProcedimientoDao(procedimientoDao);

		return jdbcTemplate.query(sql, new Object[] { inspeccion.getIns_codigo() }, mapper);
	}

}
