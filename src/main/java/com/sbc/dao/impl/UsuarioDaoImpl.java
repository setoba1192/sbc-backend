package com.sbc.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.sbc.dao.PersonaDao;
import com.sbc.dao.UsuarioDao;
import com.sbc.dao.mapper.UsuarioRowMapper;
import com.sbc.model.Persona;
import com.sbc.model.Usuario;

@Repository("UsuarioDaoImpl")
public class UsuarioDaoImpl implements UsuarioDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private PersonaDao personaDao;

	public Usuario usuarioPorUsuarioYClave(String user, String password) {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM usuario  WHERE usuario.user = ? AND usuario.password = ? and user_type = 4;";

		UsuarioRowMapper mapper = new UsuarioRowMapper();
		mapper.setPersonaDao(personaDao);

		try {
			return jdbcTemplate.queryForObject(sql, new Object[] { user, password }, mapper);
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	@Override
	public void crearUsuario(final Persona persona) {
		// TODO Auto-generated method stub

		final String password = persona.getPer_identificacion().trim()
				.substring(persona.getPer_identificacion().length() - 6, persona.getPer_identificacion().length());

		KeyHolder key = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator() {

			String sql = "INSERT INTO usuario (user,password,user_type,person) VALUES (?,?,?,?)";

			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				final PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, persona.getPer_identificacion().trim());
				ps.setString(2, password);
				ps.setInt(3, 4);
				ps.setInt(4, persona.getPer_codigo());
				return ps;
			}
		}, key);

		// return key.getKey().longValue();
	}

	@Override
	public void actualizarPassword(Usuario usuario) {
		// TODO Auto-generated method stub

		String sql = "update usuario set password = ? where id = ? and user = ? and person = ?;";

		jdbcTemplate.update(sql, new Object[] { usuario.getPassword(), usuario.getId(), usuario.getUser(),
				usuario.getPersona().getPer_codigo() });

	}

	@Override
	public void actualizarUsuario(Usuario usuario) {
		// TODO Auto-generated method stub
		String sql = "update usuario set user = ? where person = ?;";

		jdbcTemplate.update(sql, new Object[] { usuario.getPersona().getPer_identificacion().trim(),
				usuario.getPersona().getPer_codigo() });

	}

}
