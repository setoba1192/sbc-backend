package com.sbc.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.sbc.dao.DiaDao;
import com.sbc.dao.mapper.DiaRowMapper;
import com.sbc.model.Dia;

@Repository("DiaDaoImpl")
public class DiaDaoImpl implements DiaDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public Dia buscarPorId(int dia_codigo) {
		// TODO Auto-generated method stub
		String sql = "select * from dia where dia_codigo = ?;";

		return jdbcTemplate.queryForObject(sql, new Object[] { dia_codigo }, new DiaRowMapper());
	}

}
