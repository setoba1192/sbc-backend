package com.sbc.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.sbc.dao.ReporteDao;
import com.sbc.dao.mapper.ReporteMesResultSetExtractor;
import com.sbc.model.ReporteMes;

@Repository("ReporteDaoImpl")
public class ReporteDaoImpl implements ReporteDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<ReporteMes> getReporteMensual() {
		// TODO Auto-generated method stub
		String sql = "SELECT  monthname(ins.ins_fecha) as mes, ";
		sql += "COUNT(CASE WHEN ind.ind_calificacion =1 THEN 1 ELSE NULL END) AS cumple, ";
		sql += "COUNT(CASE WHEN ind.ind_calificacion =2 THEN 1 ELSE NULL END) AS nocumple, ";
		sql += "COUNT(CASE WHEN ind.ind_calificacion =3 THEN 1 ELSE NULL END) AS noaplica ";
		sql += "FROM inspeccion_detalle ind, inspeccion ins ";
		sql += "where ind.ind_inspeccion = ins.ins_codigo and ind.ind_calificacion in (1,2,3) ";
		sql += "GROUP BY YEAR(now()), MONTH(ins.ins_fecha) ";

		return jdbcTemplate.query(sql, new ReporteMesResultSetExtractor());
	}

}
