package com.sbc.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.sbc.dao.RefuerzoCheckListDao;
import com.sbc.dao.mapper.RefuerzoCheckListRowMapper;
import com.sbc.model.RefuerzoCheckList;

@Repository("RefuerzoCheckListDaoImpl")
public class RefuerzoCheckListDaoImpl implements RefuerzoCheckListDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public RefuerzoCheckList buscarPorId(int id) {
		// TODO Auto-generated method stub

		String sql = "select * from refuerzo_checklist where rec_codigo = ?;";

		return jdbcTemplate.queryForObject(sql, new Object[] { id }, new RefuerzoCheckListRowMapper());
	}

}
