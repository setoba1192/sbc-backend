package com.sbc.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.sbc.dao.ActividadDao;
import com.sbc.dao.AreaDao;
import com.sbc.dao.PersonaAreaDao;
import com.sbc.dao.PersonaDao;
import com.sbc.dao.mapper.PersonaAreaRowMapper;
import com.sbc.model.Area;
import com.sbc.model.Persona;
import com.sbc.model.PersonaArea;

@Repository("PersonaAreaDaoImpl")
public class PersonaAreaDaoImpl implements PersonaAreaDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private PersonaDao personaDao;
	@Autowired
	private AreaDao areaDao;
	@Autowired
	private ActividadDao actividadDao;

	@Autowired
	private DataSource dataSource;

	public boolean personaAsignada(int per_codigo) {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try (Connection connection = dataSource.getConnection()) {

			String sql = "Select * from persona_area where pea_persona = ?;";

			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, per_codigo);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				return true;

			}

		} catch (SQLException e) {
			e.printStackTrace();

		}

		return false;
	}

	public boolean areaAsignada(int are_codigo) {
		// TODO Auto-generated method stub

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try (Connection connection = dataSource.getConnection()) {

			String sql = "SELECT * FROM persona_area, persona WHERE pea_area = ? AND pea_persona = per_codigo AND per_estado = 1;";

			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, are_codigo);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				return true;

			}

		} catch (SQLException e) {
			e.printStackTrace();

		}
		return false;
	}

	public boolean registrarAsignacion(Persona persona, Area area) {
		// TODO Auto-generated method stub

		PreparedStatement preparedStatement = null;

		try (Connection connection = dataSource.getConnection()) {

			String sql = "INSERT INTO persona_area (pea_area,pea_persona) ";
			sql += "VALUES (?,?);";

			preparedStatement = connection.prepareStatement(sql);

			preparedStatement.setInt(1, area.getAre_codigo());
			preparedStatement.setInt(2, persona.getPer_codigo());

			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public List<PersonaArea> personasAreasList() {
		// TODO Auto-generated method stub
		String sql = "select * from persona_area;";

		PersonaAreaRowMapper mapper = new PersonaAreaRowMapper();
		mapper.setAreaDao(areaDao);
		mapper.setPersonaDao(personaDao);
		mapper.setActividadDao(actividadDao);

		return jdbcTemplate.query(sql, mapper);
	}

	public void cambiarAsignacion(PersonaArea personaArea) {
		// TODO Auto-generated method stub
		String sql = "update persona_area set pea_area = ? where pea_codigo = ?;";
		jdbcTemplate.update(sql, new Object[] { personaArea.getArea().getAre_codigo(), personaArea.getPea_codigo() });

	}

	@Override
	public PersonaArea getPersonaArea(Persona persona) {
		// TODO Auto-generated method stub
		String sql = "select * from persona_area where pea_persona = ?;";

		PersonaAreaRowMapper mapper = new PersonaAreaRowMapper();
		mapper.setAreaDao(areaDao);
		mapper.setPersonaDao(personaDao);
		mapper.setActividadDao(actividadDao);

		List<PersonaArea> personaAreas = jdbcTemplate.query(sql, new Object[] { persona.getPer_codigo() }, mapper);

		return personaAreas.size() > 0 ? personaAreas.get(0) : null;

	}

}
