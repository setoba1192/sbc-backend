package com.sbc.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.sbc.dao.ActividadDao;
import com.sbc.dao.ProcedimientoDao;
import com.sbc.dao.mapper.ActividadExtractor;
import com.sbc.dao.mapper.ActividadRowMapper;
import com.sbc.model.Actividad;
import com.sbc.model.Area;
import com.sbc.model.Persona;
import com.sbc.model.Procedimiento;

@Repository("ActividadDaoImpl")
public class ActividadDaoImpl implements ActividadDao {

	@Autowired
	private ProcedimientoDao procedimientoDao;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private DataSource dataSource;

	private String sql;

	public List<Actividad> getActividades(Area area) {
		// TODO Auto-generated method stub

		PreparedStatement preparedStatement = null;
		ResultSet rs = null;

		List<Actividad> actividadList = new ArrayList<Actividad>();

		try (Connection connection = dataSource.getConnection()) {

			String sql = "Select * from actividad where act_area = ? and act_estado = 1;";

			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, area.getAre_codigo());

			rs = preparedStatement.executeQuery();

			while (rs.next()) {

				Actividad actividad = new Actividad();

				actividad.setAct_codigo(rs.getInt("act_codigo"));
				actividad.setAct_nombre(rs.getString("act_nombre"));
				actividad.setAct_descripcion(rs.getString("act_descripcion"));
				actividad.setAct_estado(rs.getBoolean("act_estado"));
				actividad.setProcedimientosCount(totalProcedimientos(actividad));
				actividad.setProcedimientos(procedimientoDao.procedimientos(actividad));

				actividadList.add(actividad);

			}

		} catch (SQLException e) {
			e.printStackTrace();

		}

		return actividadList;
	}

	public boolean registrarActividad(Area area, Actividad actividad) {
		// TODO Auto-generated method stub
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try (Connection connection = dataSource.getConnection()) {
			connection.setAutoCommit(false);

			String sql = "INSERT INTO actividad (act_nombre,act_descripcion, act_area) ";
			sql += "VALUES (?,?,?);";

			preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			preparedStatement.setString(1, actividad.getAct_nombre());
			preparedStatement.setString(2, actividad.getAct_descripcion());
			preparedStatement.setInt(3, area.getAre_codigo());

			preparedStatement.executeUpdate();

			resultSet = preparedStatement.getGeneratedKeys();

			if (resultSet.next()) {
				actividad.setAct_codigo(resultSet.getInt(1));
			}

			if (registrarProcedimientos(actividad, connection)) {

				connection.commit();
				actividad.setProcedimientosCount(totalProcedimientos(actividad));

				return true;
			} else {

				connection.rollback();

			}

		} catch (SQLException e) {

			e.printStackTrace();
			return false;
		}
		return false;
	}

	public boolean actualizarActividad(Area area, Actividad actividad) {
		// TODO Auto-generated method stub

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try (Connection connection = dataSource.getConnection()) {
			connection.setAutoCommit(false);

			String sql = "UPDATE actividad set act_nombre = ?, act_descripcion = ? where  act_area = ? and act_codigo= ?;";

			preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			preparedStatement.setString(1, actividad.getAct_nombre());
			preparedStatement.setString(2, actividad.getAct_descripcion());
			preparedStatement.setInt(3, area.getAre_codigo());
			preparedStatement.setInt(4, actividad.getAct_codigo());

			preparedStatement.executeUpdate();

			resultSet = preparedStatement.getGeneratedKeys();

			if (resultSet.next()) {
				actividad.setAct_codigo(resultSet.getInt(1));
			}

			if (registrarProcedimientos(actividad, connection)) {

				connection.commit();

				actividad.setProcedimientosCount(totalProcedimientos(actividad));

				return true;
			} else {

				connection.rollback();

			}

		} catch (SQLException e) {

			e.printStackTrace();
			return false;
		}
		return false;
	}

	public boolean registrarProcedimientos(Actividad actividad, Connection connection_) {
		// TODO Auto-generated method stub

		PreparedStatement preparedStatement = null;

		try (Connection connection = dataSource.getConnection()) {

			String sql = "INSERT INTO procedimiento (pro_nombre, pro_actividad, pro_orden) ";
			sql += "VALUES (?,?,?);";

			preparedStatement = connection.prepareStatement(sql);

			for (Procedimiento procedimiento : actividad.getProcedimientos()) {

				if (procedimiento.getPro_codigo() != 0) {
					actualizarProcedimiento(actividad, procedimiento, connection);
				} else {

					preparedStatement.setString(1, procedimiento.getPro_nombre());
					preparedStatement.setInt(2, actividad.getAct_codigo());
					preparedStatement.setInt(3, procedimiento.getPro_orden());

					preparedStatement.addBatch();
				}
			}
			preparedStatement.executeBatch();

			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

	}

	public boolean actualizarProcedimiento(Actividad actividad, Procedimiento procedimiento, Connection connection_) {
		// TODO Auto-generated method stub

		PreparedStatement preparedStatement = null;

		try (Connection connection = dataSource.getConnection()) {

			String sql = "UPDATE procedimiento set pro_nombre = ?, pro_estado = ?, pro_orden = ? where pro_actividad = ? and pro_codigo = ?;";

			preparedStatement = connection.prepareStatement(sql);

			preparedStatement.setString(1, procedimiento.getPro_nombre());
			preparedStatement.setBoolean(2, procedimiento.isPro_estado());
			preparedStatement.setInt(3, procedimiento.getPro_orden());
			preparedStatement.setInt(4, actividad.getAct_codigo());
			preparedStatement.setInt(5, procedimiento.getPro_codigo());

			preparedStatement.executeUpdate();

			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

	}

	public int totalProcedimientos(Actividad actividad) {

		PreparedStatement preparedStatement = null;
		ResultSet rs = null;

		try (Connection connection = dataSource.getConnection()) {

			String sql = "SELECT COUNT(*) AS procedimientos FROM procedimiento WHERE pro_actividad = ? and pro_estado = TRUE;";

			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, actividad.getAct_codigo());
			rs = preparedStatement.executeQuery();

			while (rs.next()) {

				actividad.setProcedimientosCount(rs.getInt("procedimientos"));

			}

		} catch (SQLException e) {
			e.printStackTrace();

		}

		return actividad.getProcedimientosCount();

	}

	@Override
	public Actividad getActividad(int act_codigo) {
		// TODO Auto-generated method stub
		String sql = "select * from actividad where act_codigo =?;";

		return jdbcTemplate.queryForObject(sql, new Object[] { act_codigo }, new ActividadRowMapper());
	}

	@Override
	public List<Actividad> getActividadesPorPersona(int personaId) {
		// TODO Auto-generated method stub

		String sql = "select * from actividad ac, actividad_persona acp where ac.act_codigo = acp.acp_actividad and ";
		sql += "acp.acp_persona = ? and acp.acp_estado = true;";

		ActividadExtractor extractor = new ActividadExtractor();
		extractor.setProcedimientoDao(procedimientoDao);

		return jdbcTemplate.query(sql, new Object[] { personaId }, extractor);
	}

	@Override
	public int validarActividadEnPersonaExistente(Persona persona, Actividad actividad) {
		// TODO Auto-generated method stub
		String sql = "select count(*) as count from actividad_persona where acp_actividad = ? and acp_persona = ?;";

		return jdbcTemplate.queryForObject(sql, new Object[] { actividad.getAct_codigo(), persona.getPer_codigo() },
				Integer.class);
	}

	@Override
	public void actualizarActividadEnPersona(Persona persona, Actividad actividad) {
		// TODO Auto-generated method stub
		String sql = "update actividad_persona set acp_estado = true  where acp_actividad = ? and acp_persona = ?;";
		jdbcTemplate.update(sql, new Object[] { actividad.getAct_codigo(), persona.getPer_codigo() });
	}

	@Override
	public void inhabilitarActividadesEnPersona(Persona persona) {
		// TODO Auto-generated method stub
		String sql = "update actividad_persona set acp_estado = false  where acp_persona = ?;";
		jdbcTemplate.update(sql, new Object[] { persona.getPer_codigo() });
	}

	@Override
	public void registrarActividadEnPersona(final Persona persona, final Actividad actividad) {
		// TODO Auto-generated method stub

		sql = " INSERT INTO actividad_persona (acp_actividad,acp_persona) ";
		sql = sql + "values (?,?);";

		KeyHolder key = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				final PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, actividad.getAct_codigo());
				ps.setInt(2, persona.getPer_codigo());

				return ps;
			}
		}, key);

		// recordatorio.setRec_codigo(key.getKey().intValue());

	}

}
