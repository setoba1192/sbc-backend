package com.sbc.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.sbc.dao.InspeccionRefuerzoCheckListDao;
import com.sbc.dao.RefuerzoCheckListDao;
import com.sbc.dao.mapper.InspeccionRefuerzoCheckListExtractor;
import com.sbc.model.InspeccionRefuerzoCheckList;
import com.sbc.model.Refuerzo;

@Repository("InspeccionRefuerzoCheckListDaoImpl")
public class InspeccionRefuerzoCheckListDaoImpl implements InspeccionRefuerzoCheckListDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private RefuerzoCheckListDao refuerzoCheckListDao;

	@Override
	public void registrarCheckList(final List<InspeccionRefuerzoCheckList> checklist, final Refuerzo refuerzo) {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO inspeccion_refuerzo_checklist (irc_refuerzo, irc_aplica, irc_refuerzo_checklist) ";
		sql += "VALUES (?,?,?) ";

		jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				InspeccionRefuerzoCheckList check = checklist.get(i);
				ps.setInt(1, refuerzo.getId());
				ps.setBoolean(2, check.isAplica());
				ps.setInt(3, check.getRefuerzoCheckList().getId());

			}

			@Override
			public int getBatchSize() {
				return checklist.size();
			}
		});

	}

	@Override
	public List<InspeccionRefuerzoCheckList> listarPorRefuerzo(int idRefuerzo) {
		// TODO Auto-generated method stub
		String sql = "select * from  inspeccion_refuerzo_checklist where irc_refuerzo = ?;";

		InspeccionRefuerzoCheckListExtractor extractor = new InspeccionRefuerzoCheckListExtractor();
		extractor.setRefuerzoCheckListDao(refuerzoCheckListDao);

		return jdbcTemplate.query(sql, new Object[] { idRefuerzo }, extractor);
	}

}
