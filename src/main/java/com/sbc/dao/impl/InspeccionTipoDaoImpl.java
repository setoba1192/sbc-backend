package com.sbc.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.sbc.dao.InspeccionTipoDao;
import com.sbc.dao.mapper.InspeccionTipoExtractor;
import com.sbc.model.InspeccionTipo;

@Repository("InspeccionTipoDaoImpl")
public class InspeccionTipoDaoImpl implements InspeccionTipoDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public InspeccionTipo get(int int_codigo) {
		// TODO Auto-generated method stub
		String sql = "select * from inspeccion_tipo where int_codigo = ?;";

		List<InspeccionTipo> listTipo = jdbcTemplate.query(sql, new Object[] { int_codigo },
				new InspeccionTipoExtractor());

		return listTipo.size() > 0 ? listTipo.get(0) : null;
	}

}
