package com.sbc.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sbc.dao.NivelCriticoDao;
import com.sbc.model.NivelCritico;

@Repository
public class NivelCriticoDaoImpl implements NivelCriticoDao {

	@Autowired
	private DataSource dataSource;

	public Object get(int id) {
		// TODO Auto-generated method stub

		PreparedStatement preparedStatement = null;

		ResultSet resultSet = null;

		try (Connection connection = dataSource.getConnection()) {
			String sql = "select * from nivel_critico where nic_codigo = ?;";

			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, id);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				NivelCritico nivelCritico = new NivelCritico();
				nivelCritico.setNic_codigo(resultSet.getInt("nic_codigo"));
				nivelCritico.setNic_nombre(resultSet.getString("nic_nombre"));
				nivelCritico.setNic_valor(resultSet.getInt("nic_valor"));

				return nivelCritico;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<Object> getAll() {

		List<Object> nivelCriticos = new ArrayList<Object>();

		PreparedStatement preparedStatement = null;

		ResultSet resultSet = null;

		try (Connection connection = dataSource.getConnection()) {

			String sql = "select * from nivel_critico;";

			preparedStatement = connection.prepareStatement(sql);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				NivelCritico nivelCritico = new NivelCritico();
				nivelCritico.setNic_codigo(resultSet.getInt("nic_codigo"));
				nivelCritico.setNic_nombre(resultSet.getString("nic_nombre"));
				nivelCritico.setNic_valor(resultSet.getInt("nic_valor"));

				nivelCriticos.add(nivelCritico);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return nivelCriticos;
	}

	public boolean create(Object object) {
		// TODO Auto-generated method stub
		return false;
	}

}
