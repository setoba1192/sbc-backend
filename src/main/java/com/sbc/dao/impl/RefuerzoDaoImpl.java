package com.sbc.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.sbc.dao.InspeccionRefuerzoCheckListDao;
import com.sbc.dao.RefuerzoDao;
import com.sbc.dao.mapper.RefuerzoExtractor;
import com.sbc.model.Inspeccion;
import com.sbc.model.Refuerzo;

@Repository
public class RefuerzoDaoImpl implements RefuerzoDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private InspeccionRefuerzoCheckListDao inspeccionRefuerzoCheckListDao;

	private String sql;

	@Override
	public Refuerzo buscarPorInspeccion(long idInspeccion) {
		// TODO Auto-generated method stub

		sql = "select * from refuerzo where ref_inspeccion = ?;";

		RefuerzoExtractor extractor = new RefuerzoExtractor();
		extractor.setInspeccionRefuerzoCheckListDao(inspeccionRefuerzoCheckListDao);

		List<Refuerzo> refuerzos = jdbcTemplate.query(sql, new Object[] { idInspeccion }, extractor);

		return refuerzos.size() > 0 ? refuerzos.get(0) : null;
	}

	@Override
	public void registrarRefuerzo(final Refuerzo refuerzo, final Inspeccion inspeccion) {
		// TODO Auto-generated method stub
		sql = "INSERT INTO refuerzo (ref_puntaje_maximo,ref_puntaje_mejora,ref_puntaje_obtenido,ref_observacion,ref_audio,ref_inspeccion)";
		sql = sql + "values (?,?,?,?,?,?);";

		KeyHolder key = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				final PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, refuerzo.getPuntajeMaximo());
				ps.setInt(2, refuerzo.getPuntajeMejora());
				ps.setInt(3, refuerzo.getPuntajeObtenido());
				ps.setString(4, refuerzo.getObservacion());
				ps.setString(5, refuerzo.getAudio());
				ps.setLong(6, inspeccion.getIns_codigo());
				return ps;
			}
		}, key);

		refuerzo.setId(key.getKey().intValue());

	}

}
