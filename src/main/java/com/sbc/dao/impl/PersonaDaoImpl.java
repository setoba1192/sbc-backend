package com.sbc.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.sbc.dao.PersonaAreaDao;
import com.sbc.dao.PersonaDao;
import com.sbc.dao.mapper.PersonaRowMapper;
import com.sbc.model.Persona;

@Repository
public class PersonaDaoImpl implements PersonaDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private PersonaAreaDao personaAreaDao;

	@Autowired
	private DataSource dataSource;

	public Object get(int id) {
		// TODO Auto-generated method stub
		String sql = "select * from persona where per_codigo = ?;";

		PersonaRowMapper mapper = new PersonaRowMapper();
		mapper.setPersonaAreaDao(personaAreaDao);
		try {
			return jdbcTemplate.queryForObject(sql, new Object[] { id }, mapper);
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	public boolean create(Object object) {

		Persona persona = (Persona) object;

		PreparedStatement preparedStatement = null;
		ResultSet rs = null;

		try (Connection connection = dataSource.getConnection()) {
			String sql = "INSERT INTO persona (per_identificacion, per_nombre, per_apellido, per_telefono, per_email) ";
			sql += "VALUES (?,?,?,?,?);";

			preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			preparedStatement.setString(1, persona.getPer_identificacion());
			preparedStatement.setString(2, persona.getPer_nombre());
			preparedStatement.setString(3, persona.getPer_apellido());
			preparedStatement.setString(4, persona.getPer_telefono());
			preparedStatement.setString(5, persona.getPer_email());

			preparedStatement.executeUpdate();

			rs = preparedStatement.getGeneratedKeys();

			if (rs.next()) {
				persona.setPer_codigo(rs.getInt(1));
			}

			object = persona;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public List<Object> getAll() {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		List<Object> personaList = new ArrayList<Object>();

		try (Connection connection = dataSource.getConnection()) {
			String sql = "Select * from persona order by per_apellido asc;";

			preparedStatement = connection.prepareStatement(sql);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				Persona persona = new Persona();

				persona.setPer_codigo(resultSet.getInt("per_codigo"));
				persona.setPer_identificacion(resultSet.getString("per_identificacion"));
				persona.setPer_nombre(resultSet.getString("per_nombre"));
				persona.setPer_apellido(resultSet.getString("per_apellido"));
				persona.setPer_telefono(resultSet.getString("per_telefono"));
				persona.setPer_email(resultSet.getString("per_email"));
				persona.setPer_estado(resultSet.getBoolean("per_estado"));
				persona.setAsignacion(personaAreaDao.personaAsignada(persona.getPer_codigo()));

				personaList.add(persona);

			}

		} catch (SQLException e) {
			e.printStackTrace();

		}
		return personaList;
	}

	public boolean actualizarPersona(Persona persona) {
		// TODO Auto-generated method stub

		PreparedStatement preparedStatement = null;

		try (Connection connection = dataSource.getConnection()) {
			String sql = "UPDATE persona set per_identificacion = ?, per_nombre = ? , per_apellido = ?, per_telefono =? , per_email = ? ";
			sql += "where per_codigo = ?;";

			preparedStatement = connection.prepareStatement(sql);

			preparedStatement.setString(1, persona.getPer_identificacion());
			preparedStatement.setString(2, persona.getPer_nombre());
			preparedStatement.setString(3, persona.getPer_apellido());
			preparedStatement.setString(4, persona.getPer_telefono());
			preparedStatement.setString(5, persona.getPer_email());
			preparedStatement.setInt(6, persona.getPer_codigo());

			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public Persona cambiarEstado(Persona persona) {
		// TODO Auto-generated method stub
		String sql = "update persona set per_estado = ? where per_codigo = ?;";

		jdbcTemplate.update(sql, new Object[] { persona.isPer_estado(), persona.getPer_codigo() });
		return persona;
	}

	public Persona buscarPorIdentificacion(String identificacion) {
		// TODO Auto-generated method stub
		String sql = " select * from persona where per_identificacion = ?;";

		PersonaRowMapper mapper = new PersonaRowMapper();
		mapper.setPersonaAreaDao(personaAreaDao);
		try {
			return jdbcTemplate.queryForObject(sql, new Object[] { identificacion }, mapper);
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

}
