package com.sbc.dao;

import com.sbc.model.Calificacion;

public interface CalificacionDao {

	public Calificacion getCalificacion(int cal_codigo);

}
