package com.sbc.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.sbc.dao.DiaDao;
import com.sbc.model.Recordatorio;

public class RecordatorioExtractor implements ResultSetExtractor<List<Recordatorio>> {

	private DiaDao diaDao;

	@Override
	public List<Recordatorio> extractData(ResultSet rs) throws SQLException, DataAccessException {
		// TODO Auto-generated method stub

		List<Recordatorio> recordatorios = new ArrayList<>();

		while (rs.next()) {

			Recordatorio recordatorio = new Recordatorio();
			recordatorio.setRec_codigo(rs.getInt("rec_codigo"));
			recordatorio.setDia(diaDao.buscarPorId(rs.getInt("rec_dia")));
			recordatorio.setRec_hora(rs.getTimestamp("rec_hora"));
			recordatorio.setActivo(rs.getBoolean("rec_activo"));

			recordatorios.add(recordatorio);
		}
		return recordatorios;
	}

	public DiaDao getDiaDao() {
		return diaDao;
	}

	public void setDiaDao(DiaDao diaDao) {
		this.diaDao = diaDao;
	}

}
