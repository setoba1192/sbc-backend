package com.sbc.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.sbc.model.InspeccionTipo;

public class InspeccionTipoExtractor implements ResultSetExtractor<List<InspeccionTipo>> {

	@Override
	public List<InspeccionTipo> extractData(ResultSet rs) throws SQLException, DataAccessException {
		// TODO Auto-generated method stub

		List<InspeccionTipo> insList = new ArrayList<>();

		while (rs.next()) {

			InspeccionTipo insTipo = new InspeccionTipo();
			insTipo.setInt_codigo(rs.getInt("int_codigo"));
			insTipo.setInt_nombre(rs.getString("int_nombre"));

			insList.add(insTipo);

		}
		return insList;
	}

}
