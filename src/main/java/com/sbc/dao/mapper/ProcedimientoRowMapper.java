package com.sbc.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sbc.model.Procedimiento;

public class ProcedimientoRowMapper implements RowMapper<Procedimiento> {

	@Override
	public Procedimiento mapRow(ResultSet rs, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		Procedimiento procedimiento = new Procedimiento();
		procedimiento.setPro_codigo(rs.getInt("pro_codigo"));
		procedimiento.setPro_nombre(rs.getString("pro_nombre"));
		procedimiento.setPro_estado(rs.getBoolean("pro_estado"));
		procedimiento.setPro_orden(rs.getInt("pro_orden"));
		return procedimiento;
	}

}
