package com.sbc.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sbc.dao.ActividadDao;
import com.sbc.dao.AreaDao;
import com.sbc.dao.InspeccionDetalleDao;
import com.sbc.dao.InspeccionTipoDao;
import com.sbc.dao.PersonaDao;
import com.sbc.dao.RefuerzoDao;
import com.sbc.model.Area;
import com.sbc.model.Inspeccion;
import com.sbc.model.Persona;

public class InspeccionRowMapper implements RowMapper<Inspeccion> {

	private ActividadDao actividadDao;

	private AreaDao areaDao;

	private PersonaDao personaDao;

	private InspeccionTipoDao inspeccionTipoDao;

	private InspeccionDetalleDao inspeccionDetalleDao;

	private RefuerzoDao refuerzoDao;

	@Override
	public Inspeccion mapRow(ResultSet rs, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		Inspeccion inspeccion = new Inspeccion();
		inspeccion.setIns_codigo(rs.getLong("ins_codigo"));
		inspeccion.setIns_fecha(rs.getTimestamp("ins_fecha"));
		inspeccion.setIns_observacion(rs.getString("ins_observacion"));
		inspeccion.setActividad(actividadDao.getActividad(rs.getInt("ins_actividad")));
		inspeccion.setArea((Area) areaDao.get(rs.getInt("ins_area")));

		inspeccion.setPersona((Persona) personaDao.get(rs.getInt("ins_persona")));
		inspeccion.setInspeccionTipo(inspeccionTipoDao.get(rs.getInt("ins_tipo")));

		inspeccion.setInspeccionDetalles(inspeccionDetalleDao.listInspeccionDetalle(inspeccion));
		inspeccion.setRefuerzo(refuerzoDao.buscarPorInspeccion(inspeccion.getIns_codigo()));

		return inspeccion;
	}

	public RefuerzoDao getRefuerzoDao() {
		return refuerzoDao;
	}

	public void setRefuerzoDao(RefuerzoDao refuerzoDao) {
		this.refuerzoDao = refuerzoDao;
	}

	public InspeccionDetalleDao getInspeccionDetalleDao() {
		return inspeccionDetalleDao;
	}

	public void setInspeccionDetalleDao(InspeccionDetalleDao inspeccionDetalleDao) {
		this.inspeccionDetalleDao = inspeccionDetalleDao;
	}

	public ActividadDao getActividadDao() {
		return actividadDao;
	}

	public void setActividadDao(ActividadDao actividadDao) {
		this.actividadDao = actividadDao;
	}

	public AreaDao getAreaDao() {
		return areaDao;
	}

	public void setAreaDao(AreaDao areaDao) {
		this.areaDao = areaDao;
	}

	public PersonaDao getPersonaDao() {
		return personaDao;
	}

	public void setPersonaDao(PersonaDao personaDao) {
		this.personaDao = personaDao;
	}

	public InspeccionTipoDao getInspeccionTipoDao() {
		return inspeccionTipoDao;
	}

	public void setInspeccionTipoDao(InspeccionTipoDao inspeccionTipoDao) {
		this.inspeccionTipoDao = inspeccionTipoDao;
	}

}
