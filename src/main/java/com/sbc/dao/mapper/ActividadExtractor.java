package com.sbc.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.sbc.dao.ProcedimientoDao;
import com.sbc.model.Actividad;

public class ActividadExtractor implements ResultSetExtractor<List<Actividad>> {

	private ProcedimientoDao procedimientoDao;

	@Override
	public List<Actividad> extractData(ResultSet rs) throws SQLException, DataAccessException {
		// TODO Auto-generated method stub

		List<Actividad> actividades = new ArrayList<>();

		while (rs.next()) {

			Actividad actividad = new Actividad();
			actividad.setAct_codigo(rs.getInt("act_codigo"));
			actividad.setAct_descripcion(rs.getString("act_descripcion"));
			actividad.setAct_estado(rs.getBoolean("act_estado"));
			actividad.setAct_nombre(rs.getString("act_nombre"));

			actividad.setProcedimientos(procedimientoDao.procedimientos(actividad));

			actividades.add(actividad);
		}

		return actividades;
	}

	public ProcedimientoDao getProcedimientoDao() {
		return procedimientoDao;
	}

	public void setProcedimientoDao(ProcedimientoDao procedimientoDao) {
		this.procedimientoDao = procedimientoDao;
	}
}
