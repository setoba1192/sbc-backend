package com.sbc.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sbc.dao.ActividadDao;
import com.sbc.dao.AreaDao;
import com.sbc.dao.NivelCriticoDao;
import com.sbc.dao.PersonaAreaDao;
import com.sbc.model.Area;
import com.sbc.model.NivelCritico;

public class AreaRowMapper implements RowMapper<Area> {

	private PersonaAreaDao personaAreaDao;

	private NivelCriticoDao nivelCriticoDao;

	private ActividadDao actividadDao;

	private AreaDao areaDao;

	public Area mapRow(ResultSet resultSet, int arg1) throws SQLException {
		// TODO Auto-generated method stub

		Area area = new Area();

		area.setAre_codigo(resultSet.getInt("are_codigo"));
		area.setAre_nombre(resultSet.getString("are_nombre"));
		area.setAre_descripcion(resultSet.getString("are_descripcion"));
		area.setAsignacion(personaAreaDao.areaAsignada(area.getAre_codigo()));

		area.setNivelCritico((NivelCritico) nivelCriticoDao.get(resultSet.getInt("are_nivelcritico")));

		area.setActividades(actividadDao.getActividades(area));

		area.setDias(areaDao.getDiasPorArea(area));

		return area;
	}

	public PersonaAreaDao getPersonaAreaDao() {
		return personaAreaDao;
	}

	public void setPersonaAreaDao(PersonaAreaDao personaAreaDao) {
		this.personaAreaDao = personaAreaDao;
	}

	public NivelCriticoDao getNivelCriticoDao() {
		return nivelCriticoDao;
	}

	public void setNivelCriticoDao(NivelCriticoDao nivelCriticoDao) {
		this.nivelCriticoDao = nivelCriticoDao;
	}

	public ActividadDao getActividadDao() {
		return actividadDao;
	}

	public void setActividadDao(ActividadDao actividadDao) {
		this.actividadDao = actividadDao;
	}

	public AreaDao getAreaDao() {
		return areaDao;
	}

	public void setAreaDao(AreaDao areaDao) {
		this.areaDao = areaDao;
	}

}
