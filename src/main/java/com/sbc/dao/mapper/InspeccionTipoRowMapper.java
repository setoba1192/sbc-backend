package com.sbc.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sbc.model.InspeccionTipo;

public class InspeccionTipoRowMapper implements RowMapper<InspeccionTipo> {

	@Override
	public InspeccionTipo mapRow(ResultSet rs, int arg1) throws SQLException {
		// TODO Auto-generated method stub

		InspeccionTipo insTipo = new InspeccionTipo();
		insTipo.setInt_codigo(rs.getInt("int_codigo"));
		insTipo.setInt_nombre(rs.getString("int_nombre"));

		return insTipo;
	}

}
