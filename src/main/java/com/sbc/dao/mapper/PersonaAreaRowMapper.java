package com.sbc.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.sbc.dao.ActividadDao;
import com.sbc.dao.AreaDao;
import com.sbc.dao.PersonaDao;
import com.sbc.model.Area;
import com.sbc.model.Persona;
import com.sbc.model.PersonaArea;

public class PersonaAreaRowMapper implements ResultSetExtractor<List<PersonaArea>> {

	private PersonaDao personaDao;

	private AreaDao areaDao;

	private ActividadDao actividadDao;

	@Override
	public List<PersonaArea> extractData(ResultSet rs) throws SQLException, DataAccessException {

		List<PersonaArea> personaAreas = new ArrayList<>();

		// TODO Auto-generated method stub

		while (rs.next()) {
			PersonaArea personaArea = new PersonaArea();
			personaArea.setPea_codigo(rs.getInt("pea_codigo"));
			personaArea.setPersona((Persona) personaDao.get(rs.getInt("pea_persona")));
			personaArea.setArea((Area) areaDao.get(rs.getInt("pea_area")));
			personaArea.setActividades(actividadDao.getActividadesPorPersona(personaArea.getPersona().getPer_codigo()));

			personaAreas.add(personaArea);
		}

		return personaAreas;
	}

	public ActividadDao getActividadDao() {
		return actividadDao;
	}

	public void setActividadDao(ActividadDao actividadDao) {
		this.actividadDao = actividadDao;
	}

	public PersonaDao getPersonaDao() {
		return personaDao;
	}

	public void setPersonaDao(PersonaDao personaDao) {
		this.personaDao = personaDao;
	}

	public AreaDao getAreaDao() {
		return areaDao;
	}

	public void setAreaDao(AreaDao areaDao) {
		this.areaDao = areaDao;
	}

}
