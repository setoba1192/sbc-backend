package com.sbc.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sbc.model.Actividad;

public class ActividadRowMapper implements RowMapper<Actividad> {

	@Override
	public Actividad mapRow(ResultSet rs, int arg1) throws SQLException {
		// TODO Auto-generated method stub

		Actividad actividad = new Actividad();
		actividad.setAct_codigo(rs.getInt("act_codigo"));
		actividad.setAct_descripcion(rs.getString("act_descripcion"));
		actividad.setAct_estado(rs.getBoolean("act_estado"));
		actividad.setAct_nombre(rs.getString("act_nombre"));

		return actividad;
	}

}
