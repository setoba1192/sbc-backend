package com.sbc.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sbc.model.Calificacion;

public class CalificacionRowMapper implements RowMapper<Calificacion> {

	@Override
	public Calificacion mapRow(ResultSet rs, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		Calificacion calificacion = new Calificacion();
		calificacion.setCal_codigo(rs.getInt("cal_codigo"));
		calificacion.setCal_nombre(rs.getString("cal_nombre"));

		return calificacion;
	}

}
