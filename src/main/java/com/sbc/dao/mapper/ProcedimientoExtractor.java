package com.sbc.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.sbc.model.Procedimiento;

public class ProcedimientoExtractor implements ResultSetExtractor<List<Procedimiento>> {

	@Override
	public List<Procedimiento> extractData(ResultSet rs) throws SQLException, DataAccessException {
		// TODO Auto-generated method stub
		List<Procedimiento> procedimientos = new ArrayList<>();

		while (rs.next()) {

			Procedimiento procedimiento = new Procedimiento();
			procedimiento.setPro_codigo(rs.getInt("pro_codigo"));
			procedimiento.setPro_nombre(rs.getString("pro_nombre"));

			procedimiento.setPro_orden(rs.getInt("pro_orden"));
			procedimiento.setPro_estado(rs.getBoolean("pro_estado"));
			procedimientos.add(procedimiento);


		}

		return procedimientos;
	}

}
