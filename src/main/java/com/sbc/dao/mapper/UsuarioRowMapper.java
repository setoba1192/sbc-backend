package com.sbc.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sbc.dao.PersonaDao;
import com.sbc.model.Persona;
import com.sbc.model.Usuario;

public class UsuarioRowMapper implements RowMapper<Usuario> {

	private PersonaDao personaDao;

	public Usuario mapRow(ResultSet rs, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		Usuario usuario = new Usuario();

		usuario.setId(rs.getInt("id"));
		usuario.setUser(rs.getString("user"));
		usuario.setPersona((Persona) personaDao.get(rs.getInt("person")));

		return usuario;
	}

	public PersonaDao getPersonaDao() {
		return personaDao;
	}

	public void setPersonaDao(PersonaDao personaDao) {
		this.personaDao = personaDao;
	}

}
