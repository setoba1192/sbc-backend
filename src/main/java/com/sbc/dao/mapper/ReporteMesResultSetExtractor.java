package com.sbc.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.sbc.model.ReporteMes;

public class ReporteMesResultSetExtractor implements ResultSetExtractor<List<ReporteMes>> {

	@Override
	public List<ReporteMes> extractData(ResultSet rs) throws SQLException, DataAccessException {
		// TODO Auto-generated method stub

		List<ReporteMes> reportes = new ArrayList<>();

		while (rs.next()) {

			ReporteMes reporte = new ReporteMes();
			reporte.setMes(rs.getString("mes"));
			reporte.setCumple((Integer) rs.getInt("cumple"));
			reporte.setNoCumple((Integer) rs.getInt("nocumple"));
			reporte.setNoAplica((Integer) rs.getInt("noaplica"));

			reportes.add(reporte);

		}
		return reportes;
	}

}
