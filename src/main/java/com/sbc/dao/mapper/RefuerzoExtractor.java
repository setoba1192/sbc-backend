package com.sbc.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.sbc.dao.InspeccionRefuerzoCheckListDao;
import com.sbc.model.Refuerzo;

public class RefuerzoExtractor implements ResultSetExtractor<List<Refuerzo>> {

	private InspeccionRefuerzoCheckListDao inspeccionRefuerzoCheckListDao;

	@Override
	public List<Refuerzo> extractData(ResultSet rs) throws SQLException, DataAccessException {
		// TODO Auto-generated method stub

		List<Refuerzo> refuerzos = new ArrayList<>();

		while (rs.next()) {

			Refuerzo refuerzo = new Refuerzo();
			refuerzo.setId(rs.getInt("ref_codigo"));
			refuerzo.setPuntajeMaximo(rs.getInt("ref_puntaje_maximo"));
			refuerzo.setPuntajeObtenido(rs.getInt("ref_puntaje_obtenido"));
			refuerzo.setPuntajeMejora(rs.getInt("ref_puntaje_mejora"));
			refuerzo.setObservacion(rs.getString("ref_observacion"));
			refuerzo.setAudio(rs.getString("ref_audio"));
			refuerzo.setCheckList(inspeccionRefuerzoCheckListDao.listarPorRefuerzo(refuerzo.getId()));

			refuerzos.add(refuerzo);

		}

		return refuerzos;
	}

	public InspeccionRefuerzoCheckListDao getInspeccionRefuerzoCheckListDao() {
		return inspeccionRefuerzoCheckListDao;
	}

	public void setInspeccionRefuerzoCheckListDao(InspeccionRefuerzoCheckListDao inspeccionRefuerzoCheckListDao) {
		this.inspeccionRefuerzoCheckListDao = inspeccionRefuerzoCheckListDao;
	}

}
