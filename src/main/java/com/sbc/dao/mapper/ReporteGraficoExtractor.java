package com.sbc.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.sbc.model.ReporteGrafico;

public class ReporteGraficoExtractor implements ResultSetExtractor<List<ReporteGrafico>> {

	@Override
	public List<ReporteGrafico> extractData(ResultSet rs) throws SQLException, DataAccessException {
		// TODO Auto-generated method stub

		List<ReporteGrafico> valores = new ArrayList<>();

		while (rs.next()) {

			ReporteGrafico reporteGrafico = new ReporteGrafico();
			reporteGrafico.setLabel(rs.getString("Label"));
			reporteGrafico.setValor(rs.getInt("valor"));

			valores.add(reporteGrafico);

		}
		return valores;
	}

}
