package com.sbc.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sbc.dao.CalificacionDao;
import com.sbc.dao.ProcedimientoDao;
import com.sbc.model.InspeccionDetalle;

public class InspeccionDetalleRowMapper implements RowMapper<InspeccionDetalle> {

	private CalificacionDao calificacionDao;

	private ProcedimientoDao procedimientoDao;

	@Override
	public InspeccionDetalle mapRow(ResultSet rs, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		InspeccionDetalle inDetalle = new InspeccionDetalle();
		inDetalle.setInd_codigo(rs.getLong("ind_codigo"));
		inDetalle.setProcedimiento(procedimientoDao.getProcedimiento(rs.getInt("ind_procedimiento")));
		inDetalle.setCalificacion(calificacionDao.getCalificacion(rs.getInt("ind_calificacion")));
		inDetalle.setInd_observacion(rs.getString("ind_observacion"));

		return inDetalle;
	}

	public CalificacionDao getCalificacionDao() {
		return calificacionDao;
	}

	public void setCalificacionDao(CalificacionDao calificacionDao) {
		this.calificacionDao = calificacionDao;
	}

	public ProcedimientoDao getProcedimientoDao() {
		return procedimientoDao;
	}

	public void setProcedimientoDao(ProcedimientoDao procedimientoDao) {
		this.procedimientoDao = procedimientoDao;
	}

}
