package com.sbc.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.sbc.model.ReporteOcasional;

public class ReporteOcasionalExtractor implements ResultSetExtractor<List<ReporteOcasional>> {

	@Override
	public List<ReporteOcasional> extractData(ResultSet rs) throws SQLException, DataAccessException {
		// TODO Auto-generated method stub

		List<ReporteOcasional> list = new ArrayList<>();
		
		while(rs.next()) {
			
			ReporteOcasional reporteOcasional = new ReporteOcasional();
			
			reporteOcasional.setReo_area(rs.getString("reo_area"));
			reporteOcasional.setReo_observacion(rs.getString("reo_observacion"));
			
			list.add(reporteOcasional);
			
			
		}

		return list;
	}

}
