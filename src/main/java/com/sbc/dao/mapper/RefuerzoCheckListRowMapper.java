package com.sbc.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sbc.model.RefuerzoCheckList;

public class RefuerzoCheckListRowMapper implements RowMapper<RefuerzoCheckList> {

	@Override
	public RefuerzoCheckList mapRow(ResultSet rs, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		RefuerzoCheckList check = new RefuerzoCheckList();
		check.setId(rs.getInt("rec_codigo"));
		check.setNombre(rs.getString("rec_nombre"));

		return check;
	}

}
