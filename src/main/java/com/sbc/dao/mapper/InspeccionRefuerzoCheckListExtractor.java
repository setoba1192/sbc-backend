package com.sbc.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.sbc.dao.RefuerzoCheckListDao;
import com.sbc.model.InspeccionRefuerzoCheckList;

public class InspeccionRefuerzoCheckListExtractor implements ResultSetExtractor<List<InspeccionRefuerzoCheckList>> {

	private RefuerzoCheckListDao refuerzoCheckListDao;

	@Override
	public List<InspeccionRefuerzoCheckList> extractData(ResultSet rs) throws SQLException, DataAccessException {
		// TODO Auto-generated method stub
		List<InspeccionRefuerzoCheckList> checklist = new ArrayList<>();

		while (rs.next()) {

			InspeccionRefuerzoCheckList ins = new InspeccionRefuerzoCheckList();
			ins.setId(rs.getInt("irc_id"));
			ins.setAplica(rs.getBoolean("irc_aplica"));
			ins.setRefuerzoCheckList(refuerzoCheckListDao.buscarPorId(rs.getInt("irc_refuerzo_checklist")));
			checklist.add(ins);
		}

		return checklist;
	}

	public RefuerzoCheckListDao getRefuerzoCheckListDao() {
		return refuerzoCheckListDao;
	}

	public void setRefuerzoCheckListDao(RefuerzoCheckListDao refuerzoCheckListDao) {
		this.refuerzoCheckListDao = refuerzoCheckListDao;
	}

}
