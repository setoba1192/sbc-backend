package com.sbc.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sbc.dao.PersonaAreaDao;
import com.sbc.model.Persona;

public class PersonaRowMapper implements RowMapper<Persona> {

	private PersonaAreaDao personaAreaDao;

	public Persona mapRow(ResultSet resultSet, int arg1) throws SQLException {
		// TODO Auto-generated method stub

		Persona persona = new Persona();

		persona.setPer_codigo(resultSet.getInt("per_codigo"));
		persona.setPer_identificacion(resultSet.getString("per_identificacion"));
		persona.setPer_nombre(resultSet.getString("per_nombre"));
		persona.setPer_apellido(resultSet.getString("per_apellido"));
		persona.setPer_telefono(resultSet.getString("per_telefono"));
		persona.setPer_email(resultSet.getString("per_email"));
		persona.setPer_estado(resultSet.getBoolean("per_estado"));
		persona.setAsignacion(personaAreaDao.personaAsignada(persona.getPer_codigo()));

		return persona;
	}

	public PersonaAreaDao getPersonaAreaDao() {
		return personaAreaDao;
	}

	public void setPersonaAreaDao(PersonaAreaDao personaAreaDao) {
		this.personaAreaDao = personaAreaDao;
	}

}
