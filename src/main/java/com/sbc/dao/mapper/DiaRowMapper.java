package com.sbc.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sbc.model.Dia;

public class DiaRowMapper implements RowMapper<Dia> {

	@Override
	public Dia mapRow(ResultSet rs, int arg1) throws SQLException {
		// TODO Auto-generated method stub

		Dia dia = new Dia();
		dia.setDia_codigo(rs.getInt("dia_codigo"));
		dia.setDia_nombre(rs.getString("dia_nombre"));
		dia.setDia_dia_semana(rs.getInt("dia_dia_semana"));

		return dia;
	}

}
