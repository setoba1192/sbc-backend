package com.sbc.dao;

import java.util.List;

public interface BaseDao {

	public Object get(int id);

	public boolean create(Object object);

	public List<Object> getAll();

}
