package com.sbc.dao;

import java.util.List;

import com.sbc.model.ReporteGrafico;

public interface ReporteGraficoDao {

	public List<ReporteGrafico> getResumenAnualObservacion();

}
