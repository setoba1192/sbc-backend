package com.sbc.dao;

import com.sbc.model.InspeccionTipo;

public interface InspeccionTipoDao {

	public InspeccionTipo get(int int_codigo);

}
