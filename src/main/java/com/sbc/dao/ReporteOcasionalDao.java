package com.sbc.dao;

import java.util.Date;
import java.util.List;

import com.sbc.model.ReporteOcasional;

public interface ReporteOcasionalDao {

	public void crear(ReporteOcasional reporteOcasional);

	public List<ReporteOcasional> listPorFechas(Date fechaInicio, Date fechaFin);

}
