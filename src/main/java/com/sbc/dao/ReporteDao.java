package com.sbc.dao;

import java.util.List;

import com.sbc.model.ReporteMes;

public interface ReporteDao {

	public List<ReporteMes> getReporteMensual();

}
