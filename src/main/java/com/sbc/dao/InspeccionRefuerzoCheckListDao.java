package com.sbc.dao;

import java.util.List;

import com.sbc.model.InspeccionRefuerzoCheckList;
import com.sbc.model.Refuerzo;

public interface InspeccionRefuerzoCheckListDao {

	public void registrarCheckList(List<InspeccionRefuerzoCheckList> checklist, Refuerzo refuerzo);

	public List<InspeccionRefuerzoCheckList> listarPorRefuerzo(int idRefuerzo);

}
