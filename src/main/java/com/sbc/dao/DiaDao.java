package com.sbc.dao;

import com.sbc.model.Dia;

public interface DiaDao {

	public Dia buscarPorId(int dia_codigo);

}
