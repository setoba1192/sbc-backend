package com.sbc.dao;

import java.util.List;

import com.sbc.model.Area;
import com.sbc.model.Persona;
import com.sbc.model.PersonaArea;

public interface PersonaAreaDao {

	public boolean personaAsignada(int per_codigo);

	public boolean areaAsignada(int are_codigo);

	public boolean registrarAsignacion(Persona persona, Area area);

	public void cambiarAsignacion(PersonaArea personaArea);

	public List<PersonaArea> personasAreasList();

	public PersonaArea getPersonaArea(Persona persona);
	
	
}
