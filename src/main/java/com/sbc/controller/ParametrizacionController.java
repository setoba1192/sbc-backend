package com.sbc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sbc.dto.AreaActividad;
import com.sbc.dto.Respuesta;
import com.sbc.model.Area;
import com.sbc.model.Persona;
import com.sbc.model.PersonaArea;
import com.sbc.service.ActividadService;
import com.sbc.service.AreaService;
import com.sbc.service.PersonaAreaService;
import com.sbc.service.PersonaService;

@RestController
public class ParametrizacionController {

	@Autowired
	private AreaService areaService;

	@Autowired
	private PersonaService personaService;

	@Autowired
	private PersonaAreaService personaAreaService;

	@Autowired
	private ActividadService actividadService;

	@RequestMapping(value = "/admin/area", method = RequestMethod.POST)
	public @ResponseBody Respuesta createArea(@RequestBody Area area) {

		return areaService.crearArea(area);
	}

	@RequestMapping(value = "/admin/area", method = RequestMethod.PUT)
	public @ResponseBody Respuesta updateArea(@RequestBody Area area) {

		return areaService.actualizarArea(area);
	}

	@RequestMapping(value = "/admin/persona", method = RequestMethod.POST)
	public @ResponseBody Respuesta createPersona(@RequestBody Persona persona) {

		return personaService.crearPersona(persona);
	}

	@RequestMapping(value = "/admin/persona", method = RequestMethod.PUT)
	public @ResponseBody Respuesta updatePersona(@RequestBody Persona persona) {

		return personaService.actualizarPersona(persona);
	}

	@RequestMapping(value = "/admin/persona/estado", method = RequestMethod.POST)
	public @ResponseBody Persona cambiarEstadoPersona(@RequestBody Persona persona) {

		return personaService.cambiarEstado(persona);
	}

	@RequestMapping(value = "/admin/area/asignar", method = RequestMethod.POST)
	public @ResponseBody Respuesta asignar(@RequestBody PersonaArea personaArea) {

		return personaAreaService.registrarAsignacion(personaArea);
	}

	@RequestMapping(value = "/admin/area/asignar", method = RequestMethod.PUT)
	public @ResponseBody Respuesta reasignar(@RequestBody PersonaArea personaArea) {

		return personaAreaService.cambiarAsignacion(personaArea);
	}

	@RequestMapping(value = "/admin/actividad", method = RequestMethod.POST)
	public @ResponseBody Respuesta registrarActividad(@RequestBody AreaActividad areaActividad) {

		return actividadService.registrarActividad(areaActividad.getArea(), areaActividad.getActividad());
	}

	@RequestMapping(value = "/admin/actividad", method = RequestMethod.PUT)
	public @ResponseBody Respuesta updateActividad(@RequestBody AreaActividad areaActividad) {

		return actividadService.actualizarActividad(areaActividad.getArea(), areaActividad.getActividad());
	}

}
