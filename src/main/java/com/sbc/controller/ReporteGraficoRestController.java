package com.sbc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sbc.dao.ReporteDao;
import com.sbc.dao.ReporteGraficoDao;
import com.sbc.model.ReporteGrafico;
import com.sbc.model.ReporteMes;

@RestController
public class ReporteGraficoRestController {

	@Autowired
	private ReporteGraficoDao reporteGraficoDao;

	@Autowired
	private ReporteDao reporteDao;

	@RequestMapping(value = "/admin/reporte/resumenObservacion", method = RequestMethod.GET)
	public @ResponseBody List<ReporteGrafico> getResumenAnualObservacion() {

		return reporteGraficoDao.getResumenAnualObservacion();
	}

	@RequestMapping(value = "/admin/reporte/resumenMensual", method = RequestMethod.GET)
	public @ResponseBody List<ReporteMes> getReporteMensual() {

		return reporteDao.getReporteMensual();
	}

}
