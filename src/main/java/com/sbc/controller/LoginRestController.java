package com.sbc.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sbc.dto.Respuesta;
import com.sbc.dto.Respuesta.Type;
import com.sbc.model.Persona;
import com.sbc.service.UsuarioService;
import com.sbc.util.Sha1Digest;

@RestController
public class LoginRestController {

	@Autowired
	private UsuarioService usuarioService;

	/**
	 * 
	 * @param request
	 * @param response
	 * @param auth
	 *            usuario(base64):password(base64) el : para hacer un split
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/app/login", method = RequestMethod.GET)
	public Respuesta login(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "auth") String auth) throws IOException {

		return usuarioService.login(auth);
	}

	@RequestMapping(value = "/app/login/auth", method = RequestMethod.GET)
	public Respuesta loginAuth(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("token") String token, @RequestBody Persona persona) throws IOException {

		Respuesta respuesta = new Respuesta();

		String tokenGenerado = Sha1Digest.SHA1("jsrs2018" + (new SimpleDateFormat("yyyyMMdd").format(new Date())));
		if (!tokenGenerado.equals(token)) {

			respuesta.setType(Type.ERROR);
			respuesta.setLogin(false);
			return respuesta;
		}

		respuesta.setData(usuarioService.getUserByAuth(persona));
		respuesta.setLogin(true);
		respuesta.setType(Type.SUCCESS);

		return respuesta;
	}
}
