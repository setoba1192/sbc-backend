package com.sbc.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {

	@RequestMapping(value = "/app/logout", method = RequestMethod.GET)
	public void salir(HttpServletRequest request, HttpServletResponse response) throws IOException {

		request.getSession().invalidate();
		response.sendRedirect(request.getContextPath() + "/admin/");
	}

}
