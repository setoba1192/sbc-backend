package com.sbc.controller;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sbc.dao.InspeccionDao;
import com.sbc.dao.ReporteOcasionalDao;
import com.sbc.model.Inspeccion;
import com.sbc.model.ReporteOcasional;

@RestController
public class InspeccionRestController {
	@Autowired
	private InspeccionDao inspeccionDao;

	@Autowired
	private ReporteOcasionalDao reporteOcasionalDao;

	@RequestMapping(value = "/admin/inspeccion/list", method = RequestMethod.POST)
	public @ResponseBody List<Inspeccion> listarInspeccion(@RequestParam(value = "fechaInicio") long fechaInicio,
			@RequestParam(value = "fechaFin") long fechaFin, @RequestParam(value = "tipo") int tipo) {

		Timestamp inicio = new Timestamp(fechaInicio);
		Timestamp fin = new Timestamp(fechaFin);

		return inspeccionDao.listPorFechas(inicio, fin, tipo);
	}

	@RequestMapping(value = "/admin/reporteocasional/list", method = RequestMethod.POST)
	public @ResponseBody List<ReporteOcasional> listarReporte(@RequestParam(value = "fechaInicio") long fechaInicio,
			@RequestParam(value = "fechaFin") long fechaFin) {

		Timestamp inicio = new Timestamp(fechaInicio);
		Timestamp fin = new Timestamp(fechaFin);

		return reporteOcasionalDao.listPorFechas(inicio, fin);
	}

}
