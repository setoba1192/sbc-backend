package com.sbc.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sbc.dto.Respuesta;
import com.sbc.model.Inspeccion;
import com.sbc.model.InspeccionTipo;
import com.sbc.model.Persona;
import com.sbc.model.Recordatorio;
import com.sbc.model.ReporteOcasional;
import com.sbc.service.InspeccionService;
import com.sbc.service.PersonaAreaService;
import com.sbc.service.RecordatorioService;
import com.sbc.service.ReporteOcasionalService;

@RestController
public class AppRestController {

	@Autowired
	private PersonaAreaService personaAreaService;

	@Autowired
	private InspeccionService inspeccionService;

	@Autowired
	private ReporteOcasionalService reporteOcasionalService;

	@Autowired
	private RecordatorioService recordatorioService;

	@RequestMapping(value = "/app/profile", method = RequestMethod.POST)
	public Respuesta login(@RequestBody Persona persona) throws IOException {

		Respuesta respuesta = new Respuesta();
		respuesta.setData(personaAreaService.getPersonaArea(persona));

		return respuesta;
	}

	@RequestMapping(value = "/app/inspeccion", method = RequestMethod.POST)
	public Respuesta registrarInspeccion(@RequestBody Inspeccion inspeccion) throws IOException {

		return inspeccionService.registrarInspeccion(inspeccion);
	}

	@RequestMapping(value = "/app/inspeccion/list", method = RequestMethod.POST)
	public Respuesta listar(@RequestBody Persona persona, @RequestParam(value = "ins_tipo") int ins_tipo)
			throws IOException {

		InspeccionTipo inspeccionTipo = new InspeccionTipo();
		inspeccionTipo.setInt_codigo(ins_tipo);
		
		Respuesta respuesta = new Respuesta();
		respuesta.setData(inspeccionService.listInspecciones(persona, inspeccionTipo));
		return respuesta;
	}

	@RequestMapping(value = "/app/reporteocasional", method = RequestMethod.POST)
	public Respuesta registroReporteOcasional(@RequestBody ReporteOcasional reporteOcasional) throws IOException {

		return reporteOcasionalService.crear(reporteOcasional);
	}

	@RequestMapping(value = "/app/recordatorio", method = RequestMethod.POST)
	public Respuesta registroRecordatorio(@RequestBody Recordatorio recordatorio) throws IOException {

		if (recordatorio.getRec_codigo() == 0) {

			return recordatorioService.crear(recordatorio);

		} else {
			
			
			return recordatorioService.actualizar(recordatorio);

		}

	}

}
