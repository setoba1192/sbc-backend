package com.sbc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sbc.dao.AreaDao;
import com.sbc.dao.NivelCriticoDao;
import com.sbc.dao.PersonaAreaDao;
import com.sbc.dao.PersonaDao;
import com.sbc.dao.ProcedimientoDao;
import com.sbc.model.Actividad;
import com.sbc.model.PersonaArea;
import com.sbc.model.Procedimiento;

@RestController
public class ParametrizacionRestController {

	@Autowired
	private NivelCriticoDao nivelCriticoDao;

	@Autowired
	private AreaDao areaDao;

	@Autowired
	private ProcedimientoDao procedimientoDao;

	@Autowired
	private PersonaDao personaDao;

	@Autowired
	private PersonaAreaDao personaAreaDao;

	@RequestMapping(value = "/admin/getAreas", method = RequestMethod.GET)
	public @ResponseBody List<Object> areas() {

		return areaDao.getAll();
	}

	@RequestMapping(value = "/admin/getPersonas", method = RequestMethod.GET)
	public @ResponseBody List<Object> personas() {

		return personaDao.getAll();
	}

	@RequestMapping(value = "/admin/nivelesCriticos", method = RequestMethod.GET)
	public @ResponseBody List<Object> nivelesCriticos() {

		return nivelCriticoDao.getAll();
	}

	@RequestMapping(value = "/admin/procedimientos", method = RequestMethod.POST)
	public @ResponseBody List<Procedimiento> procedimientos(@RequestBody Actividad actividad) {

		return procedimientoDao.procedimientos(actividad);
	}

	@RequestMapping(value = "/admin/personasAreas", method = RequestMethod.GET)
	public @ResponseBody List<PersonaArea> personasAreas() {

		return personaAreaDao.personasAreasList();
	}

}
