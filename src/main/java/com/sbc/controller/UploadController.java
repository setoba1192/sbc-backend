package com.sbc.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import org.apache.commons.io.FilenameUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.sbc.dto.Respuesta;
import com.sbc.dto.Respuesta.Type;

@RestController
public class UploadController {

	// Save the uploaded file to this folder
	private static String UPLOADED_FOLDER = "../../uploads/";

	@GetMapping("/")
	public String index() {
		return "upload";
	}

	// @RequestMapping(value = "/upload", method = RequestMethod.POST)
	@RequestMapping(value = "/upload", method = RequestMethod.POST) // //new annotation since 4.3
	public @ResponseBody Respuesta singleFileUpload(@RequestParam("file") MultipartFile file) {

		Respuesta respuesta = new Respuesta();

		if (file.isEmpty()) {
			// redirectAttributes.addFlashAttribute("message", "Please select a file to
			// upload");
			respuesta.setMensaje("Debe seleccionar un archivo");
			respuesta.setType(Type.WARNING);

			return respuesta;
		}

		try {
			UUID uuidAudio = UUID.randomUUID();
			// Get the file and save it somewhere
			byte[] bytes = file.getBytes();

			String extension = FilenameUtils.getExtension(file.getOriginalFilename());

			Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
			Files.write(path, bytes);

			// redirectAttributes.addFlashAttribute("message",
			// "You successfully uploaded '" + file.getOriginalFilename() + "'");

			respuesta.setMensaje("Subida exitosa.");
			respuesta.setData(uuidAudio + "." + extension);
			respuesta.setType(Type.SUCCESS);
			return respuesta;

		} catch (IOException e) {
			e.printStackTrace();
		}
		respuesta.setMensaje("Error al subir el archivo");
		respuesta.setType(Type.ERROR);

		return respuesta;
	}

	@RequestMapping(value = "/audio", method = RequestMethod.GET)
	public ResponseEntity<byte[]> listarDocumentosAdjuntosProyecto(@RequestParam(value = "audio") String audio) {

		ResponseEntity<byte[]> response = null;

		try {
			Path filePath = Paths.get(UPLOADED_FOLDER + "/" + audio);

			//////////////////////////////////////

			byte[] contents = Files.readAllBytes(filePath);

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("audio/x-wav"));
			String filename = audio;
			headers.setContentDispositionFormData(filename, filename);
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			response = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);

			////////////////////////////////
		} catch (

		Exception e) {
			e.printStackTrace();

		}
		return response;
		// return new ResponseEntity<byte[]>(archivo, HttpStatus.OK);
	}

	@GetMapping("/uploadStatus")
	public String uploadStatus() {
		return "uploadStatus";
	}

}