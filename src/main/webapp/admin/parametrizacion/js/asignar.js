angular.module('myApp').controller('asignarController', function($scope, $http) {

	var vma = this;

	vma.areas = [];
	vma.nivelesCriticos = [];
	

	vma.getNivelesCriticos = getNivelesCriticos;
	
	var urls = ["../../admin/nivelesCriticos","../../admin/area",""];
	

	vma.registrar = registrar;

	function registrar() {
		
		swal({
			type: 'question',
			  title: '¿Desea registrar el área : '+vma.area.are_nombre+"?",
			  // input: 'email',
			  showCancelButton: true,
			  confirmButtonText: 'Aceptar',
			  cancelButtonText : 'Cancelar',
			  cancelButtonColor : '#C62828',
			  confirmButtonColor : '#1565C0',
			  showLoaderOnConfirm: true,
			  preConfirm: () => {
			    return new Promise((resolve) => {
			        	
			        	vma.area.nivelCritico = vma.nivelCritico;
			        	
			        	guardar("POST",vma.area,urls[1]);
			          
			    })
			  },
			  
			  allowOutsideClick: () => !swal.isLoading()
			});
		console.log(vma.area);

	}
	
	function guardar(metodo, object, url) {

		
		$http({
			method : metodo,
			url : url,
			data : object
		}).then(function(response) {

			console.log(response);
			mensajeDialog(response);
			
			getAreas();
			
		}, function(response) { 
			// failed
			console.log(response);
			mensajeDialog(response);
		});

	}
	
	function mensajeDialog(response){
		
		swal({
  	      type: response.data.type.toLowerCase(),
  	      title: 'Información:',
  	      html: response.data.mensaje
  	    })
	}


	function getNivelesCriticos() {

		var url = "../../admin/nivelesCriticos";

		// console.log($scope.tipoArticulo);
		$http({
			method : "GET",
			url : url

		}).then(function(response) {

			vma.nivelesCriticos = response.data;

		}, function(response) { // optional
			// failed
		});

	}
	getNivelesCriticos();
	
	function getAreas() {

		var url = "../../admin/getAreas";

		// console.log($scope.tipoArticulo);
		$http({
			method : "GET",
			url : url

		}).then(function(response) {

			vma.areas = response.data;

		}, function(response) { // optional
			// failed
		});

	}
	getAreas();
});