angular.module('myApp').controller(
		'actividadesController',
		function($scope, $http) {

			var vm = this;

			vm.edicion = false;
			vm.area = null;
			
			vm.indexArea = 0;
			
			vm.actividad = {

				area : null,

				act_nombre : '',

				act_estado : true,

				act_fecha : '',

				procedimientos : []

			}

			vm.procedimiento = {

				pro_nombre : '',
				pro_orden : 0,
				pro_estado : true
			}

			vm.seleccionarArea = function(area) {
				console.log(area);

				vm.area = area;
				
				$('#areasModal').modal('hide');
			}

			vm.agregarProcedimiento = function() {
				vm.procedimiento.pro_estado = true;
				
				vm.actividad.procedimientos
						.push(angular.copy(vm.procedimiento));
				console.log(vm.procedimiento);
				vm.procedimiento = {

						pro_nombre : '',
						pro_orden : 0,
						pro_estado : true
					};
			

			}
			
			vm.editar = function(actividad, index){
				
				vm.edicion = true;
				vm.indexArea = index;
				vm.actividad = angular.copy(actividad);
				
				getProcedimientos(vm.actividad);
				
			}
			
			vm.editarProcedimiento = function(procedimiento){
				

		
				   swal({
		                title: 'Procedimiento',
		                input: 'text',
		                inputValue: procedimiento.pro_nombre,
		                inputPlaceholder: 'Ingrese el nombre del procedimiento',
		                showCancelButton: true,
		                inputValidator: function inputValidator(value) {
		                	
		                if(value.length>0){
		                	
		                	actualizarProcedimiento(procedimiento, value);
		                	
		                }
		                	
		                  return !value && 'Debe ingresar un valor';
		                
		                }
		              });

				
				
			}
			
			vm.eliminarProcedimiento = function(procedimiento){
				
				procedimiento.pro_estado = false;
				
			}
			function actualizarProcedimiento(procedimiento, value){
				
				procedimiento.pro_nombre= value;
				$scope.$apply();
				
				
			}
			
			vm.cancelar = function(){
				
				vm.edicion = false;
				
				vm.actividad = {

						area : null,

						act_nombre : '',

						act_estado : true,

						act_fecha : '',

						procedimientos : []

					}
			}

			 function validarActividad() {
				 
				 console.log("validar");
				 console.log(vm.area);
				 
				if (vm.area == null || vm.area == undefined) {

					console.log("nuasdfaskdf");
					mensajeDialog({
						type : "warning",
						mensaje : "Debe seleccionar un área."
					});
					return false;
				}

				if (vm.actividad.procedimientos.length == 0) {

					mensajeDialog({
						type : "warning",
						mensaje : "Debe agregar uno o más procedimientos."
					});
					return false;
				}
				
				return true;


			};
			
			vm.registrar = function() {
				
				if(!validarOrdenRepetido()){
					
					return;
				}
				
				
				
				if(validarActividad()==false){
					
					return;
				}
				
				var operacion = vm.edicion ==true ? 'actualizar' : 'registrar';
				
				
				swal({
					type: 'question',
					  title: "¿Desea "+operacion+" la actividad : "+vm.actividad.act_nombre+"?",
					  // input: 'email',
					  showCancelButton: true,
					  confirmButtonText: 'Aceptar',
					  cancelButtonText : 'Cancelar',
					  cancelButtonColor : '#C62828',
					  confirmButtonColor : '#1565C0',
					  showLoaderOnConfirm: true,
					  preConfirm: () => {
					    return new Promise((resolve) => {

					    	var method = vm.edicion ==true ? 'PUT' : 'POST';
					        	guardar(method);
					          
					    })
					  },
					  
					  allowOutsideClick: () => !swal.isLoading()
					});

			}

			function guardar(method) {

				var areaActividad = {

					area : vm.area,
					actividad : vm.actividad
				}
				

				$http({
					method : method,
					url : "../../admin/actividad",
					data : areaActividad
				}).then(function(response) {

					
					mensajeDialog(response.data);
					
					if(response.data.type=="WARNING" || response.data.type=="ERROR"){
						
						return;
					}
					
					if(vm.edicion){
						
						vm.area.actividades[vm.indexArea] = response.data.data;
					}else{
						
						vm.area.actividades.push(response.data.data);
					}
					clear();
					 vm.edicion = false;

				}, function(response) {
					// failed
					console.log(response);
					mensajeDialog(response);
				});

			}

			function clear() {
				
				vm.procedimiento = {

						pro_nombre : '',
						pro_orden : 0
					};


				vm.actividad = vm.actividad = {

					area : null,

					act_nombre : '',

					act_estado : true,

					act_fecha : '',
					procedimientos : []

				};
				
	

		
			};

			function mensajeDialog(response) {

				swal({
					type : response.type.toLowerCase(),
					title : 'Información:',
					html : response.mensaje
				})
			};
			
			vm.cargarAreas = function(){
				
				getAreas();
			}

			function getAreas() {

				var url = "../../admin/getAreas";

				// console.log($scope.tipoArticulo);
				$http({
					method : "GET",
					url : url

				}).then(function(response) {

					vm.areas = response.data;

				}, function(response) { // optional
					// failed
				});

			}
			getAreas();
			
			
			function getProcedimientos(actividad) {
				console.log("Hola")

				var url = "../../admin/procedimientos";

				// console.log($scope.tipoArticulo);
				$http({
					method : "POST",
					url : url,
					data : actividad

				}).then(function(response) {

					vm.actividad.procedimientos = response.data;

				}, function(response) { // optional
					// failed
				});

			}
			
			function validarOrdenRepetido(){
				  
				  for(var i = 0; i < vm.actividad.procedimientos.length; i++){
					  
					  for(var j = i+1; j < vm.actividad.procedimientos.length; j++){
						  
						  var pro1 = vm.actividad.procedimientos[i];
						  var pro2 =vm.actividad.procedimientos[j];
						  
						  if(pro1.pro_orden == pro2.pro_orden){
							  
								mensajeDialog({
									type : "warning",
									mensaje : "El orden del procedimiento: "+pro1.pro_nombre+" se encuentra repetido."
								});
							  
								return false;
						  }
						  
						  
					  }
					  
					  
				  }
				  return true;
			
			}
				
		
			

			
			});