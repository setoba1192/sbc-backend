angular.module('myApp').controller('areaController', function($scope, $http) {

	var vma = this;
	
	vma.edicion = false;
	
	vma.areas = [];
	vma.nivelesCriticos = [];
	
	vma.dias = [{dia_codigo : 1, dia_nombre : "Lunes", laboral :false},{dia_codigo :2, dia_nombre :"Martes",laboral :false},
		{dia_codigo:3,dia_nombre:"Miércoles",laboral :false},{dia_codigo:4,dia_nombre:"Jueves",laboral :false},{dia_codigo:5,dia_nombre:"Viernes",laboral :false}
		,{dia_codigo:6,dia_nombre:"Sábado",laboral :false},{dia_codigo:7,dia_nombre:"Domingo",laboral :false}]
	

	vma.getNivelesCriticos = getNivelesCriticos;
	
	var urls = ["../../admin/nivelesCriticos","../../admin/area",""];
	

	vma.registrar = registrar;

	function registrar() {
		
		var operacion = vma.edicion ==true ? 'actualizar' : 'registrar';
		
		swal({
			type: 'question',
			  title: '¿Desea '+operacion+' el área : '+vma.area.are_nombre+'?',
			  // input: 'email',
			  showCancelButton: true,
			  confirmButtonText: 'Aceptar',
			  cancelButtonText : 'Cancelar',
			  cancelButtonColor : '#C62828',
			  confirmButtonColor : '#1565C0',
			  showLoaderOnConfirm: true,
			  preConfirm: () => {
			    return new Promise((resolve) => {
			        	
			        	vma.area.nivelCritico = vma.nivelCritico;
			        	
			        	var method = vma.edicion ==true ? 'PUT' : 'POST';
			        	
			        	guardar(method,vma.area,urls[1]);
			          
			    })
			  },
			  
			  allowOutsideClick: () => !swal.isLoading()
			});
		console.log(vma.area);

	}
	
	vma.editar = function(area){
		
		vma.edicion = true;
		
		vma.dias = angular.copy(area.dias);
		
		console.log(area);
		vma.area = angular.copy(area);
		vma.nivelCritico = area.nivelCritico;
	}
	
	vma.cancelar = function(){
		clear();
	}
	
	function guardar(metodo, object, url) {

		vma.area.dias = vma.dias;
		

		
		$http({
			method : metodo,
			url : url,
			data : object
		}).then(function(response) {

			
			mensajeDialog(response);
			
			if(response.data.type=="WARNING" || response.data.type=="ERROR"){
				
				return;
			}
			
			getAreas();
			
			
			clear();
			
		}, function(response) { 
			// failed
			console.log(response);
			mensajeDialog(response);
		});

	}
	
	function clear(){

		vma.area = {};
		vma.nivelCritico = null;
		vma.edicion = false;
		
		for(var d in vma.dias){
			
			vma.dias[d].laboral = false;
		}
	}
	
	function mensajeDialog(response){
		
		swal({
  	      type: response.data.type.toLowerCase(),
  	      title: 'Información:',
  	      html: response.data.mensaje
  	    })
	}


	function getNivelesCriticos() {

		var url = "../../admin/nivelesCriticos";

		// console.log($scope.tipoArticulo);
		$http({
			method : "GET",
			url : url

		}).then(function(response) {

			vma.nivelesCriticos = response.data;

		}, function(response) { // optional
			// failed
		});

	}
	getNivelesCriticos();
	
	function getAreas() {

		var url = "../../admin/getAreas";

		// console.log($scope.tipoArticulo);
		$http({
			method : "GET",
			url : url

		}).then(function(response) {

			vma.areas = response.data;

		}, function(response) { // optional
			// failed
		});

	}
	getAreas();
	
	vma.style = function(dia){
		
		var estilo = {};
		
		if(dia.dia_codigo==1){
			
			estilo = {"margin-bottom" : "15px"}
		}
		
		return estilo;
		
	}

});