angular.module('myApp').controller('personalController', function($scope, $http) {

	var vma = this;
	
	vma.edicion = false;
	
	var urls = ["../../admin/nivelesCriticos","../../admin/persona",""];
	
	vma.registrarPersona = function(){
		
		var operacion = vma.edicion ==true ? 'actualizar' : 'registrar';

		console.log(vma.persona);
		swal({
			type: 'question',
			  title: '¿Desea '+operacion+' personal : '+vma.persona.per_nombre+" "+vma.persona.per_apellido+"?",
			  // input: 'email',
			  showCancelButton: true,
			  confirmButtonText: 'Aceptar',
			  cancelButtonText : 'Cancelar',
			  cancelButtonColor : '#C62828',
			  confirmButtonColor : '#1565C0',
			  showLoaderOnConfirm: true,
			  preConfirm: () => {
			    return new Promise((resolve) => {
			        	
			    	var method = vma.edicion ==true ? 'PUT' : 'POST';
			        	
			        	guardar(method,vma.persona,urls[1]);
			          
			    })
			  },
			  
			  allowOutsideClick: () => !swal.isLoading()
			});

	}
	
	function guardar(metodo, object, url) {

		
		$http({
			method : metodo,
			url : url,
			data : object
		}).then(function(response) {

			console.log(response);
			mensajeDialog(response);
			
			
			
			
		}, function(response) { 
			// failed
			console.log(response);
			mensajeDialog(response);
		});

	}
	
	vma.editar = function(persona){
		
		vma.edicion = true;
		
		vma.persona = angular.copy(persona);
		
	}
	
	vma.cambiarEstado = function(persona){
		
		persona.per_estado = !persona.per_estado;
		
		var url = "../../admin/persona/estado";

		$http({
			method : "POST",
			url : url,
			data : persona

		}).then(function(response) {

			persona = response.data;

		}, function(response) { // optional
			
			var mensaje = {type:"ERROR", mensaje : "Ocurrió un error intentelo de nuevo..."};
			
			mensajeDialog()
		});
	}
	
	
	vma.cancelar = function(){
		
		vma.edicion = false;
		
		vma.persona = {};
		
	}
	
	function clear(){
		
		vma.persona = {per_identificacion : '', per_apellido :'', per_nombre : '', per_telefono: '', per_email : ''};
	}
	
	function mensajeDialog(response){
		
		if(response.data.type=="SUCCESS"){
		clear();
		vma.edicion = false;
		}
		getPersonas();
		swal({
  	      type: response.data.type.toLowerCase(),
  	      title: 'Información:',
  	      html: response.data.mensaje
  	    })
	}
	
	function getPersonas() {

		var url = "../../admin/getPersonas";

		// console.log($scope.tipoArticulo);
		$http({
			method : "GET",
			url : url

		}).then(function(response) {

			vma.personas = response.data;

		}, function(response) { // optional
			// failed
		});

	}
	getPersonas();

});