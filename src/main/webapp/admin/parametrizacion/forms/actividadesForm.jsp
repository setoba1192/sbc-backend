<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html data-ng-controller="actividadesController as ac">

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<div class="card" data-ng-show="main.actividades">
	<div class="header">
		<h4 class="title">
			<b>Registrar Tarea Cr�tica/ <a href=""
				data-ng-click="main.actividades = !main.actividades; main.submenu =!main.submenu">Regresar</a></b>
			</p>
		</h4>
	</div>
	<div class="content">
		<form data-ng-submit="ac.registrar()">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>�rea</label>
						<div class="input-group">
							<span class="input-group-btn">
								<button class="btn btn-primary" type="button"
									data-toggle="modal" data-target="#areasModal">Seleccionar</button>
							</span> <input type="text" class="form-control border-input"
								data-ng-model="ac.area.are_nombre" required
								placeholder="Seleccionar un �rea..." readonly>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Nombre de Tarea Cr�tica</label> <input type="text"
							data-ng-model="ac.actividad.act_nombre"
							class="form-control border-input" placeholder="Tarea cr�tica"
							value="" required>
					</div>
				</div>

			</div>



			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Descripci�n</label>
						<textarea rows="5" class="form-control border-input"
							data-ng-model="ac.actividad.act_descripcion"
							placeholder="Descripci�n" value=""></textarea>
					</div>
				</div>
			</div>


			<div class="row">
				<div class="col-md-12">
					<b><h4>Listado de comportamientos cr�ticos</h4></b>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">

					<div class="form-group">
						<label>Comportamiento cr�tico:</label>
						<div class="input-group">
							<input class="form-control border-input"
								data-ng-model="ac.procedimiento.pro_nombre"
								aria-label="Text input with multiple buttons">
							<div class="input-group-btn">
								<button type="button" class="btn btn-default" aria-label="Help"
									data-ng-disabled="ac.procedimiento.pro_nombre.length==0"
									data-ng-click="ac.agregarProcedimiento()">
									<span class="ti-plus"></span>
								</button>
							</div>
						</div>
					</div>
				</div>

			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="content table-responsive table-full-width">
						<table class="table table-hover">
							<thead>
								<th>Nombre</th>
								<th>Orden</th>
							</thead>
							<tbody>
								<tr ng-repeat="procedimiento in ac.actividad.procedimientos"
									data-ng-show="procedimiento.pro_estado">

									<td>{{procedimiento.pro_nombre}}</td>
									<td><input type="number" min="1"
										max="{{ac.actividad.procedimientos.length}}" name="orden"
										ng-model="procedimiento.pro_orden"
										ng-init="procedimiento.pro_orden = procedimiento.pro_orden == 0 ? ac.actividad.procedimientos.length : procedimiento.pro_orden"
										required></td>
									<td><button type="button" class="btn btn-primary btn-sm"
											data-ng-click="ac.editarProcedimiento(procedimiento)">Editar</button></td>

									<td><button type="button" class="btn btn-primary btn-sm"
											data-ng-click="ac.eliminarProcedimiento(procedimiento)">Eliminar</button></td>
								</tr>
							</tbody>
						</table>

					</div>


				</div>
			</div>
			<div class="text-center">
				<button type="submit" class="btn btn-info btn-fill btn-wd"
					data-ng-show="!ac.edicion">Registrar</button>
				<button type="submit" class="btn btn-success btn-fill btn-wd"
					data-ng-show="ac.edicion">Editar</button>
				<button type="button" class="btn btn-danger btn-fill btn-wd"
					data-ng-show="ac.edicion" data-ng-click="ac.cancelar()">Cancelar
				</button>
			</div>
		</form>
	</div>

	<hr />

	<div class="row">



		<div class="col-md-12">
			<div class="card card-plain">
				<div class="header">
					<h4 class="title">Tareas cr�ticas</h4>
					<p class="category">Tareas cr�ticas registradas</p>
				</div>
				<div class="content table-responsive table-full-width">
					<table class="table table-hover">
						<thead>
							<th>#</th>
							<th>Tarea C.</th>
							<th>Descripci�n</th>
							<th>Comportamientos C.</th>
							<th>Acci�n</th>


						</thead>
						<tbody>
							<tr ng-repeat="actividad in ac.area.actividades">
								<td>{{$index+1}}</td>
								<td>{{actividad.act_nombre}}</td>
								<td>{{actividad.act_descripcion}}</td>
								<td>{{actividad.procedimientosCount}}</td>

								<td><button type="button" class="btn btn-primary btn-sm"
										data-ng-click="ac.editar(actividad, $index)">Editar</button></td>

							</tr>


						</tbody>
					</table>

				</div>
			</div>
		</div>


	</div>
	<div class="clearfix"></div>

</div>

<!-- MODAL AREA -->
<div class="modal fade" id="areasModal" data-toggle="modal"
	aria-labelledby="areasLabel" style="background: rgba(0, 0, 0, 0.7)">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="areasLabel">Seleccione Area</h4>
			</div>
			<div class="modal-body">
				<div class="row">



					<div class="col-md-12">



						<table class="table table-hover sortable">
							<thead>
								<tr>
									<th>ID</th>
									<th>Nombre</th>
									<!-- <th>Nivel Cr�tico</th> -->
									<th style="text-align: center;">Asignada</th>
								</tr>
							</thead>
							<tbody>
								<tr dir-paginate="area in ac.areas  | itemsPerPage: 5"
									pagination-id="areas" data-ng-click="ac.seleccionarArea(area)">
									<td>{{area.are_codigo}}</td>
									<td>{{area.are_nombre}}</td>
									<!-- <td>{{area.nivelCritico.nic_nombre}}</td> -->

									<td align="center"><div
											data-ng-class="area.asignacion == true ? 'led-green' : 'led-red'"></div></td>

								</tr>


							</tbody>
						</table>
						<div>

							<div class="text-center">
								<dir-pagination-controls boundary-links="true"
									pagination-id="areas" template-url="dirPagination.tpl.html"></dir-pagination-controls>
							</div>
						</div>


					</div>


				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

			</div>
		</div>
	</div>
</div>
<!-- FIN MODAL AREA -->

</html>