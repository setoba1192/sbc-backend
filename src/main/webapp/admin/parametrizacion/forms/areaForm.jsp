<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style>
#btnClear {
	display: inline-block;
	vertical-align: top;
	margin-left: 2px;
	margin-right: 2px;
}

.clear {
	background: white !important;
	color: #7AC29A !important;
}
}
</style>
<div class="card" data-ng-controller="areaController as vma"
	data-ng-show="main.area">

	<div class="header">
		<h4 class="title">
			<b>Registrar Area/ <a href=""
				data-ng-click="main.area = !main.area; main.submenu =!main.submenu">Regresar</a></b>

		</h4>
	</div>
	<div class="content">
		<!-- <button ng-click="vma.prueba()">Hola</button>  -->
		<form ng-submit="vma.registrar()">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Nombre</label> <input type="text"
							class="form-control border-input" placeholder="Nombre"
							ng-model="vma.area.are_nombre" required>
					</div>
				</div>
				<!-- <div class="col-md-6">
					<div class="form-group">
						<label>Nivel Cr�tico</label> <select
							data-ng-options="nivelCritico as nivelCritico.nic_nombre for nivelCritico in vma.nivelesCriticos track by nivelCritico.nic_codigo"
							ng-model="vma.nivelCritico" class="form-control border-input"
							id="sel1">
							<option value="" ng-if="!vma.nivelCritico">Seleccionar</option>
						</select>
					</div>
				</div>
 -->
				<hr />

			</div>
			<div class="row">
				<div class="col-md-12" align="center">
					<h3>D�as laborales</h3>
				</div>


				<div class="col-md-12 text-center" align="center">


					<button type="button" id="btnClear" ng-repeat="dia in vma.dias"
						ng-click="dia.laboral = !dia.laboral"
						class="btn btn-success {{dia.laboral ? 'active' : 'clear' }} center-block">{{dia.dia_nombre}}</button>



				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Descripci�n</label>
						<textarea rows="5" class="form-control border-input"
							placeholder="Descripci�n" value=""
							data-ng-model="vma.area.are_descripcion"></textarea>
					</div>
				</div>
			</div>
			<div class="text-center">
				<button type="submit" class="btn btn-info btn-fill btn-wd"
					data-ng-show="!vma.edicion">Registrar</button>
				<button type="submit" class="btn btn-success btn-fill btn-wd"
					data-ng-show="vma.edicion">Editar</button>
				<button type="button" class="btn btn-danger btn-fill btn-wd"
					data-ng-show="vma.edicion" data-ng-click="vma.cancelar()">Cancelar
				</button>
			</div>
			<div class="clearfix"></div>
		</form>
	</div>
	<hr />

	<div class="row">



		<div class="col-md-12">
			<div class="card card-plain">
				<div class="header">
					<b><h4>�reas</h4></b>
					<p class="category">�reas registradas</p>
				</div>
				<div class="content table-responsive table-full-width">
					<table class="table table-hover">
						<thead>
							<th>ID</th>
							<th>Nombre</th>

							<th style="text-align: center;">Asignada</th>
							<th>Acci�n</th>

						</thead>
						<tbody>
							<tr dir-paginate="area in vma.areas | itemsPerPage: 5"
								pagination-id="areas">
								<td>{{area.are_codigo}}</td>
								<td>{{area.are_nombre}}</td>


								<td align="center"><div align="center"
										data-ng-class="area.asignacion == true ? 'led-green' : 'led-red'"></div></td>
								<td><button type="button" class="btn btn-primary btn-sm"
										data-ng-click="vma.editar(area)">Editar</button></td>

							</tr>


						</tbody>
					</table>

				</div>
			</div>
			<div class="text-center">
				<dir-pagination-controls boundary-links="true" pagination-id="areas"
					template-url="dirPagination.tpl.html"></dir-pagination-controls>
			</div>
		</div>


	</div>

</div>
</html>