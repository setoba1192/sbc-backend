<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<div class="card" data-ng-controller="personalController as vmp"
	data-ng-show="main.personal">
	<div class="header">
		<h4 class="title">
			<b>Registrar Personal/ <a href=""
				data-ng-click="main.personal = !main.personal; main.submenu =!main.submenu">Regresar</a></b>

		</h4>
	</div>

	<div class="content">
		<form ng-submit="vmp.registrarPersona()">
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label>Identificaci�n</label> <input type="text"
							class="form-control border-input" placeholder="Identificaci�n" pattern=".{6,}"
							required title="M�nimo 6 car�cteres"
							data-ng-model="vmp.persona.per_identificacion" value="" required>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Nombres</label> <input type="text"
							data-ng-model="vmp.persona.per_nombre"
							class="form-control border-input" placeholder="Nombres" value=""
							required>
					</div>
				</div>
				<div class="col-md-5">
					<div class="form-group">
						<label for="exampleInputEmail1">Apellidos</label> <input
							data-ng-model="vmp.persona.per_apellido" type="text"
							class="form-control border-input" placeholder="Apellidos"
							required>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Tel�fono</label> <input type="text"
							data-ng-model="vmp.persona.per_telefono"
							class="form-control border-input" placeholder="Tel�fono" value=""
							required>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Email</label> <input type="email"
							data-ng-model="vmp.persona.per_email"
							class="form-control border-input" placeholder="Email" value=""
							required>
					</div>
				</div>
			</div>






			<div class="text-center">
				<button type="submit" class="btn btn-info btn-fill btn-wd"
					data-ng-show="!vmp.edicion">Registrar</button>
				<button type="submit" class="btn btn-success btn-fill btn-wd"
					data-ng-show="vmp.edicion">Editar</button>
				<button type="button" class="btn btn-danger btn-fill btn-wd"
					data-ng-show="vmp.edicion" data-ng-click="vmp.cancelar()">Cancelar
				</button>
			</div>
			<div class="clearfix"></div>
		</form>
	</div>
	<hr />

	<div class="row">



		<div class="col-md-12">
			<div class="card card-plain">
				<div class="header">
					<b><h4>Personal</h4></b>
					<p class="category">Personas registradas</p>
				</div>
				<div class="content table-responsive table-full-width">
					<table class="table table-hover">
						<thead>
							<th>Identificacion</th>
							<th>Apellido</th>
							<th>Nombre</th>
							<th>Telefono</th>
							<th>Email</th>
							<th class="text-center">Acci�n</th>

						</thead>
						<tbody>
							<tr dir-paginate="persona in vmp.personas | itemsPerPage: 5"
								pagination-id="personas">
								<td>{{persona.per_identificacion}}</td>
								<td>{{persona.per_apellido}}</td>
								<td>{{persona.per_nombre}}</td>
								<td>{{persona.per_telefono}}</td>
								<td>{{persona.per_email}}</td>

								<td><button type="button" class="btn btn-primary btn-sm"
										data-ng-click="vmp.editar(persona)">Editar</button>
									<button type="button" class="btn btn-{{persona.per_estado == true ? 'primary' : 'danger'}} btn-sm"
										data-ng-click="vmp.cambiarEstado(persona)">{{persona.per_estado == true ? 'Deshabilitar' : 'Habilitar'}}</button></td>
							</tr>

 
						</tbody>
					</table>

				</div>
			</div>
			<div class="text-center">
				<dir-pagination-controls boundary-links="true"
					pagination-id="personas" template-url="dirPagination.tpl.html"></dir-pagination-controls>
			</div>
		</div>


	</div>
</div>
</html>