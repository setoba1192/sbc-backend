<!doctype html>
<html lang="en" data-ng-app="myApp">
<head>
<meta charset="utf-8" />
<link rel="apple-touch-icon" sizes="76x76"
	href="../../resources/img/apple-icon.png">
<link rel="icon" type="image/png" sizes="96x96"
	href="../../resources/img/favicon.png">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>Administrador</title>
<%
	String path = request.getContextPath();
%>
<meta
	content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'
	name='viewport' />
<meta name="viewport" content="width=device-width" />

<link href="css/style.css" rel="stylesheet" />
<!-- Bootstrap core CSS     -->
<link href="<%=path%>/resources/css/bootstrap.min.css" rel="stylesheet" />

<!-- Animation library for notifications   -->
<link href="<%=path%>/resources/css/animate.min.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />
<!--  Paper Dashboard core CSS    -->
<link href="<%=path%>/resources/css/paper-dashboard.css"
	rel="stylesheet" />

<!--  Sweetalert2 css -->
<link rel="stylesheet"
	href="<%=path%>/resources/sweetalert2/dist/sweetalert2.min.css">


<!--  CSS for Demo Purpose, don't include it in your project     -->
<link href="<%=path%>/resources/css/demo.css" rel="stylesheet" />


<!--  Fonts and icons     -->
<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"
	rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Muli:400,300'
	rel='stylesheet' type='text/css'>
<link href="<%=path%>/resources/css/themify-icons.css" rel="stylesheet">

<style>
.cursor {
	cursor: pointer;
}
</style>

</head>
<body data-ng-controller="mainController as main">

	<div class="wrapper">
		<div class="sidebar" data-background-color="white"
			data-active-color="danger">

			<!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

			<div class="sidebar-wrapper">


				<jsp:include page="../menu/menu.jsp">
					<jsp:param name="menu" value="parametros" />
				</jsp:include>
			</div>
		</div>

		<div class="main-panel">
			<nav class="navbar navbar-default">
				<div class="container-fluid">




					<div class="navbar-header">
						<button type="button" class="navbar-toggle">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar bar1"></span> <span class="icon-bar bar2"></span>
							<span class="icon-bar bar3"></span>
						</button>
						<a class="navbar-brand" href="#">Administrador</a>
					</div>
					<div class="collapse navbar-collapse"></div>
				</div>
			</nav>


			<div class="content">
				<div class="container-fluid">

					<!-- AREA FORM  -->

					<jsp:include page="forms/areaForm.jsp" />

					<!-- AREA FORM  -->

					<!-- PERSONAL FORM  -->

					<jsp:include page="forms/personalForm.jsp" />

					<!-- PERSONAL FORM  -->

					<!-- ASIGNAR FORM  -->



					<!-- ASIGNAR FORM  -->

					<!-- ACTIVIDADES FORM  -->

					<jsp:include page="forms/actividadesForm.jsp" />

					<!-- ACTIVIDADES FORM  -->

					<div class="row" data-ng-show="main.submenu">
						<div class="col-lg-3 col-sm-6">
							<div>
								<div class="card cursor">
									<div class="content"
										data-ng-click="main.area = !main.area; main.submenu =!main.submenu">
										<div class="row">
											<div class="col-xs-5">
												<div class="icon-big icon-warning text-center">
													<i class="ti-layout-media-right-alt"></i>
												</div>
											</div>
											<div class="col-xs-7">
												<div class="numbers">
													<p>�reas</p>


												</div>
											</div>
										</div>
										<div class="footer">
											<hr />
											<div class="stats">
												<i class="ti-reload"></i> Listar/Registrar
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>
						<div class="col-lg-3 col-sm-6">
							<div class="card cursor">
								<div class="content"
									data-ng-click="main.personal = !main.personal; main.submenu =!main.submenu">
									<div class="row">
										<div class="col-xs-5">
											<div class="icon-big icon-success text-center">
												<i class="ti-wallet"></i>
											</div>
										</div>
										<div class="col-xs-7">
											<div class="numbers">
												<p>Personal</p>
												<!-- $1,345  -->
											</div>
										</div>
									</div>
									<div class="footer">
										<hr />
										<div class="stats">
											<i class="ti-calendar"></i> Listar/Registrar
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-lg-3 col-sm-6">
							<div class="card cursor">
								<div class="content"
									data-ng-click="main.actividades= !main.actividades; main.submenu =!main.submenu; ac.cargarAreas()">
									<div class="row">
										<div class="col-xs-5">
											<div class="icon-big icon-danger text-center">
												<i class="ti-pulse"></i>
											</div>
										</div>
										<div class="col-xs-7">
											<div class="numbers">
												<p>Tareas Cr�ticas</p>

											</div>
										</div>
									</div>
									<div class="footer">
										<hr />
										<div class="stats">
											<i class="ti-timer"></i> Listar/Registrar
										</div>
									</div>
								</div>
							</div>
						</div>


					</div>


				</div>
			</div>


			<footer class="footer">
				<div class="container-fluid">
					
					
				</div>
			</footer>

		</div>
	</div>
</body>

<!--   Core JS Files   -->
<script src="<%=path%>/resources/js/jquery-1.10.2.js"
	type="text/javascript"></script>
<script src="<%=path%>/resources/js/bootstrap.min.js"
	type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="<%=path%>/resources/js/bootstrap-checkbox-radio.js"></script>

<!--  Notifications Plugin    -->
<script src="<%=path%>/resources/js/bootstrap-notify.js"></script>


<!-- Paper Dashboard Core javascript and methods for Demo purpose -->
<script src="<%=path%>/resources/js/paper-dashboard.js"></script>

<!-- Paper Dashboard DEMO methods, don't include it in your project! -->

<!-- Sweetalert2 js -->

<script src="<%=path%>/resources/sweetalert2/dist/sweetalert2.min.js"></script>

<script src="<%=path%>/resources/angular/angular.min.js"></script>
<script src="<%=path%>/resources/angular/dirPagination.js"></script>
<script src="js/app.js"></script>
<script src="js/main.js"></script>
<script src="js/area.js"></script>
<script src="js/asignar.js"></script>
<script src="js/personal.js"></script>
<script src="js/actividades.js"></script>

</html>
