
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html data-ng-controller="inspeccionesController as vm">

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">


<jsp:include page="inspeccionDetalle.jsp" />


<div class="card" ng-show="!vm.detalle">

	<div class="header">
		<b><h4>Observaciones</h4></b>
	</div>
	<form ng-submit="vm.consultar()">
		<div class="content">

			<!-- <button ng-click="vma.prueba()">Hola</button>  -->

			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6">
						<div class="form-group">
							<label>Fecha Inicial:</label> <input type="date"
								class="form-control border-input" id="exampleInput" name="input"
								ng-model="vm.fechaInicio" placeholder="yyyy-MM-dd" required />
						</div>
					</div>


					<div class="col-md-6">
						<div class="form-group">
							<label>Fecha Final:</label> <input type="date"
								class="form-control border-input" id="exampleInput" name="input"
								ng-model="vm.fechaFin" placeholder="yyyy-MM-dd" required />

						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6">
						<h6 ng-show="vm.inspecciones.length==0">No se encontraron
							resultados...</h6>
					</div>
					<div class="col-md-12">

						<button type="submit"
							class="btn btn-info btn-fill btn-wd pull-right">Consultar</button>
						<button type="button" data-ng-click="vm.descargar()"
							ng-show="vm.inspecciones.length>0"
							class="btn btn-success btn-fill btn-wd pull-left">Exportar
							a Excel</button>
					</div>
				</div>
			</div>
		</div>



		<div class="clearfix"></div>


	</form>

	<div id="table2excel" class="content table-responsive table-full-width">
		<table class="table table-hover">
			<thead>
				<tr>
					<th width="5%"><b>C�digo</b></th>
					<th><b>Fecha</b></th>
					<th><b>�rea</b></th>
					<th><b>Tarea Cr�tica</b></th>
					<th class="text-center"><b>Cumplen</b></th>
					<th class="text-center"><b>No Cumplen</b></th>
					<th class="text-center"><b>No Aplica</b></th>
					<th class="text-center"><b>Detalle</b></th>
				</tr>


			</thead>

			<tbody>
				<tr ng-repeat="ins in vm.inspecciones">

					<td align="center">{{ins.ins_codigo}}</td>
					<td>{{ins.ins_fecha | date : "dd-MM-yyyy" }}</td>
					<td>{{ins.area.are_nombre}}</td>
					<td>{{ins.actividad.act_nombre}}</td>
					<td align="center">{{vm.contar(ins.inspeccionDetalles,1)}}</td>
					<td align="center">{{vm.contar(ins.inspeccionDetalles,2)}}</td>
					<td align="center">{{vm.contar(ins.inspeccionDetalles,3)}}</td>
					<td align="center"><a href=""
						data-ng-click="vm.seleccionarInspeccion(ins)" class="btn btn-info">
							<span class="ti-eye"></span>
					</a></td>
				</tr>
			</tbody>

		</table>
		<div class="content">
			<h5>Registros: {{vm.inspecciones.length}}</h5>
		</div>
	</div>


</div>



<hr />



<!-- MODALS  -->

<!-- MODAL AREA -->
<div class="modal fade" id="areasModal" data-toggle="modal"
	aria-labelledby="areasLabel" style="background: rgba(0, 0, 0, 0.7)">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h5 class="modal-title" id="areasLabel">
					<b>�rea : {{vm.inspeccion.area.are_nombre}} - Tarea Cr�tica:
						{{vm.inspeccion.actividad.act_nombre}}</b>
				</h5>
				<small>C�digo de Observaci�n : {{vm.inspeccion.ins_codigo}}</small>

			</div>
			<div class="modal-body">
				<div class="row">



					<div class="col-md-12">



						<table class="table table-hover sortable">
							<thead>
								<tr>
									<th><b>Comportamiento Cr�tico</b></th>
									<th class="text-center"><b>Valoraci�n</b></th>
									<th class="text-center"><b>Observaci�n</b></th>

								</tr>
							</thead>
							<tbody>
								<tr data-ng-repeat="det in vm.inspeccion.inspeccionDetalles">
									<td>{{det.procedimiento.pro_nombre}}</td>
									<td align="center"><p
											class="btn btn-{{det.calificacion.cal_codigo == 1 ? 'success' : det.calificacion.cal_codigo ==2 ? 'danger' :'default'}} btn-fill btn-wd">{{det.calificacion.cal_nombre}}</p></td>
									<td align="center">{{det.ind_observacion.length ==null ?
										'Ninguna' : det.ind_observacion}}</td>
								</tr>


							</tbody>
						</table>
						<div></div>


					</div>


				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

			</div>
		</div>
	</div>
</div>
<!-- FIN MODAL AREA -->



</html>