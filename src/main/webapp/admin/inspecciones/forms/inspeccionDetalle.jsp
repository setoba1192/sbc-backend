<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%
	String path = request.getContextPath();
%>
</head>
<div class="card" ng-show="vm.detalle">

	<div class="header">
		<h4>
			<b> Observaci�n Detalle/ <a href=""
				data-ng-click="vm.detalle = false">Regresar</a></b>
		</h4>
	</div>

	<div class="content">

		<!-- <button ng-click="vma.prueba()">Hola</button>  -->

		<div class="row">
			<div class="col-md-12">

				<div id="piechart" style="width: 900px; height: 500px;"></div>
			</div>
			<div class="col-md-12">



				<table class="table table-hover sortable">
					<thead>
						<tr>
							<th><b>Comportamiento Cr�tico</b></th>
							<th class="text-center"><b>Valoraci�n</b></th>
							<th class="text-center"><b>Observaci�n</b></th>

						</tr>
					</thead>
					<tbody>
						<tr data-ng-repeat="det in vm.inspeccion.inspeccionDetalles">
							<td>{{det.procedimiento.pro_nombre}}</td>
							<td align="center"><p
									class="btn btn-{{det.calificacion.cal_codigo == 1 ? 'success' : det.calificacion.cal_codigo ==2 ? 'danger' :'default'}} btn-fill btn-wd">{{det.calificacion.cal_nombre}}</p></td>
							<td align="center">{{det.ind_observacion.length ==null ?
								'Ninguna' : det.ind_observacion}}</td>
						</tr>


					</tbody>
				</table>
				<div></div>


			</div>
		</div>

		<hr>

		<div class="row">

			<div class="col-md-12">
				<h4>
					<b>Refuerzo:</b>
				</h4>
			</div>

		</div>
		<div class="row">

			<div class="col-md-12">
				<label style="color: black">{{vm.cumple}} de {{vm.total}}
					Comportamientos cr�ticos cumplidos - Propuesta de mejora:
					{{vm.inspeccion.refuerzo.puntajeMejora}}</label>
			</div>
			<hr>
			<div class="col-md-12">
				<div class="checkbox {{check.aplica ? 'checked' :''}}"
					style="color: black !important" disabled
					ng-repeat="check in vm.inspeccion.refuerzo.checkList">
					<input id="checkbox1" type="checkbox" ng-model="check.aplica"
						disabled> <label for="checkbox1"
						style="color: black !important">
						{{check.refuerzoCheckList.nombre}} </label>
				</div>
			</div>


		</div>
		<div class="row">
			<div class="col-md-12">
				<h5>Observaciones: {{vm.inspeccion.refuerzo.observacion}}</h5>
			</div>
		</div>

		<div class="row">

			<div class="col-md-12">

				<div></div>
				<audio id="myAudio" controls preload="none"> <source
					id="idAudio" src="" type="audio/wav"></audio>

			</div>
		</div>





		<hr>

	</div>



	<div class="clearfix"></div>





</div>
</html>