angular.module('myApp').controller('inspeccionesController', function($scope, $http,$document) {

	var vma = this;
	
	vma.detalle = false;
	
	 vma.total = 0;

     vma.cumple = 0;
     
     vma.html = "prueba";

	vma.fechaInicio =new Date();
	vma.fechaFin = new Date();
	vma.areas = [];
	vma.nivelesCriticos = [];
	
	vma.edicion = false;
	
	vma.personaArea = {
			
			persona : '',
			area : ''
			
	};

	
	
	
	
	var urls = ["../../admin/nivelesCriticos","../../admin/area/asignar",""];
	

	vma.asignar = asignar;
	

	vma.editar = function(personaArea){
		console.log(personaArea);
		vma.edicion = true;
		
		vma.personaArea = angular.copy(personaArea);
		
		
	}
	
	vma.cancelar = function(){
		clearAll();
	}

	function asignar() {
		
		if(validarCampos()==false){
			
			return;
		}
		
		var operacion = vma.edicion ==true ? 'actualizar' : 'asignar';
		
		swal({
			type: 'question',
			  title: '¿Desea '+operacion+' el área : '+vma.personaArea.area.are_nombre+" al personal "+vma.personaArea.persona.per_nombre+" "+vma.personaArea.persona.per_apellido,
			  // input: 'email',
			  showCancelButton: true,
			  confirmButtonText: 'Aceptar',
			  cancelButtonText : 'Cancelar',
			  cancelButtonColor : '#C62828',
			  confirmButtonColor : '#1565C0',
			  showLoaderOnConfirm: true,
			  preConfirm: () => {
			    return new Promise((resolve) => {
			    	var method = vma.edicion ==true ? 'PUT' : 'POST';
			        	
			        	guardar(method,vma.personaArea,urls[1]);
			          
			    })
			  },
			  
			  allowOutsideClick: () => !swal.isLoading()
			});
		

	}
	
	function validarCampos(){
		
		if(vma.personaArea.persona == ''){
			alerta("una persona");
			return false;
		}
		
		if(vma.personaArea.area == ''){
			
			alerta("un area");
			return false;
		}
		
	}
	
	function alerta(objeto){
		swal(
				  'Información',
				  'Debe seleccionar '+objeto,
				  'warning'
				)
	}
	
	vma.consultar= function(){
		
		vma.inspecciones = [];
		$http({
			method : "POST",
			url : "../../admin/inspeccion/list",
			params: {fechaInicio: vma.fechaInicio.getTime(), fechaFin : vma.fechaFin.getTime(), tipo : 1}
		}).then(function(response) {

			vma.inspecciones = response.data;
			
		}, function(response) { 
			// failed
			console.log(response);
			
		});
		
		
	}
	
	
	vma.contar = function(detalle, tipo){
		
		var count = 0;
		
		for(var i in detalle){
		
		if(tipo ==detalle[i].calificacion.cal_codigo){
		
			count ++;
		
		}
		
		}
		return count;
		
	}
	
	vma.seleccionarInspeccion = function(ins){
		
		vma.inspeccion = angular.copy(ins);
		vma.calculoProcedimientos();
		vma.audioHtml();
		vma.detalle = true;
		google.charts.setOnLoadCallback(drawChart);
			
	}
	
	 vma.calculoProcedimientos = function() {

	        vma.total = 0;

	        vma.cumple = 0;

	        for (var i in  vma.inspeccion.inspeccionDetalles) {
	        	
	        	 var det = vma.inspeccion.inspeccionDetalles[i];

	            if (det.calificacion.cal_codigo == 1) {

	                vma.total++;
	                vma.cumple++;
	            }
	            if (det.calificacion.cal_codigo == 2) {
	                vma.total++;
	            }
	        }

	   
	    }
	 
	 vma.audioHtml = function(){
	
		document.getElementById("idAudio").src = "../../audio?audio="+vma.inspeccion.refuerzo.audio;
		// document.getElementById("idAudio2").src =
		// "../../audio?audio="+vma.inspeccion.refuerzo.audio;
		document.getElementById("myAudio").load();
		 
		 	 
	 }


	  google.charts.load('current', {'packages':['corechart']});
      

      function drawChart() {

    	  var cumple = vma.contar(vma.inspeccion.inspeccionDetalles,1);
    	  var noCumple = vma.contar(vma.inspeccion.inspeccionDetalles,2);
    	  var noAplica = vma.contar(vma.inspeccion.inspeccionDetalles,3);
    	  
        var data = google.visualization.arrayToDataTable([
          ['CC', 'Valor'],
          ['Cumple',     cumple],
          ['No Cumple',      noCumple],
          ['No Aplica',  noAplica],
          
        ]);

        var options = {
        		left: 18,
        		  top: 0,
          width: 900,
          height: 500,
          colors: ['#4CAF50', '#E53935', '#0D47A1'],
          title: 'Comportamientos Críticos',
          is3D: true
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
	
vma.descargar =function(){
	tableToExcel("table2excel");
}
	
	
	var tableToExcel = (function() {
        var uri = 'data:application/vnd.ms-excel;base64,'
          , template = '<meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
          , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
          , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
        return function(table, name) {
          if (!table.nodeType) table = document.getElementById(table)
          var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
          window.location.href = uri + base64(format(template, ctx))
        }
      })()
      
	
	
});