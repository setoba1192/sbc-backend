angular.module('myApp').controller('asignarController', function($scope, $http) {

	var vma = this;

	vma.areas = [];
	vma.nivelesCriticos = [];
	
	vma.edicion = false;
	
	vma.personaArea = {
			
			persona : '',
			area : '',
				actividades :[]
			
	};

	
	
	
	
	var urls = ["../../admin/nivelesCriticos","../../admin/area/asignar",""];
	

	vma.asignar = asignar;
	

	vma.editar = function(personaArea){
		console.log(personaArea);
		vma.edicion = true;
		
		vma.personaArea = angular.copy(personaArea);
		
		
	}
	
	vma.cancelar = function(){
		clearAll();
	}

	function asignar() {
		
		if(validarCampos()==false){
			
			return;
		}
		
		var operacion = vma.edicion ==true ? 'actualizar' : 'asignar';
		
		swal({
			type: 'question',
			  title: '¿Desea '+operacion+' el área : '+vma.personaArea.area.are_nombre+" al personal "+vma.personaArea.persona.per_nombre+" "+vma.personaArea.persona.per_apellido,
			  // input: 'email',
			  showCancelButton: true,
			  confirmButtonText: 'Aceptar',
			  cancelButtonText : 'Cancelar',
			  cancelButtonColor : '#C62828',
			  confirmButtonColor : '#1565C0',
			  showLoaderOnConfirm: true,
			  preConfirm: () => {
			    return new Promise((resolve) => {
			    	var method = vma.edicion ==true ? 'PUT' : 'POST';
			        	
			        	guardar(method,vma.personaArea,urls[1]);
			          
			    })
			  },
			  
			  allowOutsideClick: () => !swal.isLoading()
			});
		

	}
	
	function validarCampos(){
		
		if(vma.personaArea.persona == ''){
			alerta("una persona");
			return false;
		}
		
		if(vma.personaArea.area == ''){
			
			alerta("un area");
			return false;
		}
		
	if(vma.personaArea.actividades.length == 0){
			
			alerta("al menos una Tarea Crítica.");
			return false;
		}
		
	}
	
	function alerta(objeto){
		swal(
				  'Información',
				  'Debe seleccionar '+objeto,
				  'warning'
				)
	}
	
	function guardar(metodo, object, url) {

		
		$http({
			method : metodo,
			url : url,
			data : object
		}).then(function(response) {

			
			mensajeDialog(response);
			if(response.data.type=="WARNING"){
				return;
			}
			clearAll();
			getAreas();
			getPersonas();
			getAsignaciones();
			
		}, function(response) { 
			// failed
			console.log(response);
			mensajeDialog(response);
		});

	}
	
	vma.seleccionArea = function(area){
		
		if(area.asignacion){
			return;
		}
		
		vma.personaArea.actividades = [];
		
		vma.personaArea.area = angular.copy(area);
		$('#areasModal').modal('hide')
		
	}
	
	vma.seleccionPersona = function(persona){
		
		if(persona.asignacion){
			return;
		}
		
		vma.personaArea.persona = angular.copy(persona);
		$('#personasModal').modal('hide')
		
	}
	
	
	
	function mensajeDialog(response){
		
		swal({
  	      type: response.data.type.toLowerCase(),
  	      title: 'Información:',
  	      html: response.data.mensaje
  	    })
	}


	function getPersonas() {

		var url = "../../admin/getPersonas";

		// console.log($scope.tipoArticulo);
		$http({
			method : "GET",
			url : url

		}).then(function(response) {

			vma.personas = response.data;

		}, function(response) { // optional
			// failed
		});

	}
	getPersonas();
	
	function getAreas() {

		var url = "../../admin/getAreas";

		// console.log($scope.tipoArticulo);
		$http({
			method : "GET",
			url : url

		}).then(function(response) {

			vma.areas = response.data;

		}, function(response) { // optional
			// failed
		});

	}
	getAreas();
	
	function getAsignaciones() {

		var url = "../../admin/personasAreas";

		// console.log($scope.tipoArticulo);
		$http({
			method : "GET",
			url : url

		}).then(function(response) {

			vma.personasAreas = response.data;

		}, function(response) { // optional
			// failed
		});

	}
	getAsignaciones();
	
	function clearAll(){
		vma.personaArea = {
				
				persona : '',
				area : '',
				actividades :[]
				
		};
		vma.edicion = false;
	}
});