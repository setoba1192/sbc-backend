
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html data-ng-controller="asignarController as vmas">

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style>
/* The container */
.container {
	color: black !important;
	display: block;
	position: relative;
	padding-left: 35px;
	margin-bottom: 12px;
	cursor: pointer;
	font-size: 22px;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

/* Hide the browser's default checkbox */
.container input {
	position: absolute;
	opacity: 0;
	cursor: pointer;
}

/* Create a custom checkbox */
.checkmark {
	position: absolute;
	top: 0;
	left: 0;
	height: 25px;
	width: 25px;
	background-color: #eee;
}

/* On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
	background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container input:checked ~ .checkmark {
	background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
	content: "";
	position: absolute;
	display: none;
}

/* Show the checkmark when checked */
.container input:checked ~ .checkmark:after {
	display: block;
}

/* Style the checkmark/indicator */
.container .checkmark:after {
	left: 9px;
	top: 5px;
	width: 5px;
	height: 10px;
	border: solid white;
	border-width: 0 3px 3px 0;
	-webkit-transform: rotate(45deg);
	-ms-transform: rotate(45deg);
	transform: rotate(45deg);
}
</style>
<div class="card">

	<div class="header">
		<b><h4>Asignar Area</h4></b>
	</div>
	<div class="content">
		<!-- <button ng-click="vma.prueba()">Hola</button>  -->
		<form ng-submit="vmas.asignar()">
			<div class="row">

				<div class="col-md-6">
					<div class="form-group">
						<label>�rea</label>
						<div class="input-group">
							<span class="input-group-btn">
								<button class="btn btn-primary" type="button"
									data-toggle="modal" data-target="#areasModal">Seleccionar</button>
							</span> <input type="text" class="form-control border-input"
								data-ng-model="vmas.personaArea.area.are_nombre" required
								placeholder="Seleccionar un �rea..." readonly>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>Observador</label>
						<div class="input-group">
							<span class="input-group-btn">
								<button class="btn btn-primary" type="button"
									data-toggle="modal" data-target="#personasModal"
									ng-disabled="vmas.edicion">Seleccionar</button>
							</span> <input type="text" class="form-control border-input"
								value="{{vmas.personaArea.persona.per_nombre}} {{vmas.personaArea.persona.per_apellido}} "
								required placeholder="Seleccionar una persona..." readonly>
						</div>
					</div>
				</div>


			</div>
			<div class="row">

				<div class="col-md-6">

					Tareas Cr�ticas:

					<div style="margin: 10px;"
						data-ng-repeat="actividad in vmas.personaArea.area.actividades">
						<label class="container"><input type="checkbox"
							data-checklist-model="vmas.personaArea.actividades"
							data-checklist-value="actividad" checklist-comparator=".act_codigo">{{actividad.act_nombre}}
							<span class="checkmark"></span></label>
					</div>


				</div>
				<div class="col-md-6">

					Tareas Cr�ticas Asignadas:

					<ul>
						<li style="margin: 10px;"
							data-ng-repeat="actividad in vmas.personaArea.actividades">{{actividad.act_nombre}}</li>

					</ul>


				</div>
			</div>

			<div class="text-center">
				<p ng-if="vmas.edicion">
					<b>Informaci�n: </b>S�lo se permite cambiar el �rea
				</p>
				<button ng-show="!vmas.edicion" type="submit"
					class="btn btn-info btn-fill btn-wd">Asignar</button>

				<button ng-show="vmas.edicion" type="submit"
					class="btn btn-info btn-fill btn-wd">Actualizar</button>
				<button ng-show="vmas.edicion" type="button"
					ng-click="vmas.cancelar()" class="btn btn-danger btn-fill btn-wd">Cancelar
				</button>
			</div>
			<div class="clearfix"></div>


		</form>
	</div>
	<hr />

	<div class="row">



		<div class="col-md-12">
			<div class="card card-plain">
				<div class="header">
					<b><h4>Personal</h4></b>
					<p class="category">Personas registradas</p>

				</div>
				<div class="content table-responsive table-full-width">
					<table class="table table-hover">
						<thead>

							<th>Apellido</th>
							<th>Nombre</th>
							<th>�rea</th>

							<th style="text-align: center;">Asignaci�n</th>
							<th style="text-align: center;">Habilitado</th>
							<th style="text-align: center;"></th>

						</thead>
						<tbody>
							<tr ng-repeat="personaArea in vmas.personasAreas">

								<td>{{personaArea.persona.per_apellido}}</td>
								<td>{{personaArea.persona.per_nombre}}</td>
								<td><b>{{personaArea.area.are_nombre}}</b></td>

								<td align="center"><div align="center"
										data-ng-class="personaArea.persona.asignacion == true ? 'led-green' : 'led-red'"></div></td>
								<td align="center"><div align="center"
										data-ng-class="personaArea.persona.per_estado == true ? 'led-green' : 'led-red'"></div></td>
								<td><button type="button" class="btn btn-primary btn-sm"
										data-ng-click="vmas.editar(personaArea)">Editar</button></td>

							</tr>
						</tbody>
					</table>

				</div>
			</div>
		</div>


	</div>




</div>

<!-- MODALS  -->

<!-- MODAL AREA -->
<div class="modal fade" id="areasModal" data-toggle="modal"
	aria-labelledby="areasLabel" style="background: rgba(0, 0, 0, 0.7)">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="areasLabel">
					<b>Seleccione Area</b>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">



					<div class="col-md-12">



						<table class="table table-hover sortable">
							<thead>
								<th>ID</th>
								<th>Nombre</th>
								<!--  <th>Nivel Cr�tico</th>-->
								<th style="text-align: center;">Asignada</th>

							</thead>
							<tbody>
								<tr dir-paginate="area in vmas.areas  | itemsPerPage: 5"
									pagination-id="areas" data-ng-click="vmas.seleccionArea(area)">
									<td>{{area.are_codigo}}</td>
									<td>{{area.are_nombre}}</td>
									<!-- <td>{{area.nivelCritico.nic_nombre}}</td> -->

									<td align="center"><div
											data-ng-class="area.asignacion == true ? 'led-green' : 'led-red'"></div></td>

								</tr>


							</tbody>
						</table>
						<div>

							<div class="text-center">
								<dir-pagination-controls boundary-links="true"
									pagination-id="areas" template-url="dirPagination.tpl.html"></dir-pagination-controls>
							</div>
						</div>


					</div>


				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

			</div>
		</div>
	</div>
</div>
<!-- FIN MODAL AREA -->

<!-- MODAL PERSONA -->
<div class="modal fade" id="personasModal" data-toggle="modal"
	aria-labelledby="personasLabel" style="background: rgba(0, 0, 0, 0.7)">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="personasLabel">Seleccione una
					persona</h4>
			</div>
			<div class="modal-body">
				<div class="row">



					<div class="col-md-12">



						<table class="table table-hover sortable">
							<thead>
								<th>Identificacion</th>
								<th>Apellido</th>
								<th>Nombre</th>
								<th>Telefono</th>
								<th>Email</th>
								<th style="text-align: center;">Asignaci�n</th>
							</thead>
							<tbody>
								<tr dir-paginate="persona in vmas.personas | itemsPerPage: 5"
									pagination-id="personas"
									data-ng-click="vmas.seleccionPersona(persona)">
									<td>{{persona.per_identificacion}}</td>
									<td>{{persona.per_apellido}}</td>
									<td>{{persona.per_nombre}}</td>
									<td>{{persona.per_telefono}}</td>
									<td>{{persona.per_email}}</td>
									<td align="center"><div align="center"
											data-ng-class="persona.asignacion == true ? 'led-green' : 'led-red'"></div></td>



								</tr>


							</tbody>
						</table>
						<div>

							<div class="text-center">
								<dir-pagination-controls boundary-links="true"
									pagination-id="personas" template-url="dirPagination.tpl.html"></dir-pagination-controls>
							</div>
						</div>


					</div>


				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

			</div>
		</div>
	</div>
</div>
<!-- FIN MODAL PERSONA -->


</html>