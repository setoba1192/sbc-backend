<%@page import="com.sbc.util.Parametros"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%
	String clase = request.getParameter("menu");
	String path = request.getContextPath();
%>
<div class="logo" align="center">
	<img width="50%" src="<%=path%>/resources/img/<%=Parametros.logo%>"> <!-- <img
		width="50%"
		src="<%=path%>/resources/img/uao_logo_.png"
		alt="Smiley face"> --> <a href="" class="simple-text">
		ADMINISTRADOR </a> <a href="<%=path%>/app/logout" class="text-center"><h6>Salir</h6>
	</a>
</div>
<ul class="nav">
	<li class="<%=clase.equals("inicio") ? "active" : ""%>"><a
		href="../home"> <i class="ti-panel"></i>
			<p>Inicio</p>
	</a></li>
	<li class="<%=clase.equals("parametros") ? "active" : ""%>"><a
		href="../parametrizacion"> <i class="ti-panel"></i>
			<p>Parametrización</p>
	</a></li>
	<li class="<%=clase.equals("asignar") ? "active" : ""%>"><a
		href="../asignar"> <i class="ti-user"></i>
			<p>Asignar Área/Tareas</p>
	</a></li>
	<li class="<%=clase.equals("inspecciones") ? "active" : ""%>"><a
		href="../inspecciones"> <i class="ti-search"></i>
			<p>Observaciones</p>
	</a></li>
	<!-- <li class="<%=clase.equals("refuerzos") ? "active" : ""%>"><a
		href="../refuerzos"> <i class="ti-heart"></i>
			<p>Refuerzos</p>
	</a></li> -->
	<li class="<%=clase.equals("reporte") ? "active" : ""%>"><a
		href="../reporte"> <i class="ti-comment-alt"></i>
			<p>Reportes Ocasionales</p>
	</a></li>

</ul>
</html>