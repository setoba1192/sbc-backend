
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html data-ng-controller="asignarController as vmas">

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style>
/* The container */
.container {
	color: black !important;
	display: block;
	position: relative;
	padding-left: 35px;
	margin-bottom: 12px;
	cursor: pointer;
	font-size: 22px;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

/* Hide the browser's default checkbox */
.container input {
	position: absolute;
	opacity: 0;
	cursor: pointer;
}

/* Create a custom checkbox */
.checkmark {
	position: absolute;
	top: 0;
	left: 0;
	height: 25px;
	width: 25px;
	background-color: #eee;
}

/* On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
	background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container input:checked ~ .checkmark {
	background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
	content: "";
	position: absolute;
	display: none;
}

/* Show the checkmark when checked */
.container input:checked ~ .checkmark:after {
	display: block;
}

/* Style the checkmark/indicator */
.container .checkmark:after {
	left: 9px;
	top: 5px;
	width: 5px;
	height: 10px;
	border: solid white;
	border-width: 0 3px 3px 0;
	-webkit-transform: rotate(45deg);
	-ms-transform: rotate(45deg);
	transform: rotate(45deg);
}
</style>

<div class="row">
	<div class="panel panel-default">
		<div class="panel-heading">
			<b>Resumen Anual de Observaciones - 2019</b>
		</div>
		<div class="panel-body">
			<canvas id="pie" height="120" class="chart chart-pie chart-xs"
				chart-data="data" chart-labels="labels" chart-series="series"
				legend="true" chart-options="options"></canvas>
		</div>
	</div>

</div>


<div class="row">
	<div class="panel panel-default">
		<div class="panel-heading">
			<b>Resumen Mensual de Observaciones - 2019</b>
		</div>
		<div class="panel-body">
			<div id="chart"></div>
		</div>
	</div>
</div>


<!-- MODALS  -->

<!-- MODAL AREA -->
<div class="modal fade" id="areasModal" data-toggle="modal"
	aria-labelledby="areasLabel" style="background: rgba(0, 0, 0, 0.7)">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="areasLabel">
					<b>Seleccione Area</b>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">



					<div class="col-md-12">



						<table class="table table-hover sortable">
							<thead>
								<th>ID</th>
								<th>Nombre</th>
								<!--  <th>Nivel Cr�tico</th>-->
								<th style="text-align: center;">Asignada</th>

							</thead>
							<tbody>
								<tr dir-paginate="area in vmas.areas  | itemsPerPage: 5"
									pagination-id="areas" data-ng-click="vmas.seleccionArea(area)">
									<td>{{area.are_codigo}}</td>
									<td>{{area.are_nombre}}</td>
									<!-- <td>{{area.nivelCritico.nic_nombre}}</td> -->

									<td align="center"><div
											data-ng-class="area.asignacion == true ? 'led-green' : 'led-red'"></div></td>

								</tr>


							</tbody>
						</table>
						<div>

							<div class="text-center">
								<dir-pagination-controls boundary-links="true"
									pagination-id="areas" template-url="dirPagination.tpl.html"></dir-pagination-controls>
							</div>
						</div>


					</div>


				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

			</div>
		</div>
	</div>
</div>
<!-- FIN MODAL AREA -->

<!-- MODAL PERSONA -->
<div class="modal fade" id="personasModal" data-toggle="modal"
	aria-labelledby="personasLabel" style="background: rgba(0, 0, 0, 0.7)">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="personasLabel">Seleccione una
					persona</h4>
			</div>
			<div class="modal-body">
				<div class="row">



					<div class="col-md-12">



						<table class="table table-hover sortable">
							<thead>
								<th>Identificacion</th>
								<th>Apellido</th>
								<th>Nombre</th>
								<th>Telefono</th>
								<th>Email</th>
								<th style="text-align: center;">Asignaci�n</th>
							</thead>
							<tbody>
								<tr dir-paginate="persona in vmas.personas | itemsPerPage: 5"
									pagination-id="personas"
									data-ng-click="vmas.seleccionPersona(persona)">
									<td>{{persona.per_identificacion}}</td>
									<td>{{persona.per_apellido}}</td>
									<td>{{persona.per_nombre}}</td>
									<td>{{persona.per_telefono}}</td>
									<td>{{persona.per_email}}</td>
									<td align="center"><div align="center"
											data-ng-class="persona.asignacion == true ? 'led-green' : 'led-red'"></div></td>



								</tr>


							</tbody>
						</table>
						<div>

							<div class="text-center">
								<dir-pagination-controls boundary-links="true"
									pagination-id="personas" template-url="dirPagination.tpl.html"></dir-pagination-controls>
							</div>
						</div>


					</div>


				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

			</div>
		</div>
	</div>
</div>
<!-- FIN MODAL PERSONA -->


</html>