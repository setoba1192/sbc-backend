var app = angular.module(
		'myApp',
		[ 'angularUtils.directives.dirPagination', 'checklist-model',
				'chart.js' ]).controller('mainController',
		function($scope, $http) {

			var main = this;

			main.area = false;
			main.personal = false;
			main.actividades = false;

			main.submenu = true;

		});

app.config([ 'ChartJsProvider', function(ChartJsProvider) {
	// Configure all charts
	ChartJsProvider.setOptions({
		chartColors : [ '#64B5F6', '#EF5350','#78909C' ],
		responsive : true
	});
} ]);