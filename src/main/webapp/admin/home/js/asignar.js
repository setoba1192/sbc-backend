angular
		.module('myApp')
		.controller(
				'asignarController',
				function($scope, $http) {

					google.charts.load('current', {
						packages : [ 'corechart', 'bar' ]
					});

					var vma = this;

					$scope.labels = [];
					$scope.data = [];
					$scope.series = [];

					$scope.options = {
						showAllTooltips : true,
						title : {
							display : true,
							// text : 'Legend',
							position : 'bottom',
							padding : 5
						},
						legend : {
							display : true
						}

					};

					var urls = [ "../../admin/nivelesCriticos",
							"../../admin/area/asignar", "" ];

					function getReporte() {

						var url = "../../admin/reporte/resumenObservacion";

						// console.log($scope.tipoArticulo);
						$http({
							method : "GET",
							url : url

						}).then(function(response) {

							vma.reporteGrafico = response.data;
							graficar();

						}, function(response) { // optional
							// failed
						});

					}
					getReporte();

					function getReporteMensual() {

						var url = "../../admin/reporte/resumenMensual";

						// console.log($scope.tipoArticulo);
						$http({
							method : "GET",
							url : url

						}).then(function(response) {

							vma.reporteGraficoMensual = response.data;
							google.charts.setOnLoadCallback(drawStacked);

						}, function(response) { // optional
							// failed
						});

					}
					getReporteMensual();

					function graficar() {

						for ( var i in vma.reporteGrafico) {
							$scope.series.push(angular
									.copy(vma.reporteGrafico[i].label));
							$scope.labels.push(angular
									.copy(vma.reporteGrafico[i].label));
							$scope.data.push(angular
									.copy(vma.reporteGrafico[i].valor));

						}

					}

					function drawStacked() {
						var data = new google.visualization.DataTable();
						data.addColumn('string', 'Més');
						data.addColumn('number', 'No Aplica');
						data.addColumn('number', 'No Cumple');
						data.addColumn('number', 'Cumple');

						for ( var i in vma.reporteGraficoMensual) {

							var rep = vma.reporteGraficoMensual[i];
							// alert(chartData[i][0]+'=>'+
							// parseInt(chartData[i][1]));
							data.addRow([ rep.mes, rep.noAplica, rep.noCumple,
									rep.cumple ]);
						}
						var options = {
							title : '',
							isStacked : true,
							width : '100%',
							height : 500,
							hAxis : {
								title : 'Meses del año',

							},
							vAxis : {
								title : '',
								gridlines : {

								},

							},
							series : {
								0 : {
									color : '#78909C'
								},
								1 : {
									color : '#EF5350'
								},
								2 : {
									color : '#64B5F6'
								},
								3 : {
									color : '#AAA'
								},
								4 : {
									color : '#EEE'
								}
							}
						};

						var chart = new google.visualization.ColumnChart(
								document.getElementById('chart'));
						chart.draw(data, options);
					}

					Chart.pluginService
							.register({
								beforeRender : function(chart) {
									if (chart.config.options.showAllTooltips) {
										// create an array of tooltips
										// we can't use the chart tooltip
										// because there is only one tooltip per
										// chart
										chart.pluginTooltips = [];
										chart.config.data.datasets
												.forEach(function(dataset, i) {
													chart.getDatasetMeta(i).data
															.forEach(function(
																	sector, j) {
																chart.pluginTooltips
																		.push(new Chart.Tooltip(
																				{
																					_chart : chart.chart,
																					_chartInstance : chart,
																					_data : chart.data,
																					_options : chart.options.tooltips,
																					_active : [ sector ]
																				},
																				chart));
															});
												});

										// turn off normal tooltips
										chart.options.tooltips.enabled = false;
									}
								},
								afterDraw : function(chart, easing) {
									if (chart.config.options.showAllTooltips) {
										// we don't want the permanent tooltips
										// to animate, so don't do anything till
										// the animation runs atleast once
										if (!chart.allTooltipsOnce) {
											if (easing !== 1)
												return;
											chart.allTooltipsOnce = true;
										}

										// turn on tooltips
										chart.options.tooltips.enabled = true;
										Chart.helpers.each(
												chart.pluginTooltips, function(
														tooltip) {
													tooltip.initialize();
													tooltip.update();
													// we don't actually need
													// this since we are not
													// animating tooltips
													tooltip.pivot();
													tooltip.transition(easing)
															.draw();
												});
										chart.options.tooltips.enabled = false;
									}
								}
							})

				});