<!DOCTYPE html>
<%@page import="com.sbc.util.Parametros"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en" style="">

<%
	String path = request.getContextPath();
%>


<head>
<meta charset="utf-8">
<!-- This file has been downloaded from Bootsnipp.com. Enjoy! -->
<title>Login</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css"
	rel="stylesheet">
<style type="text/css">
</style>
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
</head>
<body style="background: #eae6e6;">
	<div class="container">

		<div id="loginbox" style="margin-top: 100px;"
			class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">

			<div class="row">
				<div class="col-md-12" align="center">
					<img class="img-responsive"
						src="<%=path%>/resources/img/<%=Parametros.logo%>">
				</div>
			</div>
			<div class="col-md-12" align="center">

				<!-- <img src="<%=path + "/resources/img/computer.png"%>" -->

				<h3>
					<strong>SEGURIDAD BASADA EN COMPORTAMIENTOS</strong>
				</h3>

			</div>
			<div class="col-md-12" align="center">
				<div class="panel panel-info" style="border: none !important;">
					<div class="panel-heading"
						style="background: #2c3e50; color: white;">

						<div class="panel-title">Ingreso Administrador</div>
						<!-- <div
						style="float: right; font-size: 80%; position: relative; top: -10px">
						<a href="<%=path%>/user/recover">Recuperar clave</a>
					</div> -->
					</div>

					<div style="padding-top: 30px; border: 1px solid gray;"
						class="panel-body">

						<div style="display: none" id="login-alert"
							class="alert alert-danger col-sm-12"></div>

						<form id="loginform" class="form-horizontal" role="form"
							action="j_security_check" method="post">

							<div style="margin-bottom: 25px" class="input-group">
								<span class="input-group-addon"><i
									class="glyphicon glyphicon-user"></i></span> <input
									id="login-username" type="text" class="form-control"
									name="j_username" value="" required="required"
									placeholder="usuario">
							</div>

							<div style="margin-bottom: 25px" class="input-group">
								<span class="input-group-addon"><i
									class="glyphicon glyphicon-lock"></i></span> <input
									id="login-password" type="password" class="form-control"
									name="j_password" required="required" placeholder="contrase�a">
							</div>

							<div style="margin-top: 10px" class="form-group">
								<!-- Button -->

								<div class="col-sm-12 controls">
									<button type="submit" class="btn btn-success">Iniciar
										sesi�n</button>

								</div>
							</div>



						</form>



					</div>
				</div>
			</div>
		

		</div>
	</div>



	<script type="text/javascript">
		
	</script>
</body>
<footer> </footer>
</html>
