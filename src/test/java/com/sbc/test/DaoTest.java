package com.sbc.test;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.sbc.dao.AreaDao;
import com.sbc.dao.impl.AreaDaoImpl;
import com.sbc.model.Area;
import com.sbc.model.Dia;
import com.sbc.model.NivelCritico;

public class DaoTest {

	@Test
	public void createArea() {

		Area area = new Area();
		area.setAre_nombre("Area de Pasteurización");
		area.setAre_descripcion(
				"Area encargada de recibir y pasteurizar la leche a travez de los diferentes procesos industriales");

		List<Dia> dias = new ArrayList<>();

		Dia dia = new Dia();
		dia.setDia_codigo(1);
		dias.add(dia);
		
		area.setDias(dias);

		AreaDao areaDao = new AreaDaoImpl();

		assertTrue(areaDao.create(area));

	}

}
