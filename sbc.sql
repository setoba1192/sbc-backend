-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: sbc
-- ------------------------------------------------------
-- Server version	5.5.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actividad`
--

DROP TABLE IF EXISTS `actividad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actividad` (
  `act_codigo` int(11) NOT NULL AUTO_INCREMENT,
  `act_nombre` varchar(50) DEFAULT NULL,
  `act_area` int(11) DEFAULT NULL,
  `act_estado` bit(1) DEFAULT b'1',
  `act_fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `act_descripcion` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`act_codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actividad`
--

LOCK TABLES `actividad` WRITE;
/*!40000 ALTER TABLE `actividad` DISABLE KEYS */;
INSERT INTO `actividad` VALUES (8,'Act 1 editado de nuevo',1,'','2018-02-25 22:55:12','fasdfadf cual editado de nuevo'),(9,'Act 1 editado 333',1,'','2018-02-26 00:52:40','fasdfadf cual editado? 3333'),(10,'act 1 1 1 haha',2,'','2018-02-26 01:02:33','fadsfasdfdsf 1 1 1'),(11,'fasd',2,'','2018-02-26 01:03:22','asdfasdf'),(12,'Actividad final',3,'','2018-02-26 01:04:18','Final'),(13,'asdfasdf',4,'','2018-02-26 01:07:45','asdfsadf'),(14,'Act 1 editado 4444',1,'','2018-02-26 01:09:31','fasdfadf cual editado? 4444'),(15,'asdfasdf',5,'','2018-02-26 01:10:03','sadfsadfas'),(16,'sfsdfds',2,'','2018-02-26 01:14:28','sdfsdfdsa'),(17,'Act 1 editado',1,'','2018-02-26 01:43:08','fasdfadf cual editado?'),(18,'AAAAA',2,'','2018-03-04 02:52:20','fsdfasdf'),(19,'Act 1 editado',1,'','2018-03-04 04:09:32','fasdfadf cual editado?'),(20,'Act 2',3,'','2018-03-04 15:34:48','ddd'),(21,'Act 3',3,'','2018-03-04 15:37:00','afdfasdf'),(22,'Aplicación de producto quimico XXX',28,'','2018-03-10 21:23:23','xxx'),(23,'asdfdsf',1,'','2018-03-10 22:22:23',NULL),(24,'a',28,'','2018-03-10 22:22:43',NULL),(25,'xa',1,'','2018-03-10 22:34:00','xa'),(26,'Actividad 1',17,'','2018-03-30 02:08:10','jdsfjalskjfñldasjfñlksfd');
/*!40000 ALTER TABLE `actividad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `area`
--

DROP TABLE IF EXISTS `area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `area` (
  `are_codigo` int(11) NOT NULL AUTO_INCREMENT,
  `are_nombre` varchar(25) DEFAULT NULL,
  `are_nivelcritico` int(11) DEFAULT NULL,
  `are_descripcion` varchar(250) DEFAULT NULL,
  UNIQUE KEY `are_codigo` (`are_codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `area`
--

LOCK TABLES `area` WRITE;
/*!40000 ALTER TABLE `area` DISABLE KEYS */;
INSERT INTO `area` VALUES (1,'Prueba111111',2,'AAAA'),(2,'sdsd',1,NULL),(3,'sdsd',2,NULL),(4,'as',1,NULL),(5,'aaaa',1,NULL),(6,'AASA',1,NULL),(7,'sads',1,NULL),(8,'dssdsd',1,NULL),(9,'Area de Pasteurización',1,'Area encargada de recibir y pasteurizar la leche a travez de los diferentes procesos industriales'),(10,'Hola',1,NULL),(11,'adsfasdfads',1,NULL),(12,'fasdfdsaf',1,NULL),(13,'fdfsdafads',1,'asdfasdfdsfa'),(14,'Prueba final',1,NULL),(15,'Area de comunicaciones',3,'fafdafasdf'),(16,'dasfdsfds',1,'asdfdsaf'),(17,'Nuevo nuevo',1,NULL),(18,'NNNN',3,NULL),(19,'Prueba122',1,'dfasfasdf'),(20,'Hello world',1,'asdfasdf'),(23,'Area dias',2,'fasdfasdf'),(24,'aaaa',1,'asdfadsf'),(25,'asdfsf',1,'asdfasdfaf'),(26,'area nn',1,'asdfadf'),(28,'Desalinización',1,'Area encargada de ....');
/*!40000 ALTER TABLE `area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `area_dia`
--

DROP TABLE IF EXISTS `area_dia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `area_dia` (
  `ard_codigo` int(11) NOT NULL AUTO_INCREMENT,
  `ard_area` int(11) DEFAULT NULL,
  `ard_dia` int(11) DEFAULT NULL,
  `ard_laboral` bit(1) DEFAULT NULL,
  PRIMARY KEY (`ard_codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `area_dia`
--

LOCK TABLES `area_dia` WRITE;
/*!40000 ALTER TABLE `area_dia` DISABLE KEYS */;
INSERT INTO `area_dia` VALUES (22,25,1,''),(23,25,2,''),(24,25,3,''),(25,25,4,''),(26,25,5,''),(27,25,6,''),(28,25,7,''),(29,26,1,''),(30,26,2,''),(31,26,3,''),(32,26,4,'\0'),(33,26,5,'\0'),(34,26,6,'\0'),(35,26,7,''),(36,28,1,''),(37,28,2,''),(38,28,3,''),(39,28,4,''),(40,28,5,''),(41,28,6,'\0'),(42,28,7,'\0'),(43,17,1,''),(44,17,2,'\0'),(45,17,3,''),(46,17,4,'\0'),(47,17,5,'\0'),(48,17,6,'\0'),(49,17,7,'\0');
/*!40000 ALTER TABLE `area_dia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calificacion`
--

DROP TABLE IF EXISTS `calificacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calificacion` (
  `cal_codigo` int(11) NOT NULL AUTO_INCREMENT,
  `cal_nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`cal_codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COMMENT='calificacion de cada procedimiento';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calificacion`
--

LOCK TABLES `calificacion` WRITE;
/*!40000 ALTER TABLE `calificacion` DISABLE KEYS */;
INSERT INTO `calificacion` VALUES (1,'CUMPLE'),(2,'NO CUMPLE'),(3,'NO APLICA'),(4,'MEJORÓ'),(5,'NO MEJORÓ');
/*!40000 ALTER TABLE `calificacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dia`
--

DROP TABLE IF EXISTS `dia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dia` (
  `dia_codigo` int(11) NOT NULL AUTO_INCREMENT,
  `dia_nombre` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`dia_codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dia`
--

LOCK TABLES `dia` WRITE;
/*!40000 ALTER TABLE `dia` DISABLE KEYS */;
INSERT INTO `dia` VALUES (1,'Lunes'),(2,'Martes'),(3,'Miércoles'),(4,'Jueves'),(5,'Viernes'),(6,'Sábado'),(7,'Domingo');
/*!40000 ALTER TABLE `dia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inspeccion`
--

DROP TABLE IF EXISTS `inspeccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inspeccion` (
  `ins_codigo` int(11) NOT NULL AUTO_INCREMENT,
  `ins_fecha` datetime DEFAULT NULL,
  `ins_actividad` int(11) DEFAULT NULL,
  `ins_area` int(11) DEFAULT NULL,
  `ins_persona` int(11) DEFAULT NULL,
  `ins_observacion` varchar(300) DEFAULT NULL,
  `ins_estado` tinyint(4) DEFAULT '1',
  `ins_tipo` int(11) DEFAULT NULL,
  PRIMARY KEY (`ins_codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 COMMENT='Tabla para guardar los tipos de inspecciones, como inspeccion y refuerzo"';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inspeccion`
--

LOCK TABLES `inspeccion` WRITE;
/*!40000 ALTER TABLE `inspeccion` DISABLE KEYS */;
INSERT INTO `inspeccion` VALUES (2,'2018-03-30 22:08:56',26,17,11,'asdfasdfdsf',1,1),(4,'2018-03-31 00:29:47',26,17,11,'asdfasdfdsf',1,1),(5,'2018-03-31 01:10:08',26,17,11,NULL,1,1),(6,'2018-03-31 01:12:11',26,17,11,NULL,1,1),(7,'2018-03-31 01:13:54',26,17,11,NULL,1,NULL),(8,'2018-03-31 01:23:20',26,17,11,NULL,1,1),(9,'2018-04-01 15:40:33',26,17,11,NULL,1,1),(10,'2018-04-08 11:30:51',26,17,11,NULL,2,1);
/*!40000 ALTER TABLE `inspeccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inspeccion_detalle`
--

DROP TABLE IF EXISTS `inspeccion_detalle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inspeccion_detalle` (
  `ind_codigo` int(11) NOT NULL AUTO_INCREMENT,
  `ind_procedimiento` int(11) DEFAULT NULL,
  `ind_observacion` varchar(250) DEFAULT NULL,
  `ind_calificacion` int(11) DEFAULT NULL,
  `ind_inspeccion` int(11) DEFAULT NULL,
  PRIMARY KEY (`ind_codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1 COMMENT='procedimientos que se evaluan';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inspeccion_detalle`
--

LOCK TABLES `inspeccion_detalle` WRITE;
/*!40000 ALTER TABLE `inspeccion_detalle` DISABLE KEYS */;
INSERT INTO `inspeccion_detalle` VALUES (1,2,'Prueba',1,2),(2,2,'Prueba',1,4),(3,54,NULL,1,5),(4,55,NULL,1,5),(5,56,NULL,2,5),(6,57,NULL,1,5),(7,54,NULL,1,6),(8,55,NULL,1,6),(9,56,'Prueba',3,6),(10,57,NULL,2,6),(11,54,NULL,1,7),(12,55,NULL,1,7),(13,56,NULL,1,7),(14,57,NULL,1,7),(15,54,NULL,1,8),(16,55,NULL,1,8),(17,56,NULL,1,8),(18,57,NULL,3,8),(19,54,NULL,1,9),(20,55,NULL,2,9),(21,56,NULL,1,9),(22,57,NULL,2,9),(23,54,NULL,3,10),(24,55,NULL,4,10),(25,56,NULL,3,10),(26,57,NULL,4,10);
/*!40000 ALTER TABLE `inspeccion_detalle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inspeccion_tipo`
--

DROP TABLE IF EXISTS `inspeccion_tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inspeccion_tipo` (
  `int_codigo` int(11) NOT NULL AUTO_INCREMENT,
  `int_nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`int_codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inspeccion_tipo`
--

LOCK TABLES `inspeccion_tipo` WRITE;
/*!40000 ALTER TABLE `inspeccion_tipo` DISABLE KEYS */;
INSERT INTO `inspeccion_tipo` VALUES (1,'INSPECCION'),(2,'REFUERZO');
/*!40000 ALTER TABLE `inspeccion_tipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nivel_critico`
--

DROP TABLE IF EXISTS `nivel_critico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nivel_critico` (
  `nic_codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nic_nombre` varchar(25) DEFAULT NULL,
  `nic_valor` int(11) DEFAULT NULL,
  UNIQUE KEY `nic_codigo` (`nic_codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nivel_critico`
--

LOCK TABLES `nivel_critico` WRITE;
/*!40000 ALTER TABLE `nivel_critico` DISABLE KEYS */;
INSERT INTO `nivel_critico` VALUES (1,'Alto',2),(2,'Medio',1),(3,'Bajo',1);
/*!40000 ALTER TABLE `nivel_critico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona` (
  `per_codigo` int(11) NOT NULL AUTO_INCREMENT,
  `per_identificacion` varchar(50) DEFAULT NULL,
  `per_nombre` varchar(50) DEFAULT NULL,
  `per_apellido` varchar(50) DEFAULT NULL,
  `per_telefono` varchar(20) DEFAULT NULL,
  `per_email` varchar(20) DEFAULT NULL,
  `per_estado` tinyint(1) NOT NULL DEFAULT '1',
  UNIQUE KEY `per_codigo` (`per_codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` VALUES (1,'1075269693','Joan Sebast','Sanchezff','31123921244444','seto@gmail.com',0),(2,'1075269694','Joan','Sanchez','3112392124','setoba1192@gmail.com',0),(3,'1075269695','Joan','Sanchez','3112392124','setoba1192@gmail.com',0),(4,'1075277657','Prueba','Prueba 2','1456321','gsfsdaf@gmail.com',0),(5,'3243232','sdfsadf','asdfsadf','asfdsdfasd','setoba1192@gmail.com',1),(6,'1075269692','Joan','Roa','3112392124','setoba1192@gmail.com',0),(7,'80767591','Christian','Valderrama','3132483701','bali.cfv@gmail.com',0),(8,'1222234567','Prueba','prueba','3112392124','setoba1192@gmail.com',1),(9,'123456789','Prueba 2','prueba 32','kdkdk','311@gmail.com',1),(10,'9876543210','Prueba 3','prueba 3','3112392124','setoba1192@gmail.com',1),(11,'10752776579','juan','suaza','2131221','setoba1192@gmail.com',1);
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona_area`
--

DROP TABLE IF EXISTS `persona_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona_area` (
  `pea_codigo` int(11) NOT NULL AUTO_INCREMENT,
  `pea_area` int(11) NOT NULL,
  `pea_persona` int(11) NOT NULL,
  `pea_fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pea_codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona_area`
--

LOCK TABLES `persona_area` WRITE;
/*!40000 ALTER TABLE `persona_area` DISABLE KEYS */;
INSERT INTO `persona_area` VALUES (1,2,1,'2018-02-19 04:55:45'),(2,5,2,'2018-02-19 04:56:43'),(3,3,3,'2018-02-19 04:57:15'),(4,4,4,'2018-02-26 01:53:00'),(5,20,5,'2018-03-04 06:56:29'),(6,28,7,'2018-03-10 21:25:38'),(7,17,11,'2018-03-28 00:58:52');
/*!40000 ALTER TABLE `persona_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `procedimiento`
--

DROP TABLE IF EXISTS `procedimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `procedimiento` (
  `pro_codigo` int(11) NOT NULL AUTO_INCREMENT,
  `pro_nombre` varchar(250) DEFAULT NULL,
  `pro_actividad` int(11) DEFAULT NULL,
  `pro_orden` int(11) DEFAULT NULL,
  `pro_estado` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`pro_codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `procedimiento`
--

LOCK TABLES `procedimiento` WRITE;
/*!40000 ALTER TABLE `procedimiento` DISABLE KEYS */;
INSERT INTO `procedimiento` VALUES (2,'ffa 1',8,0,'\0'),(3,'ffa 2',8,1,'\0'),(4,'ffa 3',8,2,'\0'),(5,'ffa 4',8,3,'\0'),(6,'1',9,0,''),(7,'2',9,1,''),(8,'Proc 1',10,0,''),(9,'proc 2',10,1,''),(10,'asdf',11,0,''),(11,'Pro 1',12,0,'\0'),(12,'Pro 2',12,1,'\0'),(13,'Pro3',12,2,'\0'),(14,'fsdfsda',12,3,'\0'),(15,'asdfasdfsaf',13,0,'\0'),(16,'asdfsadf',14,0,''),(17,'asdfsadf',15,0,''),(18,'asdfasdfsadf',15,1,''),(19,'asdfsdaf',16,0,''),(20,'sdfsadf',17,0,''),(21,'asfsadf',18,0,''),(22,'ffa 1',19,0,''),(23,'ffa 2',19,1,''),(24,'ffa 3',19,2,''),(25,'ffa 4',19,3,''),(26,'fff nuevo',8,1,''),(27,'proc 3',10,0,''),(28,'asdfasdf',8,2,''),(29,'fafa',12,0,'\0'),(30,'fdfdsf',12,0,''),(31,'fsdfsdf',12,1,'\0'),(32,'sdfsdf',12,2,'\0'),(33,'sdfsdfds',12,0,'\0'),(34,'sdfsdf',12,1,'\0'),(35,'fdsfsdf',13,0,'\0'),(36,'sdfsdf',13,0,''),(37,'sdf',13,1,''),(38,'hhhh',12,0,''),(39,'adfdf',12,0,'\0'),(40,'fff',12,1,'\0'),(41,'ffdsfdff',20,0,''),(42,'afafdff',21,0,''),(43,'Extender la piel',22,0,''),(44,'Aplicar la mezcla',22,1,''),(45,'limpiar la piel',22,2,''),(46,'aa',23,1,''),(47,'bbb',23,2,''),(48,'aa',24,2,''),(49,'bb',24,1,''),(50,'dfasfafdadf',24,3,''),(51,'lle',25,1,''),(52,'fas',25,2,''),(53,'fas',25,3,''),(54,'faasdfasdf 1',26,1,''),(55,'asdfasdfd 2',26,2,''),(56,'asdfasdfdf 3',26,3,''),(57,'adfasdff 4',26,4,'');
/*!40000 ALTER TABLE `procedimiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(25) NOT NULL,
  `password` varchar(100) NOT NULL,
  `user_type` int(11) NOT NULL,
  `person` int(11) DEFAULT NULL,
  `usu_change` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_user_type` (`user_type`),
  CONSTRAINT `FK_user_type` FOREIGN KEY (`user_type`) REFERENCES `usuario_tipo` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (22,'admin','admin',3,NULL,NULL),(23,'9876543210','456789',4,10,NULL),(24,'10752776579','776574',4,11,1);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario_tipo`
--

DROP TABLE IF EXISTS `usuario_tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario_tipo`
--

LOCK TABLES `usuario_tipo` WRITE;
/*!40000 ALTER TABLE `usuario_tipo` DISABLE KEYS */;
INSERT INTO `usuario_tipo` VALUES (3,'ADMIN'),(4,'INSPECT');
/*!40000 ALTER TABLE `usuario_tipo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-08 14:56:51
